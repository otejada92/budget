package com.example.xps.budgetapp.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;

import com.example.xps.budgetapp.Controllers.ExpenseDataController;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

/**
 * Created by XPS on 11/11/2014.
 */
public class ListViewAdapterExpenseBudget extends ArrayAdapter<ExpenseModel>

    {

        private Context context;
        private ArrayList<ExpenseModel> Data;
        private int RESOURCE;
        private int VIEWID;
        private View ROOTVIEW;

        public ListViewAdapterExpenseBudget(Context context, int resource, int element,View rootview ,ArrayList<ExpenseModel> Data){
            super(context,resource,element,Data);
            this.context=context;
            this.Data=Data;
            this.RESOURCE=resource;
            this.VIEWID=element;
            this.ROOTVIEW = rootview;
    }

        @Override
        public View getView(int position,View convertView, ViewGroup parent){

            final ViewHolder Holder;

            if(convertView==null){
                LayoutInflater inflater=(LayoutInflater)context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView=inflater.inflate(this.RESOURCE,parent,false);
            Holder=new ViewHolder();
            Holder.checkBox=(CheckBox)convertView.findViewById(this.VIEWID);
            Holder.listView = (ListView) ROOTVIEW.findViewById(R.id.expense_listview);
            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }

        Holder.checkBox.setId(Integer.parseInt(Data.get(position).getID_EXPENSE()));
        Holder.checkBox.setText(Data.get(position).getDESCRIPTION()+" $"+Data.get(position).getAMOUNT());
        return convertView;
    }


        public ArrayList<ExpenseModel>Filter(CharSequence charSequence){


        ArrayList<ExpenseModel>list_gasto= ExpenseDataController.getExpenseInformation(context);
        ArrayList<ExpenseModel>filterred_list=new ArrayList<ExpenseModel>();

        if(charSequence==null||charSequence.length()==0){
            return list_gasto;
        }
        else{
            for(ExpenseModel model_ingreso:list_gasto){
                if(model_ingreso.getDESCRIPTION().contains(charSequence.toString()))
                    filterred_list.add(model_ingreso);
            }
        }
        notifyDataSetChanged();
        return filterred_list;
    }

        static class ViewHolder {
            CheckBox checkBox;
            ListView listView;
        }

    }
