package com.example.xps.budgetapp.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.LineExpenseModel;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;
import com.example.xps.budgetapp.Tables.Expense_Table;

import java.util.ArrayList;

public class ExpenseDataController {

    public static ArrayList<ExpenseModel> getExpenseInformation(Context context){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        Cursor expense_c = table_manager.rawQuery("SELECT "+
                Expense_Table.ID_EXPENSE +","
                + Expense_Table.DESCRIPCTION +","
                + Expense_Table.AMOUNT +","
                + Expense_Table.RECURRENCE +","
                + Expense_Table.ID_CATEGORY+","+
                Expense_Table.DAY_ISSUE +
                " FROM "+Expense_Table.TABLE_NAME ,null);

        ArrayList<ExpenseModel> expenseList = new ArrayList<ExpenseModel>();

        if (expense_c.moveToFirst()) {
            do {
                expenseList.add(new ExpenseModel(expense_c.getString(0),
                        expense_c.getString(1),
                        expense_c.getString(2),
                        expense_c.getString(3),
                        expense_c.getString(4),
                        expense_c.getString(5)));
            } while(expense_c.moveToNext());
        }
        expense_c.close();
        db.close();
        return expenseList;
    }

    public  ArrayList<ExpenseModel> getFixedExpenseInformation(Context context, String currentDay){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase gastos_table = db.getWritableDatabase();
        Cursor expense_c = gastos_table.rawQuery("SELECT "+
                Expense_Table.ID_EXPENSE +","
                + Expense_Table.DESCRIPCTION +","
                + Expense_Table.AMOUNT +","
                + Expense_Table.RECURRENCE +","
                + Expense_Table.ID_CATEGORY+","+
                Expense_Table.DAY_ISSUE +
                " FROM "+Expense_Table.TABLE_NAME+
                " WHERE "+ Expense_Table.RECURRENCE+ " = 1"+
                " AND "+Expense_Table.DAY_ISSUE +" LIKE"+"'%"+currentDay+"%'",null);

        ArrayList<ExpenseModel> expenseList = new ArrayList<ExpenseModel>();

        if (expense_c.moveToFirst()) {
            do {
                expenseList.add(new ExpenseModel(expense_c.getString(0),
                        expense_c.getString(1),
                        expense_c.getString(2),
                        expense_c.getString(3),
                        expense_c.getString(4),
                        expense_c.getString(5)));
            } while(expense_c.moveToNext());
        }
        expense_c.close();
        db.close();
        return expenseList;
    }

    public static void AddExpenseToBudget(Context context, String Description, String Amount, String id_category, String id_budget,String DateHappened,String Category_desc) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        DateHappened = ManagerDate.ConvertToSqlLiteDateFormat(DateHappened);

        ContentValues data = setExpenseContentValue(Description, Amount, id_category, id_budget, DateHappened,Category_desc);

        if (data.size() != 0)
            table_manager.insert(Expense_Budget_Table.TABLE_NAME, null, data);

    }

    public static void modifyBudgetedExpense(Context context,
                                             String id_expense,
                                             String Description,
                                             String Amount,
                                             String id_category,
                                             String id_budget,
                                             String DateHappened,
                                             String Category_desc) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        DateHappened = ManagerDate.ConvertToSqlLiteDateFormat(DateHappened);
        ContentValues data = setExpenseContentValue(Description, Amount, id_category, id_budget, DateHappened,Category_desc);

        String where = Expense_Budget_Table.ID_EXPENSE + " = ? "+
                " AND "+ Expense_Budget_Table.ID_BUDGET+ " = ?";
        String[] wherearg = {id_expense,id_budget};

        if (data.size() != 0) {
            table_manager.update(Expense_Budget_Table.TABLE_NAME,
                    data,
                    where,
                    wherearg);
        }
    }

    public static ContentValues setExpenseContentValue(String description,
                                                       String amount,
                                                       String id_category,
                                                       String id_budget,
                                                       String DateHappened,
                                                       String category_desc){


        ContentValues Expense_Content_Value = new ContentValues();

        Expense_Content_Value.put(Expense_Budget_Table.DESCRIPTION, description);
        Expense_Content_Value.put(Expense_Budget_Table.AMOUNT,amount);
        Expense_Content_Value.put(Expense_Budget_Table.CATEGORY_DESC,category_desc);
        Expense_Content_Value.put(Expense_Budget_Table.ID_CATEGORY, id_category);
        Expense_Content_Value.put(Expense_Budget_Table.ID_BUDGET,id_budget);
        Expense_Content_Value.put(Expense_Budget_Table.DATE_BDGETED,DateHappened);

        return Expense_Content_Value;
    }


    public static ArrayList<BudgetedExpenseModel> getExpensesByBudget(Context context, String id_budget){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ArrayList<BudgetedExpenseModel> expenseList = new ArrayList<BudgetedExpenseModel>();

        Cursor budgeted_expense_c = table_manager.rawQuery("SELECT "+
                Expense_Budget_Table.ID_EXPENSE+","+
                Expense_Budget_Table.DESCRIPTION +","+
                Expense_Budget_Table.AMOUNT +","+
                Expense_Budget_Table.CATEGORY_DESC+","+
                Expense_Budget_Table.ID_CATEGORY+","+
                Expense_Budget_Table.ID_BUDGET+","+
                Expense_Budget_Table.DATE_BDGETED +
                " FROM "+ Expense_Budget_Table.TABLE_NAME+

                " WHERE " + Expense_Budget_Table.ID_BUDGET+" = ?"+
                " ORDER BY "+ Expense_Budget_Table.DATE_BDGETED +" ASC",new String[]{id_budget});
        if(budgeted_expense_c.moveToFirst()){
            do{
                expenseList.add(new BudgetedExpenseModel(
                        budgeted_expense_c.getString(0),
                        budgeted_expense_c.getString(1),
                        budgeted_expense_c.getString(2),
                        budgeted_expense_c.getString(3),
                        budgeted_expense_c.getString(4),
                        budgeted_expense_c.getString(5),
                        budgeted_expense_c.getString(6)));
            }while (budgeted_expense_c.moveToNext());

        }

        budgeted_expense_c.close();
        db.close();
        return expenseList;
    }

    public ArrayList<BudgetedExpenseModel> getExpenseForGeneralBudgetReport(Context context, String id_budget, String startDateMonth, String endDateMonth){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ArrayList<BudgetedExpenseModel> expenseList = new ArrayList<BudgetedExpenseModel>();

        Cursor budgeted_expense_c = table_manager.rawQuery("SELECT "+
                Expense_Budget_Table.ID_EXPENSE+","+
                Expense_Budget_Table.DESCRIPTION +","+
                Expense_Budget_Table.AMOUNT +","+
                Expense_Budget_Table.CATEGORY_DESC+","+
                Expense_Budget_Table.ID_CATEGORY+","+
                Expense_Budget_Table.ID_BUDGET+","+
                Expense_Budget_Table.DATE_BDGETED +""+
                " FROM "+ Expense_Budget_Table.TABLE_NAME+
                " WHERE "+ Expense_Budget_Table.ID_BUDGET+" = ?"+
                " AND "+ Expense_Budget_Table.DATE_BDGETED +
                " BETWEEN "+"'"+startDateMonth+"'"+" AND "+"'"+endDateMonth+"'"+
                " ORDER BY "+ Expense_Budget_Table.DATE_BDGETED +" DESC",new String[]{id_budget});
        if(budgeted_expense_c.moveToFirst()){
            do{
                expenseList.add(new BudgetedExpenseModel(budgeted_expense_c.getString(0),
                        budgeted_expense_c.getString(1),
                        budgeted_expense_c.getString(2),
                        budgeted_expense_c.getString(3),
                        budgeted_expense_c.getString(4),
                        budgeted_expense_c.getString(5),
                        budgeted_expense_c.getString(6)));
            }while (budgeted_expense_c.moveToNext());

        }
        budgeted_expense_c.close();
        db.close();
        return expenseList;
    }

    public static ArrayList<LineExpenseModel> getBudgetedExpenseBy(Context context, String fromDateStr, String toDateStr) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ArrayList<LineExpenseModel> expenseList = new ArrayList<LineExpenseModel>();
        ArrayList<BudgetedExpenseModel> markViewInformation;

        Cursor budgeted_expense_c = table_manager.rawQuery("SELECT "+
                "SUM("+ Expense_Budget_Table.AMOUNT + ")," +
                Expense_Budget_Table.ID_EXPENSE+","+
                Expense_Budget_Table.AMOUNT +","+
                Expense_Budget_Table.DATE_BDGETED +
                " FROM "+ Expense_Budget_Table.TABLE_NAME+
                " WHERE "+ Expense_Budget_Table.DATE_BDGETED +
                " BETWEEN "+"'"+fromDateStr+"'"+" AND "+"'"+toDateStr+"'"+
                " GROUP BY " + Expense_Budget_Table.DATE_BDGETED +
                " ORDER BY "+ Expense_Budget_Table.DATE_BDGETED +" ASC"
                ,null);
        if(budgeted_expense_c.moveToFirst()){
            do{
                markViewInformation = getBudgetedExpenseByDate(context, budgeted_expense_c.getString(3));

                expenseList.add(new LineExpenseModel(budgeted_expense_c.getString(0),
                        budgeted_expense_c.getString(3),markViewInformation));
            }while (budgeted_expense_c.moveToNext());

        }
        budgeted_expense_c.close();
        db.close();
        return expenseList;
    }

    public static ArrayList<BudgetedExpenseModel> getBudgetedExpenseByDate(Context context, String date) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ArrayList<BudgetedExpenseModel> expenseList = new ArrayList<BudgetedExpenseModel>();

        Cursor budgeted_expense_c = table_manager.rawQuery("SELECT "+
                Expense_Budget_Table.ID_EXPENSE+","+
                Expense_Budget_Table.DESCRIPTION +","+
                Expense_Budget_Table.AMOUNT +","+
                Expense_Budget_Table.CATEGORY_DESC+","+
                Expense_Budget_Table.ID_CATEGORY+","+
                Expense_Budget_Table.ID_BUDGET+","+
                Expense_Budget_Table.DATE_BDGETED +""+
                " FROM "+ Expense_Budget_Table.TABLE_NAME+
                " WHERE "+ Expense_Budget_Table.DATE_BDGETED +
                " LIKE "+"'%"+date+"%'",null);
        if(budgeted_expense_c.moveToFirst()){
            do{

                expenseList.add(new BudgetedExpenseModel(budgeted_expense_c.getString(0),
                        budgeted_expense_c.getString(1),
                        budgeted_expense_c.getString(2),
                        budgeted_expense_c.getString(3),
                        budgeted_expense_c.getString(4),
                        budgeted_expense_c.getString(5),
                        budgeted_expense_c.getString(6)));
            }while (budgeted_expense_c.moveToNext());

        }
        budgeted_expense_c.close();
        db.close();
        return expenseList;
    }

}
