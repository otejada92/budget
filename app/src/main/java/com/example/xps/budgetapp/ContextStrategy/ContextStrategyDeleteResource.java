package com.example.xps.budgetapp.ContextStrategy;

import android.content.Context;

import com.example.xps.budgetapp.Interfaces.DeleteStrategy;

/**
 * Created by XPS on 6/24/2015.
 */
public class ContextStrategyDeleteResource {

    DeleteStrategy STRATEGY;

    public ContextStrategyDeleteResource(DeleteStrategy strategy) {

        this.STRATEGY = strategy;
    }

    public void executeStrategy(Context context,String tableName,String whereClause,String[] whereArgs){
        this.STRATEGY.DeleteResource(context,tableName,whereClause,whereArgs);
    }

}
