package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 5/14/2015.
 */
public class GoalBudgetReportModel {

    private String MONTH;
    private String GOALBUDGET;
    private float GOALBUDGET_PROCESS;
    private String TOTALINCOME;
    private String TOTALEXPENSE;

    public GoalBudgetReportModel() {
    }

    public GoalBudgetReportModel(String month, String goalbudget,String budgetedincome,String budgetedexpense,float goalbudget_process) {

    this.MONTH = month;
    this.GOALBUDGET = goalbudget;
    this.TOTALINCOME = budgetedincome;
    this.TOTALEXPENSE =  budgetedexpense;
    this.GOALBUDGET_PROCESS = goalbudget_process;

    }

    public String getMONTH() {
        return MONTH;
    }

    public void setMONTH(String MONTH) {
        this.MONTH = MONTH;
    }

    public String getGOALBUDGET() {
        return GOALBUDGET;
    }

    public void setGOALBUDGET(String GOALBUDGET) {
        this.GOALBUDGET = GOALBUDGET;
    }

    public String getTOTALINCOME() {
        return TOTALINCOME;
    }

    public void setTOTALINCOME(String TOTALINCOME) {
        this.TOTALINCOME = TOTALINCOME;
    }

    public String getTOTALEXPENSE() {
        return TOTALEXPENSE;
    }

    public void setTOTALEXPENSE(String TOTALEXPENSE) {
        this.TOTALEXPENSE = TOTALEXPENSE;
    }

    public float getGOALBUDGET_PROCESS() {
        return GOALBUDGET_PROCESS;
    }

    public void setGOALBUDGET_PROCESS(float GOALBUDGET_PROCESS) {
        this.GOALBUDGET_PROCESS = GOALBUDGET_PROCESS;
    }
}
