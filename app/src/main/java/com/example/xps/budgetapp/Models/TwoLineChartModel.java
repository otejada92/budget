package com.example.xps.budgetapp.Models;

import java.util.ArrayList;

/**
 * Created by XPS on 7/19/2015.
 */
public class TwoLineChartModel {

    private  ArrayList<LineIncomeModel> incomeList;
    private  ArrayList<LineExpenseModel> expenseList;

    public TwoLineChartModel(ArrayList<LineIncomeModel> incomeList,ArrayList<LineExpenseModel> expenseList) {
        this.incomeList = incomeList;
        this.expenseList = expenseList;
    }

    public ArrayList<LineIncomeModel> getIncomeList() {
        return incomeList;
    }

    public void setIncomeList(ArrayList<LineIncomeModel> incomeList) {
        this.incomeList = incomeList;
    }

    public ArrayList<LineExpenseModel> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(ArrayList<LineExpenseModel> expenseList) {
        this.expenseList = expenseList;
    }
}
