package com.example.xps.budgetapp.CustomAdapters;

/**
 * Created by XPS on 5/6/2015.
 */
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.Models.GeneralBudgerReportModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class CustomGeneralBudgetInformationReportAdapter extends ArrayAdapter<GeneralBudgerReportModel> {

    private Context CONTEXT;
    private int ROOTVIEW;
    private ArrayList<GeneralBudgerReportModel> DATA;
    private int RESULTTEXTVIEW;
    private int INCOMETABLE;
    private int EXPENSETABLE;
    private int RESULTTABLE;

    private float total_icome_value = (float) 0.0;
    private float total_expense_value = (float) 0.0;
    private float diff_income_expense = (float) 0.0;
    private int MONTHVIEW;

    public CustomGeneralBudgetInformationReportAdapter(Context context, int rootView,int monthtextview,int resulttextview, int incomeTable, int expenseTable, int resultTable, ArrayList<GeneralBudgerReportModel> data) {
        super(context, rootView, monthtextview, data);

        this.CONTEXT = context;
        this.ROOTVIEW = rootView;
        this.DATA = data;
        this.MONTHVIEW = monthtextview;
        this.RESULTTEXTVIEW = resulttextview;
        this.INCOMETABLE = incomeTable;
        this.EXPENSETABLE = expenseTable;
        this.RESULTTABLE = resultTable;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        super.getView(position, convertView, parent);

        ViewHolder Holder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(this.ROOTVIEW, parent, false);

            Holder = new ViewHolder();

            Holder.incomeTable = (TableLayout) convertView.findViewById(this.INCOMETABLE);
            Holder.monthTextView = (TextView) convertView.findViewById(this.MONTHVIEW);
            Holder.expenseTable = (TableLayout) convertView.findViewById(this.EXPENSETABLE);
            Holder.resultTextView = (TextView) convertView.findViewById(this.RESULTTEXTVIEW);
            Holder.resultTable = (TableLayout) convertView.findViewById(this.RESULTTABLE);

            convertView.setTag(Holder);
        } else {
            Holder = (ViewHolder) convertView.getTag();
        }
        Holder.monthTextView.setText("Mes de "+DATA.get(position).getMONTH());

        if(Holder.incomeTable.getChildCount() == 1) {
            FillIncomeTable(Holder.incomeTable,this.DATA.get(position).getBUDGETED_INCOME());
        }

        if (Holder.expenseTable.getChildCount() == 1) {
            FillExpenseTable(Holder.expenseTable, this.DATA.get(position).getBUDGETED_EXPENSE());
        }

        if(Holder.resultTable.getChildCount() == 1){

            FillResultTable(Holder.resultTable);
            Holder.resultTextView.setText("Resultado del mes de "+DATA.get(position).getMONTH().toLowerCase());
            total_icome_value = 0;
            total_expense_value = 0;
            diff_income_expense = 0;
        }

        return convertView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void FillIncomeTable(TableLayout incomeLayout,ArrayList<BudgetedIncomeModel> incomeList) {

        int sdk = android.os.Build.VERSION.SDK_INT;

        for (BudgetedIncomeModel income : incomeList) {

            TableRow row = new TableRow(CONTEXT);
            TextView textView_desc = new TextView(CONTEXT);
            TextView textView_amount = new TextView(CONTEXT);

            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                row.setBackgroundDrawable(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
            } else {
                row.setBackground(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
            }

            textView_desc.setText(income.getDESCRIPTION());
            textView_desc.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            textView_desc.setGravity(Gravity.CENTER);

            textView_amount.setText(income.getAMOUNT());
            textView_amount.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            textView_amount.setGravity(Gravity.CENTER);

            total_icome_value = total_icome_value + Float.valueOf(income.getAMOUNT());

            row.addView(textView_desc);
            row.addView(textView_amount);
            incomeLayout.addView(row);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void FillExpenseTable(TableLayout tableLayout, ArrayList<BudgetedExpenseModel> expenseList){

        int sdk = android.os.Build.VERSION.SDK_INT;

        for(BudgetedExpenseModel expense : expenseList){

            TextView textView_desc = new TextView(CONTEXT);
            TextView textView_amount = new TextView(CONTEXT);
            TextView textView_category = new TextView(CONTEXT);
            TableRow row = new TableRow(CONTEXT);


            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                row.setBackgroundDrawable(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style) );
            } else {
                row.setBackground(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
            }

            textView_desc.setText(expense.getDESCRIPTION());
            textView_desc.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            textView_desc.setGravity(Gravity.CENTER);

            textView_amount.setText(expense.getAMOUNT());
            textView_amount.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            textView_amount.setGravity(Gravity.CENTER);

            textView_category.setText(CategoryDataController.getCategoriaDescByID(CONTEXT, expense.getID_CATEGORY()));
            textView_category.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            textView_category.setGravity(Gravity.CENTER);

            total_expense_value = total_expense_value + Float.valueOf(expense.getAMOUNT());

            row.addView(textView_desc);
            row.addView(textView_amount);
            row.addView(textView_category);

            tableLayout.addView(row);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void FillResultTable(TableLayout resultLayout){

        TableRow total_income_row = new TableRow(CONTEXT);
        TableRow total_expense_row = new TableRow(CONTEXT);
        TableRow total_diff_row = new TableRow(CONTEXT);


        SetProperty(total_income_row);
        SetProperty(total_expense_row);
        SetProperty(total_diff_row);

        TextView income_label_total = new TextView(CONTEXT);
        TextView income_total_value = new TextView(CONTEXT);

        TextView expense_label_total = new TextView(CONTEXT);
        TextView expense_total_value = new TextView(CONTEXT);

        TextView diff_label_total = new TextView(CONTEXT);
        TextView diff_total_value = new TextView(CONTEXT);

        income_label_total.setText("Total de Ingresos");
        income_total_value.setText(String.valueOf(total_icome_value));
        income_total_value.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        income_total_value.setGravity(Gravity.CENTER);

        expense_label_total.setText("Total de Gastos");
        expense_total_value.setText(String.valueOf(total_expense_value));
        expense_total_value.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        expense_total_value.setGravity(Gravity.CENTER);


        diff_income_expense = total_icome_value - total_expense_value;

        if(total_icome_value > total_expense_value) {
            diff_label_total.setText("Superavit(Posible ahorro)");
            diff_total_value.setText(String.valueOf(diff_income_expense));
            diff_total_value.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            diff_total_value.setTextColor(CONTEXT.getResources().getColor(R.color.green_2));
            diff_total_value.setGravity(Gravity.CENTER);

        }else{
            diff_label_total.setText("Deficit(Deuda)");
            diff_total_value.setText(String.valueOf(diff_income_expense));
            diff_total_value.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            diff_total_value.setGravity(Gravity.CENTER);
            diff_total_value.setTextColor(CONTEXT.getResources().getColor(R.color.red_1));
        }

        total_income_row.addView(income_label_total);
        total_income_row.addView(income_total_value);

        total_expense_row.addView(expense_label_total);
        total_expense_row.addView(expense_total_value);

        total_diff_row.addView(diff_label_total);
        total_diff_row.addView(diff_total_value);

        resultLayout.addView(total_income_row);
        resultLayout.addView(total_expense_row);
        resultLayout.addView(total_diff_row);

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void SetProperty(TableRow tableRow){

        int sdk = android.os.Build.VERSION.SDK_INT;

        tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            tableRow.setBackgroundDrawable(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
        } else {
            tableRow.setBackground(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
        }

    }

    class  ViewHolder {
        TextView monthTextView;
        TextView resultTextView;
        TableLayout incomeTable;
        TableLayout expenseTable;
        TableLayout resultTable;

    }
}



