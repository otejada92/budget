package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyInsertResource;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.Processes.InsertProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 7/4/2015.
 */
public class ListViewAdapterAddIncomeToBudgetFragment  extends ArrayAdapter<IncomeModel>{


    private Context CONTEXT;
    private final ArrayList<IncomeModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;
    private String ID_BUDGET;
    public GenericBudgetModel budgetInformation;
    public ManagerDate managerDate;

    public ListViewAdapterAddIncomeToBudgetFragment(Context context, int resource, View rootview, ArrayList<IncomeModel> Data,String id_budget) {

        super(context, resource,Data);
        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;
        this.ID_BUDGET = id_budget;
        getBudgetInformation(rootview,id_budget);

    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder Holder;
        managerDate = new ManagerDate();
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView=inflater.inflate(this.RESOURCE,parent,false);

            Holder=new ViewHolder();

            Holder.textViewDesc=(TextView)convertView.findViewById(R.id.descripcion);
            Holder.textViewAmount=(TextView)convertView.findViewById(R.id.amount);
            Holder.addButton =(ImageButton) convertView.findViewById(R.id.anadir);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }

        Holder.textViewDesc.setId(Integer.parseInt(this.DATA.get(position).getID_INCOME()));


        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getDESCRIPTION();
        String amountString = "<b>Monto:$ </b>"+ this.DATA.get(position).getAMOUNT();

        Holder.textViewDesc.setText(Html.fromHtml(descString));
        Holder.textViewAmount.setText(Html.fromHtml(amountString));



        Holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);
                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                final String tableName = Income_Budget_Table.TABLE_NAME;
                final ContentValues incomeValues = new ContentValues();

                textView.setText("Estas seguro que deseas agregar este ingreso al presupuestar?");

                incomeValues.put(Income_Budget_Table.DESCRIPTION,DATA.get(position).getDESCRIPTION());
                incomeValues.put(Income_Budget_Table.AMOUNT,DATA.get(position).getAMOUNT());
                incomeValues.put(Income_Budget_Table.DATE_INSERTED,ManagerDate.ConvertToSqlLiteDateFormat(budgetInformation.getSTART_DATE()));
                incomeValues.put(Income_Budget_Table.ID_BUDGET,ID_BUDGET);

                AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
                builder
                        .setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyInsertResource contextStrategyInsertResource = new ContextStrategyInsertResource(new InsertProcesses());
                                        contextStrategyInsertResource.executeStategy(CONTEXT,tableName,incomeValues);

                                    }
                                }
                        )
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();
            }
        });

        return convertView;
    }

    public void getBudgetInformation(View rootView,String id_budget){

        budgetInformation = BudgetDataController.getBudgetByID(rootView.getContext(), id_budget);

    }


    static class ViewHolder {

        TextView textViewDesc;
        TextView textViewAmount;
        ImageButton addButton;

    }


}
