package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.Dialogs.ModifyCategoryDialog;
import com.example.xps.budgetapp.Fragments.CategoryFragment;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.Processes.DeleteProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Tables.Category_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 3/15/2015.
 */
public class CustomAdapterListViewModifyCategory extends  ArrayAdapter<CategoryModel> {

    private final ArrayList<CategoryModel> DATA;
    private Context CONTEXT;
    private int RESOURCE;
    private FragmentManager FRAGMENTMANAGER;
    private int VIEW_INFORMATION;
    private  int EDIT_BUTTON;
    private int DELETE_BUTTON;


    public CustomAdapterListViewModifyCategory(Context context, int resource, int ViewInformation, int ButtonEdit,
                                               int ButtonDelete,FragmentManager fragmentManager ,ArrayList<CategoryModel> Data) {
        super(context, resource,ViewInformation,Data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.VIEW_INFORMATION = ViewInformation;
        this.EDIT_BUTTON = ButtonEdit;
        this.DELETE_BUTTON = ButtonDelete;
        this.FRAGMENTMANAGER = fragmentManager;
        this.DATA = Data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder Holder;

        if(convertView == null){
            LayoutInflater inflater=(LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            convertView = inflater.inflate(this.RESOURCE,parent,false);

            Holder = new ViewHolder();

            Holder.textView = (TextView) convertView.findViewById(this.VIEW_INFORMATION);
            Holder.Edit_button = (ImageButton) convertView.findViewById(this.EDIT_BUTTON);
            Holder.Delete_button = (ImageButton) convertView.findViewById(this.DELETE_BUTTON);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }

        Holder.textView.setId(Integer.valueOf(DATA.get(position).getID_CATEGORY()));
        Holder.textView.setText(DATA.get(position).getDESCRIPTION());
        Holder.Edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ModifyCategoryDialog dialog =  new ModifyCategoryDialog(
                        DATA.get(position).getID_CATEGORY()
                        ,DATA.get(position).getDESCRIPTION());

                dialog.show(FRAGMENTMANAGER,"Modify");

            }
        });

        Holder.Delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                notifyDataSetChanged();

                final String whereClause = Category_Table.ID_CATEGORY + " = ?";
                final String[] whereArgs = new String[] { DATA.get(position).getID_CATEGORY()};

                LayoutInflater inflater = (LayoutInflater) CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);

                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                textView.setText("Estas seguro que deseas borrar esta categoria?, este sera borrado y desvinculado para siempre de los GASTOS NO PRESUPUESTADOS!");

                AlertDialog.Builder builder = new AlertDialog.Builder(CONTEXT);
                builder.setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyDeleteResource contextDelete = new ContextStrategyDeleteResource(new DeleteProcesses());
                                        contextDelete.executeStrategy(rootView.getContext(),Category_Table.TABLE_NAME,whereClause,whereArgs);

                                        notifyDataSetChanged();


                                    }
                                }
                        )
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();


            }
        });
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {

        super.notifyDataSetChanged();

        CategoryFragment.PopulateListViewCategory();


    }


    static class ViewHolder {
        TextView textView;
        ImageButton Edit_button;
        ImageButton Delete_button;
    }
}
