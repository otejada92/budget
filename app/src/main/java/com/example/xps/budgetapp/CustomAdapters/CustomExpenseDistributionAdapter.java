package com.example.xps.budgetapp.CustomAdapters;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.xps.budgetapp.Dialogs.CategoryExpenseDetailDialog;
import com.example.xps.budgetapp.Models.ChartDistribucionModel;
import com.example.xps.budgetapp.Models.DistributionCategoryTotals;
import com.example.xps.budgetapp.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Highlight;
import com.github.mikephil.charting.utils.ValueFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CustomExpenseDistributionAdapter extends ArrayAdapter<ChartDistribucionModel>{


    Context context;
    int rootView;
    int pieChartId;
    int textViewId;
    ArrayList<ChartDistribucionModel> data;
    CategoryExpenseDetailDialog categoryDetail;
    FragmentManager fragmentManager;

    public CustomExpenseDistributionAdapter(Context context, int rootview, int piechartID,int textviewID,ArrayList<ChartDistribucionModel> data,FragmentManager fragmentManager)  {
        super(context, rootview,textviewID,data);

        this.context = context;
        this.rootView = rootview;
        this.pieChartId = piechartID;
        this.textViewId = textviewID;
        this.data = data;
        this.fragmentManager = fragmentManager;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        super.getView(position, convertView, parent);

        ViewHolder Holder;

        if(convertView == null){

            LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflate.inflate(this.rootView,parent,false);
            Holder = new ViewHolder();

            Holder.pieChart = (PieChart) convertView.findViewById(this.pieChartId);
            Holder.textView = (TextView) convertView.findViewById(this.textViewId);

            convertView.setTag(Holder);
        }else{
            Holder = (ViewHolder) convertView.getTag();
        }

        populateChartView(Holder.pieChart, Holder.textView, data.get(position));

        return convertView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void populateChartView(PieChart pieChart, TextView textView, final ChartDistribucionModel chartInformation){

        PieDataSet chartValues;
        int sub_index = 0;
        Resources resources = context.getResources();

        ArrayList<String> element_names = new ArrayList<String>();
        ArrayList<Entry> element_values = new ArrayList<Entry>();

        int[] colorsArrays = new int[]{
                resources.getColor(R.color.red_1),
                resources.getColor(R.color.blue_1),
                resources.getColor(R.color.green_1),
                resources.getColor(R.color.yellow_1),
                resources.getColor(R.color.purple_1)};

        for(final DistributionCategoryTotals categoryInfo : chartInformation.getCategoryInformation()){

            textView.setText("Distribucion de gastos de " + chartInformation.getMonth().toLowerCase());
            textView.setTextSize(20);
            element_names.add(categoryInfo.getCategory());
            element_values.add(new Entry(Float.valueOf(categoryInfo.getTotalExpense()), sub_index++));
            pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                     categoryDetail = new CategoryExpenseDetailDialog(chartInformation.getExpenseList(),chartInformation.getCategoryInformation().get(e.getXIndex()).getIdCategory());
                     categoryDetail.show(fragmentManager,"CategoryDetail");
             }

                @Override
                public void onNothingSelected() {

                }
            });
        }
        
        chartValues = new PieDataSet(element_values,"");

        chartValues.setSliceSpace(2f);
        chartValues.setColors(ColorTemplate.createColors(colorsArrays));
        chartValues.setValueTextSize(12);
        chartValues.setValueFormatter(new MyValueFormatter());

        pieChart.setData(new PieData(element_names, chartValues));

        pieChart.setDescription("");
        pieChart.setDrawSliceText(false);
        pieChart.setUsePercentValues(true);

        pieChart.animateXY(3000, 3000);
        pieChart.setNoDataText("No hay data por el momento");
    }


    class ViewHolder{

        PieChart pieChart;
        TextView textView;

    }

    public class MyValueFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0.0");
        }

        @Override
        public String getFormattedValue(float value) {
            return mFormat.format(value) + " %";
        }
    }


}






