package com.example.xps.budgetapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskCategoryProcess;
import com.example.xps.budgetapp.R;

/**
 * Created by XPS on 3/16/2015.
 */
public class ModifyCategoryDialog extends  DialogFragment{

    private String ID_CATEGORY;
    private String CATEGORY_DESCRIPTION;

    public ModifyCategoryDialog(String id_category, String Category_Description) {

        this.ID_CATEGORY = id_category;
        this.CATEGORY_DESCRIPTION = Category_Description;
    }

    @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            super.onCreateDialog(savedInstanceState);
            LayoutInflater inflater = getActivity().getLayoutInflater();

            final View rootView = inflater.inflate(R.layout.dialog_add_category_fragment_layout, null);

            final EditText Description_EditText = (EditText) rootView.findViewById(R.id.descripcion_categoria);

            Description_EditText.setText(CATEGORY_DESCRIPTION);
            final Toast[] toast = new Toast[1];
            return new AlertDialog.Builder(getActivity())
                    .setTitle("Modificar categoria")
                    .setView(rootView)
                    .setPositiveButton("Modificar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    CATEGORY_DESCRIPTION = Description_EditText.getText().toString().trim();
                                    String ACTION = "MODIFY";

                                    if (!CATEGORY_DESCRIPTION.equals("")) {
                                        AsyncTaskCategoryProcess task = new AsyncTaskCategoryProcess(getActivity().getBaseContext(), ID_CATEGORY, CATEGORY_DESCRIPTION,ACTION);
                                        task.execute();
                                    } else {
                                        toast[0] = Toast.makeText(rootView.getContext(), "No puede insertar un valor vacio", Toast.LENGTH_LONG);
                                        toast[0].show();
                                    }
                                }
                            }
                    )
                    .setNegativeButton("Cerrar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                }
                            }
                    )
                    .create();
        }

}


