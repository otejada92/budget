package com.example.xps.budgetapp.CustomView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private FragmentManager FRAGMENT;
    private View ROOTVIEW;
    int year;
    int month;
    int day;
    private EditText KEEPER;

    SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

    public DatePicker(View view,EditText keeper ){

        this.ROOTVIEW = view;
        this.KEEPER = keeper;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar calendar;

        if (this.KEEPER.getText().toString().equals("")) {

            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            String[] date_splitted = this.KEEPER.getText().toString().split("\\-");

            year = Integer.valueOf(date_splitted[2]);
            month = Integer.valueOf(date_splitted[0]) - 1;
            day = Integer.valueOf(date_splitted[1]);
        }
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        this.KEEPER.setText(dateFormatter.format(newDate.getTime()));

    }

}
