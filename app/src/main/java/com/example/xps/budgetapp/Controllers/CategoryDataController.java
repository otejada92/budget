package com.example.xps.budgetapp.Controllers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.Tables.Category_Table;
import com.example.xps.budgetapp.Tables.Expense_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 11/16/2014.
 */
public class CategoryDataController {

    public static ArrayList<CategoryModel> GetCategories(Context context){

        DataBaseController db =  new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        Cursor categories_c = table_manager.rawQuery("SELECT " +
                Category_Table.ID_CATEGORY +","+
                Category_Table.DESCRIPTION +
                " FROM " + Category_Table.TABLE_NAME, null);


        ArrayList<CategoryModel> categorias_array = new ArrayList<CategoryModel>();

        if (categories_c.moveToFirst()) {
            do {
                categorias_array.add(new CategoryModel(categories_c.getString(0),
                        categories_c.getString(1)));
            } while(categories_c.moveToNext());
        }
        categories_c.close();
        db.close();
        return categorias_array;
    }

    public static String getCategoriaDescByID(Context context, String id_category) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase categoryTable = db.getWritableDatabase();
        String categoriaName = "";

         Cursor categoriaCursor = categoryTable.rawQuery("Select " + Category_Table.DESCRIPTION +
                 " FROM " +Category_Table.TABLE_NAME +
                 " WHERE " + Category_Table.ID_CATEGORY + " = ?", new String[]{id_category});

        if (categoriaCursor.moveToFirst()) {
            do {
                categoriaName = categoriaCursor.getString(0);
            } while (categoriaCursor.moveToNext());
        }
        categoriaCursor.close();
        db.close();
        return categoriaName;
        }

    public  String GetIdCategoryByExpense(Context context, String id_gasto) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase gasto_table = db.getWritableDatabase();
        String id_categoria = "";


        Cursor id_cursor = gasto_table.rawQuery("SELECT " +
                    Expense_Table.ID_CATEGORY + " FROM " +
                    Expense_Table.TABLE_NAME + " WHERE " +
                    Expense_Table.ID_EXPENSE + " = ?", new String[]{id_gasto});

        if (id_cursor.moveToFirst()) {
            do {
                id_categoria = id_cursor.getString(0);

            } while (id_cursor.moveToNext());
        }

        id_cursor.close();
        db.close();
        return id_categoria;
    }
}
