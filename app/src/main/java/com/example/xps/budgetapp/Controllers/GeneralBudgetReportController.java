package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.MenuInflater;
import com.example.xps.budgetapp.CustomAdapters.CustomAdapterListviewGeneralBudgetReport;
import com.example.xps.budgetapp.Dialogs.GeneralReportDialog;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class GeneralBudgetReportController extends Activity{

    public  View parentRootView;
    public static ListView budgetListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_presupuesto_consulta_view_controller);

        LayoutInflater inflater =
                (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        parentRootView = inflater.inflate(R.layout.activity_presupuesto_consulta_view_controller, null);

        budgetListView = (ListView) findViewById(R.id.presupuesto_listview);

        ArrayAdapter adapter = PopulateBudgetListview(parentRootView);
        budgetListView.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.general_report_menu_option, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id = item.getItemId();

        if(id == R.id.filter) {
            GeneralReportDialog dialogBudgetFilter = new GeneralReportDialog(parentRootView);
            dialogBudgetFilter.show(getFragmentManager(), "Filter");
        }
        if(id == R.id.clear_filter) {
            ArrayAdapter adapter = PopulateBudgetListview(parentRootView);
            budgetListView.setAdapter(adapter);
        }
        return super.onOptionsItemSelected(item);


    }

    public static ArrayAdapter PopulateBudgetListview(View rootview) {

        ArrayList<GenericBudgetModel> presupuestos_list = BudgetDataController.getBudgetsInformation(rootview.getContext());
        String[] error = {"No hay presupuestos."};

        if (!presupuestos_list.isEmpty()) {
            return new CustomAdapterListviewGeneralBudgetReport(
                    rootview.getContext(),
                    R.layout.budget_listview_report_layout,
                    rootview,
                    presupuestos_list);
        }
            return new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
    }


}
