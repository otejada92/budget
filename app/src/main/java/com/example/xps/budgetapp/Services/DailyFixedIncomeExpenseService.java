package com.example.xps.budgetapp.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.example.xps.budgetapp.AlarmServicesConfigurations.DailyFixedIncomeExpenseAlarm;
import com.example.xps.budgetapp.Broadcast.DimissDailyFixedIncomeExpenseNotification;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Controllers.ExpenseDataController;
import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 6/30/2015.
 */
public class DailyFixedIncomeExpenseService extends Service {

    ManagerDate managerDate = new ManagerDate();
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        DataBaseController db = new DataBaseController(this);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        IncomeDataController incomeDataController = new IncomeDataController();
        ExpenseDataController expenseDataController = new ExpenseDataController();
        DailyFixedIncomeExpenseAlarm dailyFixedIncomeExpenseAlarm;
        String[] splitCurrentDate;
        int dayIndex = 2;
        String updated_budgets = "";
        int requestId = managerDate.getAlarmId();
        String idAlarm = "";

        try {
            idAlarm = intent.getStringExtra("idAlarm");
        }catch (NullPointerException ignored){}

        ManagerDate managerDate = new ManagerDate();

        SQLiteDatabase table_manager = db.getWritableDatabase();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String currentDate = managerDate.getCurrentDateSqlLiteFormat();
        splitCurrentDate = currentDate.split("/");

        ArrayList<IncomeModel> fixedIncomes = incomeDataController.getFixedIncomesInformation(getApplication().getBaseContext(),splitCurrentDate[dayIndex]);
        ArrayList<ExpenseModel> fixedExpense = expenseDataController.getFixedExpenseInformation(getApplication().getBaseContext(), splitCurrentDate[dayIndex]);
        ArrayList<GenericBudgetModel> budgetList = BudgetDataController.getFixedBudgetsInformation(getApplication().getBaseContext());

        if (!idAlarm.equals("")) {
            if (!budgetList.isEmpty()) {
                for (GenericBudgetModel budget : budgetList) {
                    insertIncomeToBudgets(budget, fixedIncomes, table_manager);
                    insertExpenseToBudgets(budget, fixedExpense, table_manager);

                    if (budgetList.indexOf(budget) != -1) {
                        updated_budgets += budget.getBUDGET_DESCRIPTION() + ",";
                    } else {
                        updated_budgets += budget.getBUDGET_DESCRIPTION();
                    }
                    if (!fixedExpense.isEmpty() || !fixedIncomes.isEmpty()) {

                        Intent dismissIntent = new Intent(this, DimissDailyFixedIncomeExpenseNotification.class);
                        dismissIntent.putExtra("notificationId", startId);
                        dismissIntent.putExtra("idAlarm", idAlarm);

                        PendingIntent piDismiss = PendingIntent.getBroadcast(this, requestId, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                        mBuilder
                                .setSmallIcon(R.drawable.status_dialog_warning_icon)
                                .setContentTitle("Budget Helper")
                                .setContentIntent(piDismiss)
                                .setContentText("Se han actualizado los presupuestos:...")
                                .setStyle(new NotificationCompat.BigTextStyle()
                                        .setBigContentTitle("Budget Helper")
                                        .bigText("Se han actualizado los presupuestos: " + updated_budgets));
                        mBuilder.setDefaults(Notification.DEFAULT_SOUND);

                        dailyFixedIncomeExpenseAlarm = new DailyFixedIncomeExpenseAlarm(getBaseContext(), false);
                        dailyFixedIncomeExpenseAlarm.setDailyFixedIncomeExpenseServices();
                        notificationManager.notify(startId, mBuilder.build());
                    }
                }
            } else {
                dailyFixedIncomeExpenseAlarm = new DailyFixedIncomeExpenseAlarm(getBaseContext(), false);
                dailyFixedIncomeExpenseAlarm.setDailyFixedIncomeExpenseServices();
            }
        }
        return START_STICKY;
    }


    public void insertIncomeToBudgets(GenericBudgetModel budget,ArrayList<IncomeModel> incomeFixed,SQLiteDatabase table_manager){

        ContentValues incomeContent = new ContentValues();
        ManagerDate managerDate = new ManagerDate();

        for (IncomeModel income: incomeFixed){

            incomeContent.put(Income_Budget_Table.DESCRIPTION,income.getDESCRIPTION());
            incomeContent.put(Income_Budget_Table.AMOUNT,income.getAMOUNT());
            incomeContent.put(Income_Budget_Table.ID_BUDGET,budget.getID_BUDGET());
            incomeContent.put(Income_Budget_Table.DATE_INSERTED,managerDate.getCurrentDateSqlLiteFormat());

            table_manager.insert(Income_Budget_Table.TABLE_NAME,null,incomeContent);
            incomeContent.clear();
        }
    }

    public void insertExpenseToBudgets(GenericBudgetModel budget,ArrayList<ExpenseModel> expenseFixed,SQLiteDatabase table_manager){

        ContentValues expenseContent = new ContentValues();
        ManagerDate managerDate = new ManagerDate();

        for (ExpenseModel expense: expenseFixed){

            String categoryDesc = CategoryDataController.getCategoriaDescByID(getBaseContext(), expense.getID_CATEGORY());

            expenseContent.put(Expense_Budget_Table.DESCRIPTION, expense.getDESCRIPTION());
            expenseContent.put(Expense_Budget_Table.AMOUNT, expense.getAMOUNT());
            expenseContent.put(Expense_Budget_Table.ID_CATEGORY, expense.getID_CATEGORY());
            expenseContent.put(Expense_Budget_Table.ID_BUDGET, budget.getID_BUDGET());
            expenseContent.put(Expense_Budget_Table.CATEGORY_DESC,categoryDesc);
            expenseContent.put(Expense_Budget_Table.DATE_BDGETED, managerDate.getCurrentDateSqlLiteFormat());

            table_manager.insert(Expense_Budget_Table.TABLE_NAME,null,expenseContent);
            expenseContent.clear();
        }

    }
}
