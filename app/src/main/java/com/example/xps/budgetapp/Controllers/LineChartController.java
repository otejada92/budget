package com.example.xps.budgetapp.Controllers;

/**
 * Created by XPS on 7/9/2015.
 */
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import com.example.xps.budgetapp.Fragments.LineChartFragment;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;


public class LineChartController extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.line_container_fragment);

        FragmentManager fragmentManager = getFragmentManager();

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        String fromDateStr = ManagerDate.ConvertToSqlLiteDateFormat(getIntent().getStringExtra("fromDate"));
        String toDateStr = ManagerDate.ConvertToSqlLiteDateFormat(getIntent().getStringExtra("toDate"));
        String toShow = getIntent().getStringExtra("Show");

        if(toShow.equals("ingresos")) {

            transaction.add(R.id.line_container_fragment, new LineChartFragment(toShow));
            transaction.commit();
            LineChartFragment.incomeList =  IncomeDataController.getBudgetedIncomeBy(getBaseContext(), fromDateStr, toDateStr);

        }

        if(toShow.equals("gastos")) {
            transaction.add(R.id.line_container_fragment, new LineChartFragment(toShow));
            transaction.commit();
            LineChartFragment.expenseList =  ExpenseDataController.getBudgetedExpenseBy(getBaseContext(), fromDateStr, toDateStr);

        }

        if(toShow.equals("Ingreso y Gasto")) {
            transaction.add(R.id.line_container_fragment, new LineChartFragment(toShow));
            transaction.commit();

            LineChartFragment.incomeList =  IncomeDataController.getBudgetedIncomeBy(getBaseContext(), fromDateStr, toDateStr);
            LineChartFragment.expenseList =  ExpenseDataController.getBudgetedExpenseBy(getBaseContext(), fromDateStr, toDateStr);

        }

    }
}
