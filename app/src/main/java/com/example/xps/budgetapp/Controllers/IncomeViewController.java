package com.example.xps.budgetapp.Controllers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.example.xps.budgetapp.Fragments.AddIncomeFragment;
import com.example.xps.budgetapp.Fragments.ModifyIncomeFragment;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.TabListener.TabListenerIncomeView;


public class IncomeViewController extends Activity{

    ActionBar.Tab Tab1, Tab2;
    Fragment fragmentTab1 = new AddIncomeFragment();
    Fragment fragmentTab2 = new ModifyIncomeFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.income_fragment_layout);

        ActionBar actionBar = getActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        Tab1 = actionBar.newTab().setText("Añadir Ingreso");
        Tab2 = actionBar.newTab().setText("Modificar Ingresos");

        Tab1.setTabListener(new TabListenerIncomeView(fragmentTab1));
        Tab2.setTabListener(new TabListenerIncomeView(fragmentTab2));


        actionBar.addTab(Tab1);
        actionBar.addTab(Tab2);

    }


}
