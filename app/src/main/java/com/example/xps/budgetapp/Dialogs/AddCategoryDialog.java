package com.example.xps.budgetapp.Dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.example.xps.budgetapp.AsyncTask.AsyncTaskCategoryProcess;
import com.example.xps.budgetapp.R;

public class AddCategoryDialog extends DialogFragment {



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreateDialog(savedInstanceState);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View rootView = inflater.inflate(R.layout.dialog_add_category_fragment_layout, null);
        final Toast[] toast = new Toast[1];

        return new AlertDialog.Builder(getActivity())
                .setTitle("Categoria nueva")
                .setView(rootView)
                .setPositiveButton("Salvar",
                        new DialogInterface.OnClickListener() {
                            public final String ACTION = "ADD";
                            public void onClick(DialogInterface dialog, int whichButton) {

                                EditText Description_EditText = (EditText) rootView.findViewById(R.id.descripcion_categoria);
                                String categoryDescription = Description_EditText.getText().toString().trim();

                                if (!categoryDescription.equals("")) {
                                    AsyncTaskCategoryProcess task = new AsyncTaskCategoryProcess(rootView.getContext(), categoryDescription, this.ACTION);
                                    task.execute();
                                } else {
                                    toast[0] = Toast.makeText(rootView.getContext(), "La descripcion de la categoria no puede estar en blanco.", Toast.LENGTH_LONG);
                                    toast[0].show();
                                }

                            }
                        }
                )
                .setNegativeButton("Cerrar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .create();

    }
}
