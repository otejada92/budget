package com.example.xps.budgetapp.Dialogs;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.example.xps.budgetapp.AlarmServicesConfigurations.EndBudgetAlarm;
import com.example.xps.budgetapp.AsyncTask.AsyncTaskUpdateMainBudgetInformation;
import com.example.xps.budgetapp.Broadcast.EndBudgetReceiver;
import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Processes.BudgetProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Budget_Table;
import java.util.HashMap;

/**
 * Created by XPS on 7/13/2015.
 */
public class BudgetDatesChangesDialog extends DialogFragment {

   private HashMap<String, String> budgetInformation;

    public BudgetDatesChangesDialog(HashMap<String, String> budget_information) {
        this.budgetInformation = budget_information;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View childRootView = inflater.inflate(R.layout.dialog_budget_date_change, null);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Aviso")
                .setView(childRootView)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {

                        AsyncTaskUpdateMainBudgetInformation task = new AsyncTaskUpdateMainBudgetInformation(getActivity(), budgetInformation);
                        task.execute();

                        Intent intent = getActivity().getIntent();
                        intent.putExtra("BUDGET_DATE",budgetInformation.get("StartDate")+","+budgetInformation.get("EndDate"));

                        }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();
    }
}
