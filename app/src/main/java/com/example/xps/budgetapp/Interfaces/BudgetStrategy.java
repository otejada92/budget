package com.example.xps.budgetapp.Interfaces;


import android.app.Activity;
import android.view.View;

import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.IncomeModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

public interface BudgetStrategy {

    void SaveBudget(View rootView, String idBudget, String budgetDescription, String goalBudget, String startDate, String endDate,String budgetState,String idStartBudgetProcess,String idEndtBudgetProcess,String sincronizado) throws ParseException;

    Boolean ModifyMainBudgetInformation(Activity activity, HashMap<String,String> budgetInformation);

    void SaveBudgetedIncomes(View rootView, String idBudget, ArrayList<IncomeModel> budgetedIncomes, String dateBudgeted) throws ParseException;

    void SaveBudgetedExpense(View rootView, String idBudget, ArrayList<ExpenseModel> budgtedExpenses, String dateBudgeted) throws ParseException;
}
