package com.example.xps.budgetapp.Broadcast;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.AlarmServicesConfigurations.MonthBudgetCheckerAlarm;


public class DimissMonthCheckerNotification extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {

        int idAlarm = Integer.valueOf(intent.getStringExtra("idAlarm"));
        int notificationId = intent.getIntExtra("notificationId",1);

        Intent intentMonthCheckerReciver = new Intent(context,MonthBudgetCheckerReceiver.class);

        PendingIntent pendingIntentStartServices = PendingIntent.getBroadcast(context, idAlarm, intentMonthCheckerReciver, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager  = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (pendingIntentStartServices != null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntentStartServices);
            context.stopService(intentMonthCheckerReciver);
            notificationManager.cancel(notificationId);
        }

        MonthBudgetCheckerAlarm monthBudgetCheckerAlarm = new MonthBudgetCheckerAlarm(context, true, 1);
        monthBudgetCheckerAlarm.setMonthBudgetChecker();

    }
}
