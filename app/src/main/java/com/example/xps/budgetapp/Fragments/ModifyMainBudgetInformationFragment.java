package com.example.xps.budgetapp.Fragments;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskUpdateMainBudgetInformation;
import com.example.xps.budgetapp.ClickListiners.OnClickEditDate;
import com.example.xps.budgetapp.ClickListiners.ShowHelpDialog;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Controllers.BudgetViewController;
import com.example.xps.budgetapp.Dialogs.BudgetDatesChangesDialog;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by XPS on 3/31/2015.
 */
public class ModifyMainBudgetInformationFragment extends Fragment {

    public String ID_BUDGET;
    public View ROOTVIEW;
    public EditText budgetDesc;
    public EditText budgetGoal;
    public EditText startDate;
    public EditText endDate;
    public Switch syncSwitch;
    public BudgetDataController dataController = new BudgetDataController();
    public GenericBudgetModel budgetInformation;
    private boolean endDateChanged = false;
    private boolean startDateChanged = false;
    public ManagerDate managerDate = new ManagerDate();
    public Calendar calendar = Calendar.getInstance();
    public SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
    private ImageButton helpButton;




    public ModifyMainBudgetInformationFragment() {
    }

    public ModifyMainBudgetInformationFragment(String id_budget) {

        this.ID_BUDGET = id_budget;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.budget_main_information_layout,container,false);
        this.ROOTVIEW = rootView;

        initView(rootView);
        SetViewValues(rootView, this.ID_BUDGET);

        startDate.setOnClickListener(new OnClickEditDate(rootView, getFragmentManager()));
        startDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                startDateChanged = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        startDate.setInputType(InputType.TYPE_NULL);

        endDate.setOnClickListener(new OnClickEditDate(rootView, getFragmentManager()));
        endDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                endDateChanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        endDate.setInputType(InputType.TYPE_NULL);

        startDateChanged = false;
        endDateChanged =false;

        syncSwitch.setTextOff("No");
        syncSwitch.setTextOn("Si");

        helpButton.setOnClickListener(new ShowHelpDialog(getFragmentManager(), "syncHelp"));


        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();
        HashMap<String,String> budget_information = new HashMap<String, String>();
        Toast toast;
        String errorToShow = "";

        String budgetDescValue = budgetDesc.getText().toString().trim();
        String budgetGoalValue = budgetGoal.getText().toString().trim();
        String startDateValue = ManagerDate.ConvertToSqlLiteDateFormat(startDate.getText().toString().trim());
        String endDateValue = ManagerDate.ConvertToSqlLiteDateFormat(endDate.getText().toString().trim());
        String budgetStatus =  getBudgetStatus(startDateValue);
        String idEndBudgetAlarm = budgetInformation.getID_END_PROCESS();
        String idStartBudgetAlamr = budgetInformation.getID_START_PROCESS();
        String switchValue = String.valueOf(syncSwitch.isChecked());


        if(id_item == R.id.edit_button){

            errorToShow = dataController.vatidateBudgetInformation(budgetDescValue, budgetGoalValue, startDateValue, endDateValue);

            if(errorToShow.equals("")) {

                budget_information.put("ID_BUDGET",ID_BUDGET);
                budget_information.put("Description", budgetDescValue);
                budget_information.put("Goal", budgetGoalValue);
                budget_information.put("StartDate", startDateValue);
                budget_information.put("EndDate", endDateValue);
                budget_information.put("BudgetStatus",budgetStatus);
                budget_information.put("Sincronizar",switchValue);
                budget_information.put("IdEndBudgetAlarm",idEndBudgetAlarm);
                budget_information.put("IdStartBudgetAlarm",idStartBudgetAlamr);
                budget_information.put("EndDateChanged",String.valueOf(endDateChanged));
                budget_information.put("StartDateChanged",String.valueOf(startDateChanged));

                if(startDateChanged || endDateChanged) {
                    BudgetDatesChangesDialog dialog = new BudgetDatesChangesDialog(budget_information);
                    dialog.show(getFragmentManager(), "Advise");
                }else{
                    AsyncTaskUpdateMainBudgetInformation task = new AsyncTaskUpdateMainBudgetInformation(getActivity(), budget_information);
                    task.execute();

                    Intent intent = getActivity().getIntent();
                    intent.putExtra("BUDGET_DATE",startDateValue+","+endDateValue);
                }
            }
            else {
                toast = Toast.makeText(getActivity().getBaseContext(),errorToShow.trim(),Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(id_item == android.R.id.home){

            Intent intentHome = new Intent(getActivity().getBaseContext(), BudgetViewController.class);
            startActivity(intentHome);
        }

        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.modify_budget_information_menu,menu);
    }

    public void SetViewValues(View rootView,String id_budget){

        budgetInformation = BudgetDataController.getBudgetByID(rootView.getContext(), id_budget);

        budgetDesc.setText(budgetInformation.getBUDGET_DESCRIPTION());
        budgetGoal.setText(budgetInformation.getBUDGET_GOAL());
        startDate.setText(budgetInformation.getSTART_DATE());
        endDate.setText(budgetInformation.getFINISH_DATE());
        syncSwitch.setChecked(Boolean.valueOf(budgetInformation.getSINCRONIZADO()));
    }

    public void initView(View rootView){

        budgetDesc = (EditText) rootView.findViewById(R.id.DescriptionBudget);
        budgetGoal = (EditText) rootView.findViewById(R.id.BudgetGoal);
        startDate = (EditText) rootView.findViewById(R.id.Fecha_inicio);
        endDate = (EditText) rootView.findViewById(R.id.Fecha_fin);
        syncSwitch = (Switch) rootView.findViewById(R.id.PredeterminadoSwitch);
        helpButton = (ImageButton) rootView.findViewById(R.id.help_button);

    }

    public String getBudgetStatus(String start_date_budget){

        String status = "";

        calendar.set(Calendar.DAY_OF_MONTH,calendar.getMinimum(Calendar.DAY_OF_MONTH));
        Date minDateBadFormat = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date maxDateBadFormat = calendar.getTime();

        String minDateRightFormat = simpleDateFormat.format(minDateBadFormat);
        String maxDateRightFormat = simpleDateFormat.format(maxDateBadFormat);

        try {

            Date startBudgetDate = simpleDateFormat.parse(start_date_budget);
            Date minDate =  simpleDateFormat.parse(minDateRightFormat);
            Date maxDate = simpleDateFormat.parse(maxDateRightFormat);

            if((startBudgetDate.after(minDate) || startBudgetDate.equals(minDate)) && (startBudgetDate.before(maxDate) ||startBudgetDate.equals(maxDate)))
                status = "1";

            if(startBudgetDate.after(maxDate))
                status = "2";

        }catch (ParseException e){
            e.printStackTrace();
        }
        return status;
    }

}
