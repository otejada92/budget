package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.xps.budgetapp.Broadcast.EndBudgetReceiver;
import com.example.xps.budgetapp.Broadcast.StartBudgetReceiver;
import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.Controllers.ModifyBudgetViewController;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Fragments.ModifyBudgetFragment;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.Processes.DeleteProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Tables.Budget_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 11/26/2014.
 */
public class ListViewAdapterModifyBudget extends ArrayAdapter<GenericBudgetModel> {


    private Context CONTEXT;
    private ArrayList<GenericBudgetModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;


    public ListViewAdapterModifyBudget(Context context, int resource, View rootview, ArrayList<GenericBudgetModel> Data) {
        super(context, resource,Data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder Holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(this.RESOURCE,parent,false);
            Holder = new ViewHolder();

            Holder.descView =(TextView)convertView.findViewById(R.id.descripcion_budget);
            Holder.metaView = (TextView)convertView.findViewById(R.id.goal_budget);
            Holder.duracionView = (TextView)convertView.findViewById(R.id.range_budget);
            Holder.estadoView = (TextView) convertView.findViewById(R.id.state_budget);

            Holder.editar_button=(ImageButton) convertView.findViewById(R.id.Editar);
            Holder.borrar_button=(ImageButton) convertView.findViewById(R.id.Borrar);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }


        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getBUDGET_DESCRIPTION();
        String amountString = "<b>Meta:$ </b>"+ this.DATA.get(position).getBUDGET_GOAL();

        String duration = "<b>Duracion: </b>"+ ManagerDate.ConvertToUserDateFormat(this.DATA.get(position).getSTART_DATE())+"" +
                " - "+ManagerDate.ConvertToUserDateFormat(this.DATA.get(position).getFINISH_DATE());

        String estado = "<b>Estado: </b>"+ getBudgetStatus(this.DATA.get(position).getBUDGET_STATUS());

        Holder.descView.setText(Html.fromHtml(descString));
        Holder.metaView.setText(Html.fromHtml(amountString));
        Holder.duracionView.setText(Html.fromHtml(duration));
        Holder.estadoView.setText(Html.fromHtml(estado));

        Holder.editar_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CONTEXT, ModifyBudgetViewController.class);
                intent.putExtra("ID_BUDGET", DATA.get(position).getID_BUDGET());
                intent.putExtra("BUDGET_DATE",DATA.get(position).getSTART_DATE()+","+DATA.get(position).getFINISH_DATE());
                CONTEXT.startActivity(intent);

            }
        });

        Holder.borrar_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final String whereClause = Budget_Table.ID_BUDGET + " = ?";
                final String[] whereArgs = new String[] { DATA.get(position).getID_BUDGET() };

                LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);

                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                textView.setText("Estas seguro que deseas borrar este presupuesto?, TODA INFORMACION(Ingresos y Gastos) SERAN BORRADOS!");

                AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
                builder
                        .setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyDeleteResource contextDelete = new ContextStrategyDeleteResource(new DeleteProcesses());
                                        contextDelete.executeStrategy(rootView.getContext(), Budget_Table.TABLE_NAME, whereClause, whereArgs);

                                        Intent intentStartBudgetReceiver = new Intent(CONTEXT, StartBudgetReceiver.class);
                                        Intent intentEndBudgetReceiver = new Intent(CONTEXT, EndBudgetReceiver.class);

                                        int idStartAlarmDay = Integer.parseInt(DATA.get(position).getID_START_PROCESS());
                                        int idEndAlarmDay = Integer.parseInt(DATA.get(position).getID_END_PROCESS());

                                        PendingIntent pendingIntentStartServices = PendingIntent.getBroadcast(CONTEXT, idStartAlarmDay, intentStartBudgetReceiver, PendingIntent.FLAG_UPDATE_CURRENT);
                                        PendingIntent pendingIntentEndServices = PendingIntent.getBroadcast(CONTEXT, idEndAlarmDay, intentEndBudgetReceiver, PendingIntent.FLAG_UPDATE_CURRENT);

                                        if (pendingIntentStartServices != null) {
                                            AlarmManager alarmManager = (AlarmManager) CONTEXT.getSystemService(Context.ALARM_SERVICE);
                                            alarmManager.cancel(pendingIntentStartServices);
                                            CONTEXT.stopService(intentStartBudgetReceiver);
                                        }

                                        if (pendingIntentEndServices != null) {
                                            AlarmManager alarmManager = (AlarmManager) CONTEXT.getSystemService(Context.ALARM_SERVICE);
                                            alarmManager.cancel(pendingIntentEndServices);
                                            CONTEXT.stopService(intentEndBudgetReceiver);
                                        }

                                        notifyDataSetChanged();

                                    }
                                }
                        )
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();

            }
        });
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        ModifyBudgetFragment.PopulateBudgetListview(ROOTVIEW);
    }

    public String getBudgetStatus(String status){

        if (status.equals("0"))
            return "Finalizado";

        if (status.equals("1"))
            return "En Proceso";

        if (status.equals("2"))
            return "Programado";

        return  "Nunca pasara";
    }

    static class ViewHolder {

        TextView descView;
        TextView metaView;
        TextView duracionView;
        TextView estadoView;
        ImageButton editar_button;
        ImageButton borrar_button;
    }
}
