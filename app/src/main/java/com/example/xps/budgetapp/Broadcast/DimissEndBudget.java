package com.example.xps.budgetapp.Broadcast;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by XPS on 8/11/2015.
 */
public class DimissEndBudget extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        int notificationId = intent.getIntExtra("notificationId", 1);
        int idAlarm = Integer.valueOf(intent.getStringExtra("idAlarm"));

        Intent intentEndBudgetReciver = new Intent(context,EndBudgetReceiver.class);

        PendingIntent pendingIntentStartServices = PendingIntent.getBroadcast(context, idAlarm, intentEndBudgetReciver, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager  = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (pendingIntentStartServices != null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntentStartServices);
            context.stopService(intentEndBudgetReciver);
            notificationManager.cancel(notificationId);
        }




    }
}
