package com.example.xps.budgetapp.Broadcast;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by XPS on 8/11/2015.
 */
public class DimissStartBudget extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        int idAlarm = Integer.valueOf(intent.getStringExtra("idAlarm"));
        int notificationId = intent.getIntExtra("notificationId", 1);

        Intent intentStartBudgetReciver = new Intent(context,StartBudgetReceiver.class);

        PendingIntent pendingIntentStartServices = PendingIntent.getBroadcast(context, idAlarm, intentStartBudgetReciver, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager  = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (pendingIntentStartServices != null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntentStartServices);
            context.stopService(intentStartBudgetReciver);
            notificationManager.cancel(notificationId);
        }

    }
}
