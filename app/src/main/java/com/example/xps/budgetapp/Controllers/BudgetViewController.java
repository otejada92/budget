package com.example.xps.budgetapp.Controllers;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.example.xps.budgetapp.Fragments.AddBudgetFragment;
import com.example.xps.budgetapp.Fragments.ModifyBudgetFragment;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.TabListener.TabListenerBudgetView;


public class BudgetViewController extends Activity {

    ActionBar.Tab Tab1,Tab2;
    Fragment fragmentTab1 = new AddBudgetFragment();
    Fragment fragmentTab2 = new ModifyBudgetFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.presupuesto_fragment_layout);

        ActionBar actionBar = getActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        Tab1 = actionBar.newTab().setText("Presupuestar");
        Tab2 = actionBar.newTab().setText("Modificar Presupestos");

        Tab1.setTabListener(new TabListenerBudgetView(fragmentTab1));
        Tab2.setTabListener(new TabListenerBudgetView(fragmentTab2));

        actionBar.addTab(Tab1);
        actionBar.addTab(Tab2);
    }

}
