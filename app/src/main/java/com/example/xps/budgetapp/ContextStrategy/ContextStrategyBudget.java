package com.example.xps.budgetapp.ContextStrategy;

import android.app.Activity;
import android.view.View;

import com.example.xps.budgetapp.Interfaces.BudgetStrategy;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.IncomeModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by XPS on 2/25/2015.
 */
public class ContextStrategyBudget {

    private BudgetStrategy STRATEGY;

    public ContextStrategyBudget(BudgetStrategy strategy) {

        this.STRATEGY = strategy;
    }

    public void executeSaveBudget(View rootview, String id_budget, String description, String goal_budget, String start_date, String end_date,String budget_state,String idStartBudgetProcess,String idEndtBudgetProcess,String sincronizado) throws ParseException {
        this.STRATEGY.SaveBudget(rootview, id_budget,description,goal_budget,start_date, end_date,budget_state,idStartBudgetProcess,idEndtBudgetProcess,sincronizado);
    }


    public  void executeSaveBudgetIncomes(View rootview, String id_budget,ArrayList<IncomeModel> budgted_incomes,String dateBudgeted) throws ParseException {
        this.STRATEGY.SaveBudgetedIncomes(rootview,id_budget,budgted_incomes,dateBudgeted);
    }

    public  void executeSaveBudgetExpenses(View rootview, String id_budget,ArrayList<ExpenseModel> budgted_expenses,String dateBudgeted) throws ParseException {
        this.STRATEGY.SaveBudgetedExpense(rootview,id_budget,budgted_expenses,dateBudgeted);
    }

    public Boolean executeModifyMainBudgetInformation(Activity activity, HashMap<String,String> budget_information){
        return this.STRATEGY.ModifyMainBudgetInformation(activity,budget_information);
    }


}
