package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;

/**
 * Created by XPS on 10/25/2014.
 */
public class Expense_Budget_Table implements BaseColumns {

    public static final String TABLE_NAME = "GASTO_PRESUPUESTADO";
    public static final String ID_BUDGET = "id_presupuesto";
    public static final String ID_EXPENSE = "id_gasto_presupuestado";
    public static final String DESCRIPTION = "descripcion";
    public static final String AMOUNT = "monto";
    public static String CATEGORY_DESC = "categoria_desc";
    public static String ID_CATEGORY = "id_categoria";
    public static final String DATE_BDGETED = "fecha_presupuestado";
}
