package com.example.xps.budgetapp.AsyncTask;

import android.os.AsyncTask;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyBudget;
import com.example.xps.budgetapp.Fragments.AddBudgetFragment;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.Processes.BudgetProcesses;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

public class AsyncTaskBudgetProcess extends AsyncTask<Void,Void,Void> {

    private View ROOTVIEW;
    private HashMap<String,String> BUDGET_INFORMATION;
    private ArrayList<IncomeModel> BUDGETED_INCOMES;
    private ArrayList<ExpenseModel> BUDGETED_EXPENSES;


    public AsyncTaskBudgetProcess(View rootview, HashMap<String, String> budget_information, ArrayList<IncomeModel> budgetedIncome, ArrayList<ExpenseModel> budgetedExpense) {


        this.ROOTVIEW = rootview;
        this.BUDGET_INFORMATION = budget_information;
        this.BUDGETED_INCOMES = budgetedIncome;
        this.BUDGETED_EXPENSES = budgetedExpense;
    }


    @Override
    protected Void doInBackground(Void... voids) {

        ContextStrategyBudget contextStrategyBudget = new ContextStrategyBudget(new BudgetProcesses());
        try {
            contextStrategyBudget.executeSaveBudget(this.ROOTVIEW,
                    this.BUDGET_INFORMATION.get("idBudget"),
                    this.BUDGET_INFORMATION.get("description"),
                    this.BUDGET_INFORMATION.get("goalBudget"),
                    this.BUDGET_INFORMATION.get("startDate"),
                    this.BUDGET_INFORMATION.get("endDate"),
                    this.BUDGET_INFORMATION.get("budgetState"),
                    this.BUDGET_INFORMATION.get("idStartBudgetAlarm"),
                    this.BUDGET_INFORMATION.get("idEndBudgetAlarm"),
                    this.BUDGET_INFORMATION.get("sincronizado"));

            contextStrategyBudget.executeSaveBudgetIncomes(this.ROOTVIEW,this.BUDGET_INFORMATION.get("idBudget"),this.BUDGETED_INCOMES,this.BUDGET_INFORMATION.get("startDate"));
            contextStrategyBudget.executeSaveBudgetExpenses(this.ROOTVIEW,this.BUDGET_INFORMATION.get("idBudget"),this.BUDGETED_EXPENSES,this.BUDGET_INFORMATION.get("startDate"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Cleaner  clean_helper = new Cleaner();
        Toast toast;
        toast = Toast.makeText(this.ROOTVIEW.getContext(), "Se ha guardado su presupuesto.", Toast.LENGTH_LONG);

        toast.show();
        clean_helper.cleanView((ScrollView) ROOTVIEW);
        AddBudgetFragment.PopulateIncomeListView(ROOTVIEW);
        AddBudgetFragment.PopulateExpenseListView(ROOTVIEW);

    }

}
