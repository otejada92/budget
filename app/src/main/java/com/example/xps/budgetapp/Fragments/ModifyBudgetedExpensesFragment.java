package com.example.xps.budgetapp.Fragments;

/**
 * Created by XPS on 11/29/2014.
 */

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskExpenseProcess;
import com.example.xps.budgetapp.Controllers.BudgetViewController;
import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Controllers.ExpenseDataController;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.CustomAdapters.CustomAdapterSpinnerCategory;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterModifyBudgetedExpenses;
import com.example.xps.budgetapp.CustomView.DatePicker;
import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ModifyBudgetedExpensesFragment extends  Fragment{

    private static boolean saveState = true;
    private View ROOTVIEW;
    private static ListView Expense_ListView;
    public static String id_expense;
    private EditText descriptionExpenseEditText;
    private EditText amountExpenseEditText;
    private static Spinner categorySpinner;
    private static  EditText DateHappened_EditText;
    private static String ID_BUDGET;
    public static Menu menuActionbar;
    int editButtonIndex = 0;

    public ModifyBudgetedExpensesFragment(String id_budget) {
        this.ID_BUDGET = id_budget;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.modify_budgeted_expense_fragment_layout,container,false);
        this.ROOTVIEW = rootView;

        InitViews(rootView);

        DateHappened_EditText.setInputType(InputType.TYPE_NULL);

        DateHappened_EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker datePicker = new DatePicker(rootView,(EditText)view);
                datePicker.show(getFragmentManager(),"DateHappened");

            }
        });

        descriptionExpenseEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionExpenseEditText.length() != 0 || !descriptionExpenseEditText.getText().toString().equals("")) {

                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        amountExpenseEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountExpenseEditText.length() != 0 || !amountExpenseEditText.getText().toString().equals("")) {
                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });

        DateHappened_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                saveState = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        PopulateExpensesListView(rootView);
        PopulateSpinnerCategory(rootView);

        categorySpinner.setEnabled(false);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.modify_expense_of_budget_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();
        String errorMessage = "";

        Toast toast;
        ManagerDate managerDate = new ManagerDate();
        String[] budgetDate =  getActivity().getIntent().getStringExtra("BUDGET_DATE").split(",");

        String startDateBudgetStr = budgetDate[0];
        String endDateBudgetStr = budgetDate[1];
        String expenseDateHappened = DateHappened_EditText.getText().toString();

        String errorDateHappened = managerDate.ValidateDateHappened(startDateBudgetStr, endDateBudgetStr, expenseDateHappened);

        if(id_item == R.id.edit_button){

            String description = descriptionExpenseEditText.getText().toString().trim();
            String amount  = amountExpenseEditText.getText().toString().trim();
            String categorySelectedId = String.valueOf(categorySpinner.getSelectedView().getId());
            String categoryDesc = "";

            if(!categorySelectedId.equals(String.valueOf(R.id.CategoryChoise))) {
                CategoryModel model = (CategoryModel) categorySpinner.getSelectedItem();
                categoryDesc = model.getDESCRIPTION();
            }

            HashMap<String, String> expenseInformation = new HashMap<String, String>();
            String SaveStateStr;

            if (description.length() == 0 || description.matches("\\s+")) {
                errorMessage += "La descripcion del gasto no puede estar vacia.\n";
                saveState = false;
            }

            if (amount.length() == 0) {
                errorMessage += "El monto del gasto no puede estar vacia.\n";
                saveState = false;
            }

            if (categorySelectedId.equals(String.valueOf(R.id.CategoryChoise))) {
                errorMessage += "Seleccione una categoria.\n";
                saveState = false;
            }

            if (!errorDateHappened.equals("")){
                errorMessage += errorDateHappened+"\n ";
                saveState = false;
            }


            if(saveState) {

                SaveStateStr =  String.valueOf(saveState);
                expenseInformation.put("idExpense", id_expense);
                expenseInformation.put("description", description);
                expenseInformation.put("amount", amount);
                expenseInformation.put("idCategory", categorySelectedId);
                expenseInformation.put("categoryDesc", categoryDesc);
                expenseInformation.put("saveState", SaveStateStr);
                expenseInformation.put("idBudget", ID_BUDGET);
                expenseInformation.put("dateHappened", DateHappened_EditText.getText().toString());
                expenseInformation.put("DO", "MODIFYOFBUDGET");

                AsyncTaskExpenseProcess task = new AsyncTaskExpenseProcess(ROOTVIEW, expenseInformation,ROOTVIEW.getContext());
                task.execute();

                descriptionExpenseEditText.setEnabled(false);
                amountExpenseEditText.setEnabled(false);
                DateHappened_EditText.setEnabled(false);
                categorySpinner.setEnabled(false);
                menuActionbar.getItem(editButtonIndex).setEnabled(false);
            }else {
                toast =  Toast.makeText(getActivity(),errorMessage.trim(),Toast.LENGTH_LONG);
                toast.show();
            }

        }

        if(id_item == android.R.id.home){

            Intent intenHome = new Intent(getActivity().getBaseContext(), BudgetViewController.class);
            startActivity(intenHome);
        }


        return true;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menuActionbar = menu;

        menu.getItem(editButtonIndex).setEnabled(false);
    }

    public static void PopulateSpinnerCategory(View rootview){

        ArrayList<CategoryModel> CategoriesList = CategoryDataController.GetCategories(rootview.getContext());

        if (!CategoriesList.isEmpty()) {
            CustomAdapterSpinnerCategory categoryAdapter = new CustomAdapterSpinnerCategory(rootview.getContext(), R.layout.category_spinner_layout, R.id.CategoryChoise, CategoriesList);
            categorySpinner.setAdapter(categoryAdapter);
        }
        else{
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),R.layout.category_spinner_layout,new String[]{"No hay categoria"});
            categorySpinner.setAdapter(errorAdapter);
        }

    }

    public static void PopulateExpensesListView(View rootview){

        ArrayList<BudgetedExpenseModel> Expense_List =  ExpenseDataController.getExpensesByBudget(rootview.getContext(), ID_BUDGET);
        String[] error = {"No hay gastos disponibles"};

        if (!Expense_List.isEmpty()) {
            ListViewAdapterModifyBudgetedExpenses AdapterExpense = new ListViewAdapterModifyBudgetedExpenses(
                    rootview.getContext(),
                    R.layout.modify_expense_listview_layout,
                    rootview,
                    Expense_List);
            Expense_ListView.setAdapter(AdapterExpense);
        }
        else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
            Expense_ListView.setAdapter(errorAdapter);
        }
    }

    public void InitViews(View rootview){

        Expense_ListView = (ListView)   rootview.findViewById(R.id.expense_modify_listview);
        descriptionExpenseEditText = (EditText) rootview.findViewById(R.id.descripction_expense);
        amountExpenseEditText = (EditText) rootview.findViewById(R.id.amount_expense);
        categorySpinner = (Spinner) rootview.findViewById(R.id.categories_spinner);
        DateHappened_EditText = (EditText) rootview.findViewById(R.id.DateHappened);
    }

}
