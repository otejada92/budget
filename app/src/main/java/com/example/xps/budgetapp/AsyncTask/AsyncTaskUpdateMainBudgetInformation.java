package com.example.xps.budgetapp.AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyBudget;
import com.example.xps.budgetapp.Processes.BudgetProcesses;

import java.util.HashMap;

/**
 * Created by XPS on 4/3/2015.
 */
public class AsyncTaskUpdateMainBudgetInformation extends AsyncTask<Void,Void,Boolean> {

    private Activity ACTIVITY;
    private HashMap<String,String> BUDGET_INFORMATION = new  HashMap<String,String>();

    public AsyncTaskUpdateMainBudgetInformation(Activity activity,HashMap<String,String> budget_information) {

        this.ACTIVITY = activity;
        this.BUDGET_INFORMATION = budget_information;

    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        ContextStrategyBudget strategyBudget = new ContextStrategyBudget(new BudgetProcesses());

        return strategyBudget.executeModifyMainBudgetInformation(this.ACTIVITY,this.BUDGET_INFORMATION);

    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        Toast toast;

        if(aBoolean) {
            toast = Toast.makeText(this.ACTIVITY.getBaseContext(), "Se ha acutalizado la información del presupuesto.", Toast.LENGTH_LONG);
            toast.show();
        }

    }
}


