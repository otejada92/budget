package com.example.xps.budgetapp.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.example.xps.budgetapp.AlarmServicesConfigurations.MonthBudgetCheckerAlarm;
import com.example.xps.budgetapp.Broadcast.DimissDailyFixedIncomeExpenseNotification;
import com.example.xps.budgetapp.Broadcast.DimissMonthCheckerNotification;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by XPS on 7/7/2015.
 */
public class MonthBudgetCheckerNotificationService extends Service {

    ManagerDate managerDate = new ManagerDate();
    MonthBudgetCheckerAlarm monthBudgetCheckerAlarm;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        Calendar calendarInstance = Calendar.getInstance();

        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();


        startDate.set(Calendar.DAY_OF_MONTH, calendarInstance.getActualMinimum(Calendar.DAY_OF_MONTH));
        endDate.set(Calendar.DAY_OF_MONTH, calendarInstance.getActualMaximum(Calendar.DAY_OF_MONTH));

        String endDateStr = managerDate.parseCalendarToStringSqlFormat(endDate);
        String startDateStr = managerDate.parseCalendarToStringSqlFormat(startDate);

        String idAlarm = "";

        try {
            idAlarm = intent.getStringExtra("idAlarm");
        } catch (NullPointerException ignored) {
        }


        try {
            if (managerDate.areDateEquals(startDateStr, endDateStr)) {
                endDate.add(Calendar.DAY_OF_MONTH, 1);
                endDate.set(Calendar.DAY_OF_MONTH, calendarInstance.getActualMaximum(Calendar.DAY_OF_MONTH));

                endDateStr = managerDate.parseCalendarToStringSqlFormat(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        boolean existBudget = BudgetDataController.verifyBudgetOnDateRange(getBaseContext(), startDateStr, endDateStr);

        if (!idAlarm.equals("")) {
            if (!existBudget) {

                Intent dismissIntent = new Intent(this, DimissMonthCheckerNotification.class);
                dismissIntent.putExtra("notificationId", startId);
                dismissIntent.putExtra("idAlarm", String.valueOf(idAlarm));

                PendingIntent piDismiss = PendingIntent.getBroadcast(this, 0, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent acceptIntent = new Intent(this, CreateBudgetByMonthService.class);
                acceptIntent.putExtra("START_DATE", startDateStr);
                acceptIntent.putExtra("END_DATE", endDateStr);

                PendingIntent piAccept = PendingIntent.getService(this, 1, acceptIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.setSmallIcon(R.drawable.status_dialog_warning_icon)
                        .addAction(R.drawable.ic_action_cancel_black, "Cancelar", piDismiss)
                        .addAction(R.drawable.ic_action_accept_black, "Aceptar", piAccept)
                        .setContentTitle("Budget Helper")
                        .setContentText("No se han encontrado presupuestos...")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle("Budget Helper")
                                .bigText("No se han encontrado presupuestos para la fecha desde "
                                        + ManagerDate.ConvertToUserDateFormat(startDateStr) + " hasta "
                                        + ManagerDate.ConvertToUserDateFormat(endDateStr) + ". " +
                                        "Deseas crear uno entre ese rango y " +
                                        "sincronizado con los ingresos y gastos fijos?"));
                mBuilder.setDefaults(Notification.DEFAULT_SOUND);
                notificationManager.notify(startId, mBuilder.build());
            } else {
                monthBudgetCheckerAlarm = new MonthBudgetCheckerAlarm(getBaseContext(), true, 1);
                monthBudgetCheckerAlarm.setMonthBudgetChecker();
            }
        }
        return START_STICKY;
    }

}

