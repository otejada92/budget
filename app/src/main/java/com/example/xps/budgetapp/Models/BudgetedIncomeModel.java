package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 11/29/2014.
 */
public class BudgetedIncomeModel {

    private String ID_INCOME;
    private String DESCRIPTION;
    private String AMOUNT;
    private String ID_BUDGET;
    private String BUDGETED_DATE;

    public BudgetedIncomeModel(String id_income, String description, String amount, String id_budget,String budgeted_date) {
        super();

        this.ID_INCOME = id_income;
        this.DESCRIPTION = description;
        this.AMOUNT = amount;
        this.ID_BUDGET = id_budget;
        this.BUDGETED_DATE = budgeted_date;
    }

    public void setID_INCOME(String value) {
        ID_INCOME = value;
    }

    public String getID_INCOME() {
        return ID_INCOME;
    }

    public void setDESCRIPTION(String value) {
        DESCRIPTION = value;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setAMOUNT(String value) {
        AMOUNT = value;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public String getID_BUDGET() {
        return ID_BUDGET;
    }

    public void setID_BUDGET(String ID_BUDGET) {
        this.ID_BUDGET = ID_BUDGET;
    }

    public String getBUDGETED_DATE() {
        return BUDGETED_DATE;
    }

    public void setBUDGETED_DATE(String BUDGETED_DATE) {
        this.BUDGETED_DATE = BUDGETED_DATE;
    }
}
