package com.example.xps.budgetapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.example.xps.budgetapp.R;

/**
 * Created by XPS on 8/10/2015.
 */
public class SyncHelpDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        View dialogLayout = getActivity().getLayoutInflater().inflate(R.layout.sync_dialog_help,null);

        return new AlertDialog.Builder(getActivity())
                .setView(dialogLayout)
                .setTitle("Atencion")
                .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                }).create();
    }


}
