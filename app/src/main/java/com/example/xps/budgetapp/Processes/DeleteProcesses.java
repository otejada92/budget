package com.example.xps.budgetapp.Processes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Interfaces.DeleteStrategy;

/**
 * Created by XPS on 6/24/2015.
 */
public class DeleteProcesses implements DeleteStrategy{


    @Override
    public void DeleteResource(Context context, String tableName, String whereClause, String[] whereArgs) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        tableManager.delete(tableName,whereClause,whereArgs);

    }
}
