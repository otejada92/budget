package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;

/**
 * Created by XPS on 10/25/2014.
 */
public class Budget_Table implements BaseColumns {

    public static final String TABLE_NAME = "PRESUPUESTO_TABLE";
    public static final String ID_BUDGET = "id_presupuesto";
    public static final String BUDGET_DESCRIPTION = "descripcion_presupuesto";
    public static final String BUDGET_GOAL = "meta_presupuesto";
    public static final String INIT_DATE = "fecha_inicio";
    public static final String END_DATE = "fecha_fin";
    public static final String BUDGET_STATUS = "estado_presupuesto";
    public static final String SINCRONIZADO = "sincronizado";
    public static final String ID_ALARM_START_PROCESS = "id_alarma_comienzo";
    public static final String ID_ALARM_END_PROCESS = "id_alarma_finalizacion";


}
