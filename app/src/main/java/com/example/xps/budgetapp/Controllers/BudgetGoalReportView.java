package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.xps.budgetapp.CustomAdapters.CustomGoalReportAdapter;
import com.example.xps.budgetapp.Models.GoalBudgetReportModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class BudgetGoalReportView extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_goal_report_view);


        ListView ListViewContainer = (ListView) findViewById(R.id.ListViewContainer);
        String id_budget = getIntent().getStringExtra("ID_BUDGET");

        ArrayList<GoalBudgetReportModel> chartInformation = BudgetDataController.getGoalBudgetInformation(getBaseContext(), id_budget);

        if(!chartInformation.isEmpty()) {
            CustomGoalReportAdapter adapter = new CustomGoalReportAdapter(
                    getBaseContext(),
                    R.layout.budget_goal_listview_layout,
                    R.id.BarChart,
                    R.id.ResultTable,
                    R.id.Month,
                    chartInformation);
            ListViewContainer.setAdapter(adapter);
        }else {
            String[] errorMessage = {"No hay data por el momento"};
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,
                    errorMessage);
            ListViewContainer.setAdapter(errorAdapter);
        }

    }


}
