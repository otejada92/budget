package com.example.xps.budgetapp.Models;

import java.util.ArrayList;

/**
 * Created by XPS on 8/1/2015.
 */
public class LineExpenseModel {

        private String EXPENSESUM;
        private String BUDGETED_HAPPENED;
        private ArrayList<BudgetedExpenseModel> EXPENSELIST = new ArrayList<BudgetedExpenseModel>();

        public LineExpenseModel(String incomeSum, String dateHappened, ArrayList<BudgetedExpenseModel> expenseList) {
            super();

            this.EXPENSESUM = incomeSum;
            this.BUDGETED_HAPPENED = dateHappened;
            this.EXPENSELIST = expenseList;
        }

        public String getEXPENSESUM() {
            return EXPENSESUM;
        }

        public void setEXPENSESUM(String EXPENSESUM) {
            this.EXPENSESUM = EXPENSESUM;
        }

        public String getBUDGETED_HAPPENED() {
            return BUDGETED_HAPPENED;
        }

        public void setBUDGETED_HAPPENED(String BUDGETED_HAPPENED) {
            this.BUDGETED_HAPPENED = BUDGETED_HAPPENED;
        }


    public ArrayList<BudgetedExpenseModel> getEXPENSELIST() {
        return EXPENSELIST;
    }

    public void setEXPENSELIST(ArrayList<BudgetedExpenseModel> EXPENSELIST) {
        this.EXPENSELIST = EXPENSELIST;
    }
}
