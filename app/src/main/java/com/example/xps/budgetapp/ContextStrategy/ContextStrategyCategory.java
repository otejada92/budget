package com.example.xps.budgetapp.ContextStrategy;

import android.content.Context;

import com.example.xps.budgetapp.Interfaces.CategoryStrategy;

/**
 * Created by XPS on 3/15/2015.
 */
public class ContextStrategyCategory {

    private CategoryStrategy STRATEGY;

    public ContextStrategyCategory(CategoryStrategy strategy) {

        this.STRATEGY = strategy;
    }

    public void executeSaveCategory(Context context,String description){
        this.STRATEGY.Save(context,description);
    }

    public void executeModifyCategory(Context context,String id_category,String description){
        this.STRATEGY.Modify(context,id_category,description);
    }

}
