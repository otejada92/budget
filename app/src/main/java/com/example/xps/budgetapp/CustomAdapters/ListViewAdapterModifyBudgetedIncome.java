package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Fragments.ModifyBudgetedIncomesFragment;
import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.Processes.DeleteProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;

import java.util.ArrayList;


public class ListViewAdapterModifyBudgetedIncome extends ArrayAdapter<BudgetedIncomeModel> {

    private Context CONTEXT;
    private ArrayList<BudgetedIncomeModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;

    public EditText Description_EditText;
    public EditText Amount_EdiText;
    private EditText DateHappened_EditText;
    int editButtonIndex = 0;


    public ListViewAdapterModifyBudgetedIncome(Context context, int resource, View rootview, ArrayList<BudgetedIncomeModel> Data){
        super(context, resource,Data);
        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;

        }
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder Holder;
        final Cleaner clean_helper = new Cleaner();

        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView=inflater.inflate(this.RESOURCE,parent,false);
            Holder=new ViewHolder();

            Holder.textViewDesc=(TextView)convertView.findViewById(R.id.descripcion);
            Holder.textViewAmount=(TextView)convertView.findViewById(R.id.amount);
            Holder.update_button=(ImageButton) convertView.findViewById(R.id.Editar);
            Holder.delete_button =(ImageButton) convertView.findViewById(R.id.Borrar);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }

        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getDESCRIPTION();
        String amountString = "<b>Monto:$ </b>"+ this.DATA.get(position).getAMOUNT();

        Holder.textViewDesc.setId(Integer.parseInt(this.DATA.get(position).getID_INCOME()));

        Holder.textViewDesc.setText(Html.fromHtml(descString));

        Holder.textViewAmount.setText(Html.fromHtml(amountString));

        Holder.update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                initViews();

                Description_EditText.setEnabled(true);
                Amount_EdiText.setEnabled(true);
                DateHappened_EditText.setEnabled(true);
                ModifyBudgetedIncomesFragment.menuActionbar.getItem(editButtonIndex).setEnabled(true);

                ModifyBudgetedIncomesFragment.idIncome = DATA.get(position).getID_INCOME();

                Description_EditText.setText(DATA.get(position).getDESCRIPTION());
                Amount_EdiText.setText(DATA.get(position).getAMOUNT().trim());
                DateHappened_EditText.setText(ManagerDate.ConvertToUserDateFormat(DATA.get(position).getBUDGETED_DATE()));
            }
        });

        Holder.delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


        final String whereClause = Income_Budget_Table.ID_INCOME + " = ?" + " AND " + Income_Budget_Table.ID_BUDGET + " = ? ";
        final String[] whereArgs = new String[] { DATA.get(position).getID_INCOME(),DATA.get(position).getID_BUDGET() };

        LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);

        TextView textView = (TextView) rootView.findViewById(R.id.Advice);
        textView.setText("Estas seguro que deseas borrar este ingreso presupuestado?, este sera borrado y desvinculado para siempre del presupuesto correspondiente!");

        AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
        builder.setTitle("Aviso")
                .setView(rootView)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                ContextStrategyDeleteResource contextDelete = new ContextStrategyDeleteResource(new DeleteProcesses());
                                contextDelete.executeStrategy(rootView.getContext(),Income_Budget_Table.TABLE_NAME,whereClause,whereArgs);

                                notifyDataSetChanged();
                                clean_helper.cleanView((ViewGroup) ROOTVIEW);
                                initViews();

                                Description_EditText.setEnabled(false);
                                Amount_EdiText.setEnabled(false);

                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                ).show();
                }
        });
        return convertView;
    }

    static class ViewHolder {

        TextView textViewDesc;
        TextView textViewAmount;
        ImageButton  update_button;
        ImageButton delete_button;

    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        ModifyBudgetedIncomesFragment.populateIncomeListView(ROOTVIEW);
    }

    public void initViews() {



        Description_EditText = (EditText) ROOTVIEW.findViewById(R.id.descripction_income);
        Amount_EdiText = (EditText) ROOTVIEW.findViewById(R.id.amount_income);
        DateHappened_EditText = (EditText) ROOTVIEW.findViewById(R.id.DateHappened);

    }

}
