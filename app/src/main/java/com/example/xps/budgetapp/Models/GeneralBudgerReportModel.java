package com.example.xps.budgetapp.Models;

import java.util.ArrayList;

/**
 * Created by XPS on 5/6/2015.
 */
public class GeneralBudgerReportModel {

    private String MONTH;
    private ArrayList<BudgetedIncomeModel> BUDGETED_INCOME;
    private ArrayList<BudgetedExpenseModel> BUDGETED_EXPENSE;


    public GeneralBudgerReportModel(String MONTH,ArrayList<BudgetedIncomeModel> BUDGETED_INCOME,ArrayList<BudgetedExpenseModel> BUDGETED_EXPENSE) {

        this.MONTH = MONTH;
        this.BUDGETED_INCOME = BUDGETED_INCOME;
        this.BUDGETED_EXPENSE = BUDGETED_EXPENSE;
    }


    public ArrayList<BudgetedIncomeModel> getBUDGETED_INCOME() {
        return BUDGETED_INCOME;
    }

    public void setBUDGETED_INCOME(ArrayList<BudgetedIncomeModel> budgeted_income) {
        this.BUDGETED_INCOME = budgeted_income;
    }

    public ArrayList<BudgetedExpenseModel> getBUDGETED_EXPENSE() {
        return BUDGETED_EXPENSE;
    }

    public void setBUDGETED_EXPENSE(ArrayList<BudgetedExpenseModel> budgeted_expense) {
        this.BUDGETED_EXPENSE = budgeted_expense;
    }

    public String getMONTH() {
        return MONTH;
    }

    public void setMONTH(String MONTH) {
        this.MONTH = MONTH;
    }
}
