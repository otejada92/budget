package com.example.xps.budgetapp.ContextStrategy;

import android.content.ContentValues;
import android.content.Context;

import com.example.xps.budgetapp.Interfaces.InsertStrategy;

/**
 * Created by XPS on 7/4/2015.
 */
public class ContextStrategyInsertResource {

    InsertStrategy STRATEGY;
    public ContextStrategyInsertResource(InsertStrategy strategy) {

        this.STRATEGY = strategy;
    }

    public void executeStategy(Context context,String tableName,ContentValues resourceValues){
        this.STRATEGY.InsertResource(context,tableName,resourceValues);
    };
}
