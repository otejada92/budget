package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 5/9/2015.
 */
public class DistributionCategoryTotals {

    private String category;
    private String totalExpense;
    private String idCategory;

    public DistributionCategoryTotals(String category, String totalExpense,String idCategory) {
        this.category = category;
        this.totalExpense = totalExpense;
        this.idCategory = idCategory;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIdCategory() {
        return idCategory;
    }
}
