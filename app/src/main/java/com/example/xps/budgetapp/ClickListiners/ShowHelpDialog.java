package com.example.xps.budgetapp.ClickListiners;

import android.app.FragmentManager;
import android.view.View;

import com.example.xps.budgetapp.Dialogs.DayHappenedHelpDialog;
import com.example.xps.budgetapp.Dialogs.SyncHelpDialog;

/**
 * Created by XPS on 7/13/2015.
 */
public class ShowHelpDialog implements View.OnClickListener {

    public FragmentManager fragmentManager;
    public String dialogForShow;

    public ShowHelpDialog(FragmentManager fragmentManager, String dialogForShow) {
        this.fragmentManager = fragmentManager;
        this.dialogForShow = dialogForShow;
    }

    @Override
    public void onClick(View view) {

        if(this.dialogForShow.equals("dayHappenedHelp")) {
            DayHappenedHelpDialog dialog = new DayHappenedHelpDialog();
            dialog.show(this.fragmentManager, "Help");
        }

        if(this.dialogForShow.equals("syncHelp")){
            SyncHelpDialog dialog = new SyncHelpDialog();
            dialog.show(this.fragmentManager, "Help");
        }
    }
}
