package com.example.xps.budgetapp.AlarmServicesConfigurations;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Broadcast.MonthBudgetCheckerReceiver;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.util.Calendar;

/**
 * Created by XPS on 7/7/2015.
 */
public class MonthBudgetCheckerAlarm {

    public boolean AVANCE_DAYS;
    public Context context;
    public int DAYS_FOR_AVANCES;
    public ManagerDate managerDate = new ManagerDate();


    public MonthBudgetCheckerAlarm(Context context,boolean avance_days,int days_for_avances) {

        this.context = context;
        this.AVANCE_DAYS = avance_days;
        this.DAYS_FOR_AVANCES = days_for_avances;

    }

    public  void  setMonthBudgetChecker(){

        Calendar timeNow = Calendar.getInstance();
        Calendar scheduleTime = Calendar.getInstance();
        int idAlarm = managerDate.getAlarmId();

        if (!this.AVANCE_DAYS) {

            scheduleTime.set(Calendar.HOUR, 12);
            scheduleTime.set(Calendar.HOUR_OF_DAY, 0);
            scheduleTime.set(Calendar.MINUTE,0);
            scheduleTime.set(Calendar.SECOND,0);

            if (scheduleTime.before(timeNow)){

                scheduleTime.add(Calendar.DAY_OF_MONTH, 1);
                scheduleTime.set(Calendar.HOUR, 12);
                scheduleTime.set(Calendar.HOUR_OF_DAY,0);
                scheduleTime.set(Calendar.MINUTE,0);
                scheduleTime.set(Calendar.SECOND,0);
            }

        }
        else{
            scheduleTime.add(Calendar.DAY_OF_MONTH, this.DAYS_FOR_AVANCES);

            scheduleTime.set(Calendar.HOUR_OF_DAY, 8);
            scheduleTime.set(Calendar.HOUR, 8);
            scheduleTime.set(Calendar.MINUTE,0);
            scheduleTime.set(Calendar.SECOND,0);

        }

        Intent intentReciver = new Intent(this.context,MonthBudgetCheckerReceiver.class);
        intentReciver.putExtra("idAlarm",String.valueOf(idAlarm));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.context, idAlarm, intentReciver, PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC, scheduleTime.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC, scheduleTime.getTimeInMillis(), pendingIntent);
        }

    }
}
