package com.example.xps.budgetapp.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyExpense;
import com.example.xps.budgetapp.Controllers.ExpenseDataController;
import com.example.xps.budgetapp.Fragments.AddExpenseToBudgetFragment;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Fragments.ModifyBudgetedExpensesFragment;
import com.example.xps.budgetapp.Fragments.ModifyExpenseFragment;
import com.example.xps.budgetapp.Processes.ExpenseProcesses;

import java.util.HashMap;

/**
 * Created by XPS on 2/13/2015.
 */
public class AsyncTaskExpenseProcess extends AsyncTask<Void,Void,Boolean> {

    private View ROOTVIEW;
    private HashMap<String,String> EXPENSE_INFORMATION;
    private Context CONTEXT;
    public AsyncTaskExpenseProcess(View rootview, HashMap<String,String> expense_information,Context context) {

        this.ROOTVIEW = rootview;
        this.CONTEXT = context;
        this.EXPENSE_INFORMATION = expense_information;

    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        ContextStrategyExpense contextStrategyExpense = new ContextStrategyExpense(new ExpenseProcesses());

        if(EXPENSE_INFORMATION.get("saveState").equals("true")){
            if(EXPENSE_INFORMATION.get("DO").equals("SAVE") ){
                contextStrategyExpense.executeprocessAdd(
                        this.CONTEXT,
                        EXPENSE_INFORMATION.get("description"),
                        EXPENSE_INFORMATION.get("amount"),
                        EXPENSE_INFORMATION.get("recurrence"),
                        EXPENSE_INFORMATION.get("idCategory"),
                        EXPENSE_INFORMATION.get("dayHappened"));
                return true;
            }
            
            if(EXPENSE_INFORMATION.get("DO").equals("MODIFY")) {
                contextStrategyExpense.executeprocessModify(this.CONTEXT,
                        EXPENSE_INFORMATION.get("idExpense"),
                        EXPENSE_INFORMATION.get("description"),
                        EXPENSE_INFORMATION.get("amount"),
                        EXPENSE_INFORMATION.get("recurrence"),
                        EXPENSE_INFORMATION.get("idCategory"),
                        EXPENSE_INFORMATION.get("dayHappened"));
                return true;
            }
            if(this.EXPENSE_INFORMATION.get("DO").equals("ADDTOBUDGET")) {
                ExpenseDataController.AddExpenseToBudget(this.CONTEXT,
                        EXPENSE_INFORMATION.get("description"),
                        EXPENSE_INFORMATION.get("amount"),
                        EXPENSE_INFORMATION.get("idCategory"),
                        EXPENSE_INFORMATION.get("idBudget"),
                        EXPENSE_INFORMATION.get("dateHappened"),
                        EXPENSE_INFORMATION.get("categoryDesc"));
                ContextStrategyExpense context = new ContextStrategyExpense(new ExpenseProcesses());
                String[] date = EXPENSE_INFORMATION.get("dateHappened").split("-");

                String day = date[1];
                context.executeprocessAdd(ROOTVIEW.getContext(),EXPENSE_INFORMATION.get("description"),EXPENSE_INFORMATION.get("amount"),"1",EXPENSE_INFORMATION.get("idCategory"),day);

                return true;
            }
            if(this.EXPENSE_INFORMATION.get("DO").equals("MODIFYOFBUDGET")) {
                ExpenseDataController.modifyBudgetedExpense(this.CONTEXT,
                        EXPENSE_INFORMATION.get("idExpense"),
                        EXPENSE_INFORMATION.get("description"),
                        EXPENSE_INFORMATION.get("amount"),
                        EXPENSE_INFORMATION.get("idCategory"),
                        EXPENSE_INFORMATION.get("idBudget"),
                        EXPENSE_INFORMATION.get("dateHappened"),
                        EXPENSE_INFORMATION.get("categoryDesc"));
                return true;
            }

        }
        return false;
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {

        Toast toast;
        Cleaner  clean_helper = new Cleaner();
        if (aBoolean) {
            if (this.EXPENSE_INFORMATION.get("DO").equals("SAVE")) {
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Se ha guardado su gasto.", Toast.LENGTH_LONG);
                toast.show();
                clean_helper.cleanView((RelativeLayout) this.ROOTVIEW);
            }
            if(this.EXPENSE_INFORMATION.get("DO").equals("MODIFY")) {
                clean_helper.cleanView((RelativeLayout) ROOTVIEW);
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Se ha actualizado su gasto.", Toast.LENGTH_LONG);
                toast.show();
                ModifyExpenseFragment.populateExpenseListView(ROOTVIEW);
            }
            if(this.EXPENSE_INFORMATION.get("DO").equals("ADDTOBUDGET")){
                clean_helper.cleanView((RelativeLayout) ROOTVIEW);
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Se gasto ha sido presupuestado.", Toast.LENGTH_SHORT);
                AddExpenseToBudgetFragment.PopulateExpenseListView((RelativeLayout) ROOTVIEW);
                toast.show();
            }
            if(this.EXPENSE_INFORMATION.get("DO").equals("MODIFYOFBUDGET")){
                clean_helper.cleanView((RelativeLayout) ROOTVIEW);
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Su gasto ha sido actualizado.", Toast.LENGTH_SHORT);
                toast.show();
                ModifyBudgetedExpensesFragment.PopulateExpensesListView(this.ROOTVIEW);
            }

        }
    }


}