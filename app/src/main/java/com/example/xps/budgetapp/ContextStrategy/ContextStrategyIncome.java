package com.example.xps.budgetapp.ContextStrategy;

import android.content.Context;

import com.example.xps.budgetapp.Interfaces.IncomeStrategy;


public class ContextStrategyIncome {

    private IncomeStrategy STRATEGY;

    public ContextStrategyIncome(IncomeStrategy strategy){
        this.STRATEGY = strategy;
    }

    public void executeStrategyAdd(Context context, String Description, String Amount, String Recurrence,String DayHappened){
        STRATEGY.Save(context,Description,Amount,Recurrence,DayHappened);
    }
    public void executeStrategyModify(Context context, String id_income, String Description, String Amount, String Recurrence,String DayHappened){
        STRATEGY.Modify(context,id_income,Description,Amount,Recurrence,DayHappened);
    }
}
