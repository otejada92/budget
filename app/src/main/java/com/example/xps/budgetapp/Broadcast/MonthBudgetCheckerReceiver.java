package com.example.xps.budgetapp.Broadcast;

/**
 * Created by XPS on 7/7/2015.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Services.MonthBudgetCheckerNotificationService;

public class MonthBudgetCheckerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String idAlarm = intent.getStringExtra("idAlarm");

        Intent serviceIntent = new Intent(context,MonthBudgetCheckerNotificationService.class);
        serviceIntent.putExtra("idAlarm",String.valueOf(idAlarm));
        context.startService(serviceIntent);
    }
}
