package com.example.xps.budgetapp.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;

import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.Models.LineIncomeModel;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;
import com.example.xps.budgetapp.Tables.Income_Table;

import java.util.ArrayList;

/**
 * Created by Osvaldo on 11/12/2014.
 */
public class IncomeDataController {

    public static ArrayList<IncomeModel> getIncomesInformation(Context context) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        int idIncomeIndex = 0;
        int descriptionIndex = 1;
        int amountIndex = 2;
        int recurrenceIndex = 3;
        int dateIssue = 4;

        Cursor incomeCursor = tableManager.rawQuery(
                "SELECT " + Income_Table.ID_INCOME + ","
                        + Income_Table.DESCRIPCTION + ","
                        + Income_Table.AMOUNT + ","
                        +Income_Table.RECURRENCE+","
                        +Income_Table.DAY_ISSUE +
                        " FROM " + Income_Table.TABLE_NAME , null);

        ArrayList<IncomeModel> incomeList = new ArrayList<IncomeModel>();
        if (incomeCursor.moveToFirst()) {
            do {
                incomeList.add(new IncomeModel(incomeCursor.getString(idIncomeIndex),
                        incomeCursor.getString(descriptionIndex),
                        incomeCursor.getString(amountIndex),
                        incomeCursor.getString(recurrenceIndex),
                        incomeCursor.getString(dateIssue)));
            } while (incomeCursor.moveToNext());
        }
        incomeCursor.close();
        db.close();
        return incomeList;
    }

    public static void modifyBudgetedIncome(View rootView, String idIncome,String description, String amount,String idBudget,String dateHappened) {

        DataBaseController db = new DataBaseController(rootView.getContext());
        SQLiteDatabase tableManager = db.getWritableDatabase();

        ContentValues data;

        dateHappened = ManagerDate.ConvertToSqlLiteDateFormat(dateHappened);
        String where = Income_Budget_Table.ID_INCOME + " = ? "+
                "AND "+ Income_Budget_Table.ID_BUDGET +" = ?";
        String[] wherearg = {idIncome,idBudget};


         data = setIncomeContentValue(description,amount,idBudget,dateHappened);

        if (data.size() != 0) {
            tableManager.update(Income_Budget_Table.TABLE_NAME,
                    data,
                    where,
                    wherearg);
        }
    }

    public static void AddIncomeToBudget(Context context, String description, String amount, String idBudget,String dateHappened){

        ContentValues data;

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();


        dateHappened = ManagerDate.ConvertToSqlLiteDateFormat(dateHappened);

        data = setIncomeContentValue(description, amount, idBudget, dateHappened);

        if (data.size() != 0) {

            table_manager.insert(Income_Budget_Table.TABLE_NAME, null, data);

            }

    }

    public static ContentValues setIncomeContentValue(String description, String amount, String idBudget, String dateHappened){

        ContentValues Income_Content_Value = new ContentValues();

        Income_Content_Value.put(Income_Budget_Table.ID_BUDGET,idBudget);
        Income_Content_Value.put(Income_Budget_Table.DESCRIPTION,description);
        Income_Content_Value.put(Income_Budget_Table.AMOUNT,amount);
        Income_Content_Value.put(Income_Budget_Table.DATE_INSERTED,dateHappened);

        return Income_Content_Value;
    }

    public static ArrayList<BudgetedIncomeModel> getIncomesByBudget(Context context, String idBudget){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        ArrayList<BudgetedIncomeModel> incomeList = new ArrayList<BudgetedIncomeModel>();

        Cursor budgetedIncomeCursor = tableManager.rawQuery("SELECT "+
                Income_Budget_Table.ID_INCOME +","+
                Income_Budget_Table.DESCRIPTION +","+
                Income_Budget_Table.AMOUNT +","+
                Income_Budget_Table.ID_BUDGET+","+
                Income_Budget_Table.DATE_INSERTED +
                " FROM "+ Income_Budget_Table.TABLE_NAME+
                " WHERE " + Income_Budget_Table.ID_BUDGET + " = ? "+
               " ORDER BY "+ Income_Budget_Table.DATE_INSERTED +" ASC",new String[]{idBudget});


        if(budgetedIncomeCursor.moveToFirst()){
            do{
                incomeList.add(new BudgetedIncomeModel(budgetedIncomeCursor.getString(0),
                        budgetedIncomeCursor.getString(1),
                        budgetedIncomeCursor.getString(2),
                        budgetedIncomeCursor.getString(3),
                        budgetedIncomeCursor.getString(4)));

            }while (budgetedIncomeCursor.moveToNext());

        }
        budgetedIncomeCursor.close();
        db.close();
        return incomeList;
    }

    public  ArrayList<BudgetedIncomeModel> getIncomesForGeneralBudgetReport(Context context, String idBudget, String startDateMonth, String endDateMonth){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        ArrayList<BudgetedIncomeModel> incomeList = new ArrayList<BudgetedIncomeModel>();

        Cursor budgetedIncomeCursor = tableManager.rawQuery("SELECT "+
                Income_Budget_Table.ID_INCOME +","+
                Income_Budget_Table.DESCRIPTION +","+
                Income_Budget_Table.AMOUNT +","+
                Income_Budget_Table.ID_BUDGET+","+
                Income_Budget_Table.DATE_INSERTED +
                " FROM "+ Income_Budget_Table.TABLE_NAME+
                " WHERE "
                + Income_Budget_Table.ID_BUDGET +" = ?"+
                " AND "+Income_Budget_Table.DATE_INSERTED +
                " BETWEEN "+"'"+startDateMonth+"'"+" AND "+"'"+endDateMonth+"'"+
               " ORDER BY "+ Income_Budget_Table.DATE_INSERTED +" DESC",new String[]{idBudget});

        if(budgetedIncomeCursor.moveToFirst()){
            do{
                incomeList.add(new BudgetedIncomeModel(budgetedIncomeCursor.getString(0),
                        budgetedIncomeCursor.getString(1),
                        budgetedIncomeCursor.getString(2),
                        budgetedIncomeCursor.getString(3),
                        budgetedIncomeCursor.getString(4)));

            }while (budgetedIncomeCursor.moveToNext());
        }

        budgetedIncomeCursor.close();
        db.close();
        return incomeList;
    }

    public  ArrayList<IncomeModel> getFixedIncomesInformation(Context context, String currentDay) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        int idIncomeIndex = 0;
        int descriptionIndex = 1;
        int amountIndex = 2;
        int recurrenceIndex = 3;
        int dateIssue = 4;

        Cursor incomeCursor = table_manager.rawQuery(
                "SELECT " + Income_Table.ID_INCOME + ","
                        + Income_Table.DESCRIPCTION + ","
                        + Income_Table.AMOUNT + ","
                        +Income_Table.RECURRENCE+","
                        +Income_Table.DAY_ISSUE +
                        " FROM " + Income_Table.TABLE_NAME+
                        " WHERE "+Income_Table.RECURRENCE+ " = 1"+
                        " AND "+Income_Table.DAY_ISSUE + " LIKE"+"'%"+currentDay+"%'",null);

        ArrayList<IncomeModel> income_list = new ArrayList<IncomeModel>();
        if (incomeCursor.moveToFirst()) {
            do {
                income_list.add(new IncomeModel(incomeCursor.getString(idIncomeIndex),
                        incomeCursor.getString(descriptionIndex),
                        incomeCursor.getString(amountIndex),
                        incomeCursor.getString(recurrenceIndex),
                        incomeCursor.getString(dateIssue)));
            } while (incomeCursor.moveToNext());
        }
        incomeCursor.close();
        db.close();
        return income_list;
    }

    public static ArrayList<LineIncomeModel> getBudgetedIncomeBy(Context context, String fromDateStr, String toDateStr) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        ArrayList<LineIncomeModel> linerIncomeInformation = new ArrayList<LineIncomeModel>();
        ArrayList<BudgetedIncomeModel> markViewInformation;

        Cursor budgetedIncomeCursor = tableManager.rawQuery("SELECT "+
                "SUM("+ Income_Budget_Table.AMOUNT + ")," +
                Income_Budget_Table.DATE_INSERTED+","+
                Income_Budget_Table.AMOUNT +
                " FROM "+ Income_Budget_Table.TABLE_NAME+
                " WHERE "+Income_Budget_Table.DATE_INSERTED +
                " BETWEEN "+"'"+fromDateStr+"'"+" AND "+"'"+toDateStr+"'"+
                " GROUP BY " + Income_Budget_Table.DATE_INSERTED+
                " ORDER BY "+ Income_Budget_Table.DATE_INSERTED +" ASC"
                ,null);

        if(budgetedIncomeCursor.moveToFirst()){
            do{

                markViewInformation = getBudgetedIncomeByDate(context, budgetedIncomeCursor.getString(1));
                linerIncomeInformation.add(new LineIncomeModel(budgetedIncomeCursor.getString(0), budgetedIncomeCursor.getString(1),markViewInformation));
            }while (budgetedIncomeCursor.moveToNext());
        }
        budgetedIncomeCursor.close();
        return linerIncomeInformation;
    }


    public static ArrayList<BudgetedIncomeModel> getBudgetedIncomeByDate(Context context, String date){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        ArrayList<BudgetedIncomeModel> incomeInformation = new ArrayList<BudgetedIncomeModel>();

        Cursor budgetedIncomeCursor = tableManager.rawQuery("SELECT "+
                Income_Budget_Table.ID_INCOME +","+
                Income_Budget_Table.DESCRIPTION +","+
                Income_Budget_Table.AMOUNT +","+
                Income_Budget_Table.ID_BUDGET+","+
                Income_Budget_Table.DATE_INSERTED+
                " FROM "+ Income_Budget_Table.TABLE_NAME+
                " WHERE "+Income_Budget_Table.DATE_INSERTED +
                " LIKE "+"'%"+date+"%'"
                ,null);

        if(budgetedIncomeCursor.moveToFirst()){
            do{
                incomeInformation.add(new BudgetedIncomeModel(budgetedIncomeCursor.getString(0),
                        budgetedIncomeCursor.getString(1),
                        budgetedIncomeCursor.getString(2),
                        budgetedIncomeCursor.getString(3),
                        budgetedIncomeCursor.getString(4)));

            }while (budgetedIncomeCursor.moveToNext());
        }
        budgetedIncomeCursor.close();
        return incomeInformation;

    }
}