package com.example.xps.budgetapp.Interfaces;

import android.content.Context;

/**
 * Created by XPS on 2/12/2015.
 */
public interface IncomeStrategy {

    void Save(Context context, String Description, String Amount, String Recurrence, String DayHappened);

    void Modify(Context context, String id_income, String Description, String Amount, String Recurrence, String DayHappened);
}
