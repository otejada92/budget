package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Switch;

import com.example.xps.budgetapp.ClickListiners.ShowHelpDialog;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Controllers.ExpenseDataController;
import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.Controllers.PrincipalMenuViewController;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterExpenseBudget;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterIncomeBudget;
import com.example.xps.budgetapp.CustomView.DatePicker;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.Processes.BudgetProcesses;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;


public class AddBudgetFragment extends Fragment{

    private View rootView;
    public static ListView expensesListView;
    public static ListView incomesListView;
    private EditText descriptionBudgetText;
    private EditText goalEditText;
    private EditText initDate;
    private EditText endDate;
    private Switch syncSwitch;
    BudgetDataController dataController = new BudgetDataController();
    private ImageButton helpButton;
    public static ArrayList<IncomeModel> IncomeToSave = new ArrayList<IncomeModel>();
    public static ArrayList<ExpenseModel> expenseToSave = new ArrayList<ExpenseModel>();

    public AddBudgetFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.add_budget_fragment_layout,container,false);

        this.rootView = rootView;

        InitView(rootView);


        incomesListView.setAdapter(PopulateIncomeListView(rootView));
        expensesListView.setAdapter(PopulateExpenseListView(rootView));

        initDate.setInputType(InputType.TYPE_NULL);
        endDate.setInputType(InputType.TYPE_NULL);

        initDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker datePicker = new DatePicker(rootView, (EditText) view);
                datePicker.show(getFragmentManager(), "InitDay");
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker datePicker = new DatePicker(rootView, (EditText) view);
                datePicker.show(getFragmentManager(), "EndDay");
            }
        });

        helpButton.setOnClickListener(new ShowHelpDialog(getFragmentManager(),"syncHelp"));

        syncSwitch.setTextOff("No");
        syncSwitch.setTextOn("Si");

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.budget_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        Toast toast;
        int id_item = item.getItemId();
        String messageError = "";

        if(id_item == R.id.add_button) {

            String descBudget = descriptionBudgetText.getText().toString();
            String goalBudget = goalEditText.getText().toString();
            String initDateValue = initDate.getText().toString();
            String endDateValue = endDate.getText().toString();

            BudgetProcesses executeBudgetFunction = new BudgetProcesses(rootView);
            messageError = dataController.vatidateBudgetInformation(descBudget, goalBudget, initDateValue, endDateValue);

            if(messageError.equals("")) {
                executeBudgetFunction.ExecuteSaveBudget(IncomeToSave, expenseToSave);
            }else{
                toast = Toast.makeText(rootView.getContext(),messageError.trim(),Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(id_item == android.R.id.home){

            Intent homeIntent = new Intent(getActivity().getBaseContext(), PrincipalMenuViewController.class);
            startActivity(homeIntent);
        }

        return true;
    }

    public static ArrayAdapter PopulateIncomeListView(final View rootView){

        incomesListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        final ArrayList<IncomeModel> Income_List = IncomeDataController.getIncomesInformation(rootView.getContext());
        String[] error = {"No hay ingresos disponibles."};

        if (Income_List.isEmpty()){

            ArrayAdapter<String> ErrorAdapter = new ArrayAdapter<String>(rootView.getContext(), android.R.layout.simple_list_item_1,error);
            return ErrorAdapter;

        }
        incomesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckBox income = (CheckBox) view;
                if (income.isChecked()) {
                    IncomeToSave.add(Income_List.get(i));
                } else {
                    IncomeToSave.remove(Income_List.get(i));
                }
            }
        });

        return new
                ListViewAdapterIncomeBudget(rootView.getContext(),
                R.layout.customadapaterlayout_id_desc_monto,
                R.id.checkboxC,
                rootView,
                Income_List);
    }

    public static ArrayAdapter PopulateExpenseListView(View rootView){


        expensesListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        final ArrayList<ExpenseModel> expenseList = ExpenseDataController.getExpenseInformation(rootView.getContext());
        String[] error = {"No hay gastos disponibles."};

        if (expenseList.isEmpty()){
            return new ArrayAdapter<String>(rootView.getContext(),
                    android.R.layout.simple_list_item_1,error);
        }
        expensesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckBox expense = (CheckBox) view;
                if (expense.isChecked()) {
                    expenseToSave.add(expenseList.get(i));
                } else {
                    expenseToSave.remove(expenseList.get(i));
                }
            }
        });

        return new ListViewAdapterExpenseBudget(rootView.getContext(),
                R.layout.customadapaterlayout_id_desc_monto,
                R.id.checkboxC, rootView, expenseList);
    }

    public void InitView(View rootview){

        descriptionBudgetText = (EditText) rootview.findViewById(R.id.DescriptionBudget);
        goalEditText = (EditText) rootview.findViewById(R.id.BudgetGoal);
        expensesListView = (ListView) rootview.findViewById(R.id.expense_listview);
        incomesListView = (ListView )rootview.findViewById(R.id.income_listview);
        initDate = (EditText) rootview.findViewById(R.id.Fecha_inicio);
        endDate = (EditText) rootview.findViewById(R.id.Fecha_fin);
        syncSwitch = (Switch) rootview.findViewById(R.id.PredeterminadoSwitch);
        helpButton = (ImageButton) rootview.findViewById(R.id.help_button);
    }

}
