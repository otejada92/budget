package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.xps.budgetapp.Fragments.CategoryFragment;
import com.example.xps.budgetapp.R;



public class CategoryViewController extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.category_fragment_layout);

        CategoryFragment  categoryFragment  = new CategoryFragment();

        FragmentManager fragmentManager  = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.add(R.id.category_layout,categoryFragment);
        transaction.commit();


    }


}
