package com.example.xps.budgetapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

/**
 * Created by XPS on 8/12/2015.
 */
public class IncomeLineaDetailDialog extends DialogFragment {

    ArrayList<BudgetedIncomeModel> incomeDetail;
    View dialogRootView;
    TextView incomeInformation;
    TextView totalIncome;
    float totalIncomeValue = (float) 0.0;
    public IncomeLineaDetailDialog(ArrayList<BudgetedIncomeModel> incomeDetail) {
        this.incomeDetail = incomeDetail;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        dialogRootView = getActivity().getLayoutInflater().inflate(R.layout.detail_information_layout,null);
        incomeInformation = (TextView) dialogRootView.findViewById(R.id.ViewInformation);
        totalIncome = (TextView) dialogRootView.findViewById(R.id.Total);


        String incomeDetail = "";

        for (BudgetedIncomeModel income : this.incomeDetail){
                incomeDetail += income.getDESCRIPTION()+" - "+"$"+income.getAMOUNT()+"\n";
            totalIncomeValue +=Integer.valueOf(income.getAMOUNT());
        }

        incomeInformation.setText(incomeDetail);
        totalIncome.setText("Total de ingresos: $"+String.valueOf(totalIncomeValue));


        return new AlertDialog.Builder(getActivity())
                .setView(dialogRootView)
                .setTitle("Detalle de ingresos")
                .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();

    }
}

