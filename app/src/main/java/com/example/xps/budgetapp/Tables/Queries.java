package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;

/**
 * Created by XPS on 10/12/2014.
 */
public class Queries implements BaseColumns {

    public static final String INCOME_QUERY = "CREATE TABLE " + Income_Table.TABLE_NAME + "(" +
            Income_Table.ID_INCOME + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            Income_Table.DESCRIPCTION + " TEXT,"+
            Income_Table.AMOUNT + " INTEGER," +
            Income_Table.RECURRENCE +" INTEGER,"+
            Income_Table.DAY_ISSUE +" TEXT)";

    public static final String EXPENSE_QUERY = "CREATE TABLE " + Expense_Table.TABLE_NAME +"("+
            Expense_Table.ID_EXPENSE + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            Expense_Table.DESCRIPCTION + " TEXT,"+
            Expense_Table.AMOUNT + " INTEGER,"+
            Expense_Table.ID_CATEGORY + " INTEGER,"+
            Expense_Table.RECURRENCE+" INTEGER,"+
            Expense_Table.DAY_ISSUE +" TEXT, "+
            " FOREIGN KEY ("+ Expense_Table.ID_CATEGORY +") REFERENCES "+ Category_Table.TABLE_NAME+" ("+ Category_Table.ID_CATEGORY +") ON DELETE SET NULL )";

    public static final String CATEGORY_QUERY = "CREATE TABLE " + Category_Table.TABLE_NAME + "("+
            Category_Table.ID_CATEGORY + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            Category_Table.DESCRIPTION + " TEXT)";

    public static final String BUDGET_QUERY = "CREATE TABLE "+ Budget_Table.TABLE_NAME+"("+
            Budget_Table.ID_BUDGET + " TEXT PRIMARY KEY,"+
            Budget_Table.BUDGET_DESCRIPTION+ " TEXT,"+
            Budget_Table.BUDGET_GOAL+ " BLOB,"+
            Budget_Table.INIT_DATE +" TEXT,"+
            Budget_Table.END_DATE +" TEXT,"+
            Budget_Table.BUDGET_STATUS +" INTEGER," +
            Budget_Table.ID_ALARM_START_PROCESS +" INTEGER,"+
            Budget_Table.ID_ALARM_END_PROCESS +" INTEGER,"+
            Budget_Table.SINCRONIZADO + " TEXT)";


    public static  final String BUDGETED_INCOMES_QUERY = "CREATE TABLE "+ Income_Budget_Table.TABLE_NAME+"("+
            Income_Budget_Table.ID_INCOME+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Income_Budget_Table.ID_BUDGET +" TEXT,"+
            Income_Budget_Table.DESCRIPTION + " INTEGER,"+
            Income_Budget_Table.AMOUNT +" INTEGER, " +
            Income_Budget_Table.DATE_INSERTED +" TEXT, "+
            " FOREIGN KEY ("+ Expense_Budget_Table.ID_BUDGET +") REFERENCES "+ Budget_Table.TABLE_NAME+" ("+ Budget_Table.ID_BUDGET +")ON DELETE CASCADE )";

    public static  final String BUDGTED_EXPENSES_QUERY = "CREATE TABLE "+ Expense_Budget_Table.TABLE_NAME+"("+
            Expense_Budget_Table.ID_EXPENSE+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            Expense_Budget_Table.ID_BUDGET+" TEXT,"+
            Expense_Budget_Table.DESCRIPTION + " INTEGER,"+
            Expense_Budget_Table.AMOUNT +" INTEGER," +
            Expense_Budget_Table.CATEGORY_DESC+" TEXT,"+
            Expense_Budget_Table.ID_CATEGORY+" INTEGER,"+
            Expense_Budget_Table.DATE_BDGETED +" TEXT, "
            +" FOREIGN KEY ("+ Expense_Budget_Table.ID_BUDGET +") REFERENCES "+ Budget_Table.TABLE_NAME+" ("+ Budget_Table.ID_BUDGET +") ON DELETE CASCADE )";

}
