package com.example.xps.budgetapp.Controllers;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.example.xps.budgetapp.AlarmServicesConfigurations.DailyFixedIncomeExpenseAlarm;
import com.example.xps.budgetapp.AlarmServicesConfigurations.MonthBudgetCheckerAlarm;
import com.example.xps.budgetapp.R;

/**
 * Created by XPS on 8/2/2015.
 */
public class SplashViewController extends Activity {

    View parentRootView;
    ProgressBar progressBar;
    DailyFixedIncomeExpenseAlarm dailyFixedIncomeExpenseAlarm;
    MonthBudgetCheckerAlarm monthBudgetCheckerAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        parentRootView = getLayoutInflater().inflate(R.layout.splash_layout,null);


        this.progressBar = (ProgressBar) parentRootView.findViewById(R.id.progressSplashBar);
        ActionBar actionBar = getActionBar();

        SharedPreferences runCheck = getSharedPreferences("hasRunBefore", 0);
        Boolean hasRun = runCheck.getBoolean("hasRun", false);

        if (!hasRun) {

            SharedPreferences settings = getSharedPreferences("hasRunBefore", 0);
            SharedPreferences.Editor edit = settings.edit();
            edit.putBoolean("hasRun", true);
            edit.apply();

            dailyFixedIncomeExpenseAlarm = new DailyFixedIncomeExpenseAlarm(this.parentRootView.getContext(),true);
            monthBudgetCheckerAlarm = new MonthBudgetCheckerAlarm(this.parentRootView.getContext(),false,0);

            dailyFixedIncomeExpenseAlarm.setDailyFixedIncomeExpenseServices();
            monthBudgetCheckerAlarm.setMonthBudgetChecker();
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(SplashViewController.this, PrincipalMenuViewController.class);
                startActivity(intent);

                finish();
            }
        }, 3000);
    }


}

