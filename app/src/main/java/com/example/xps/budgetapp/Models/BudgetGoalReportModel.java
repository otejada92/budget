package com.example.xps.budgetapp.Models;

import java.util.ArrayList;

/**
 * Created by XPS on 3/26/2015.
 */
public class BudgetGoalReportModel {

    private String BUDGET_DESC;
    private String BUDGET_GOAL;
    private String START_DATE;
    private String END_DATE;
    private ArrayList<BudgetedIncomeModel> BUDGETED_INCOME;
    private ArrayList<BudgetedExpenseModel> BUDGETED_EXPENSE;

    public BudgetGoalReportModel(String BUDGET_DESC,String BUDGET_GOAL, String START_DATE, String END_DATE, ArrayList<BudgetedIncomeModel> BUDGETED_INCOME, ArrayList<BudgetedExpenseModel> BUDGETED_EXPENSE) {

        this.BUDGET_DESC = BUDGET_DESC;
        this.BUDGET_GOAL = BUDGET_GOAL;
        this.START_DATE = START_DATE;
        this.END_DATE = END_DATE;
        this.BUDGETED_INCOME = BUDGETED_INCOME;
        this.BUDGETED_EXPENSE = BUDGETED_EXPENSE;
    }


    public String getBUDGET_GOAL() {
        return BUDGET_GOAL;
    }

    public void setBUDGET_GOAL(String BUDGET_GOAL) {
        this.BUDGET_GOAL = BUDGET_GOAL;
    }

    public String getSTART_DATE() {
        return START_DATE;
    }

    public void setSTART_DATE(String START_DATE) {
        this.START_DATE = START_DATE;
    }

    public String getEND_DATE() {
        return END_DATE;
    }

    public void setEND_DATE(String END_DATE) {
        this.END_DATE = END_DATE;
    }

    public ArrayList<BudgetedIncomeModel> getBUDGETED_INCOME() {
        return BUDGETED_INCOME;
    }

    public void setBUDGETED_INCOME(ArrayList<BudgetedIncomeModel> budgeted_income) {
        this.BUDGETED_INCOME = budgeted_income;
    }

    public ArrayList<BudgetedExpenseModel> getBUDGETED_EXPENSE() {
        return BUDGETED_EXPENSE;
    }

    public void setBUDGETED_EXPENSE(ArrayList<BudgetedExpenseModel> budgeted_expense) {
        this.BUDGETED_EXPENSE = budgeted_expense;
    }

    public String getBUDGET_DESC() {
        return BUDGET_DESC;
    }

    public void setBUDGET_DESC(String BUDGET_DESC) {
        this.BUDGET_DESC = BUDGET_DESC;
    }
}
