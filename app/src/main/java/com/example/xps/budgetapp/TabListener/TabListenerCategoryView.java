package com.example.xps.budgetapp.TabListener;

import android.app.ActionBar;
import android.app.FragmentTransaction;

/**
 * Created by XPS on 1/20/2015.
 */
import android.app.Fragment;

import com.example.xps.budgetapp.R;

public class TabListenerCategoryView implements ActionBar.TabListener {

    private Fragment FRAGMENT;

    public TabListenerCategoryView(Fragment fragment) {

       this.FRAGMENT = fragment;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.replace(R.id.categoria_layout,this.FRAGMENT);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            fragmentTransaction.remove(this.FRAGMENT);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
