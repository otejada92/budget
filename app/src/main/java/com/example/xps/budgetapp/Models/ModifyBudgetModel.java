package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 11/26/2014.
 */
public class ModifyBudgetModel {

    private String ID_PRESUPUESTO;
    private String START_DATE;
    private String FINISH_DATE;

    public ModifyBudgetModel(String id_presupuesto, String start_date, String finish_date) {
        this.ID_PRESUPUESTO = id_presupuesto;
        this.START_DATE = start_date;
        this.FINISH_DATE = finish_date;
    }

    public String getSTART_DATE() {
        return START_DATE;
    }

    public void setSTART_DATE(String START_DATE) {
        this.START_DATE = START_DATE;
    }

    public String getFINISH_DATE() {
        return FINISH_DATE;
    }

    public void setFINISH_DATE(String FINISH_DATE) {
        this.FINISH_DATE = FINISH_DATE;
    }

    public String getID_PRESUPUESTO() {
        return ID_PRESUPUESTO;
    }

    public void setID_PRESUPUESTO(String ID_PRESUPUESTO) {
        this.ID_PRESUPUESTO = ID_PRESUPUESTO;
    }
}
