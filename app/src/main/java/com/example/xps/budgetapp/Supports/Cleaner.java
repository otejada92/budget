package com.example.xps.budgetapp.Supports;

import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;

import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.Fragments.AddBudgetFragment;
import com.example.xps.budgetapp.R;

public class Cleaner {

    public void cleanView(ViewGroup viewgroup){

        int count = viewgroup.getChildCount();

        for (int index=0; index <= count; index++) {
            View view = viewgroup.getChildAt(index);

            if (view instanceof EditText) {
                if(view.getId() != R.id.DateHappened)
                    ((EditText) viewgroup.getChildAt(index)).setText("");
            }

            if (view instanceof ListView) {
                if(view.getId() == R.id.income_listview)
                    ((ListView) view).clearChoices();
                if(view.getId() == R.id.expense_listview)
                    ((ListView) view).clearChoices();
            }
            if(view instanceof RadioGroup){
                ((RadioGroup) view).clearCheck();
            }
            if (view instanceof MultipleSpinnerSelection){
                ((MultipleSpinnerSelection) view).clearView();
            }

            if(view instanceof ViewGroup && (((ViewGroup)view).getChildCount() > 0))
                cleanView((ViewGroup) view);
        }

    }

}
