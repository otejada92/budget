package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 11/29/2014.
 */
public class BudgetedExpenseModel {


    private String ID_EXPENSE;
    private String DESCRIPTION;
    private String AMOUNT;
    private String CATEGORY_DESC;
    private String ID_CATEGORY;
    private String ID_BUDGET;
    private String BUDGETED_DATE;

    public BudgetedExpenseModel(String id_expense, String description, String amount,String category_desc ,String id_category, String id_budget, String budgeted_date){


        this.ID_EXPENSE = id_expense;
        this.DESCRIPTION = description;
        this.AMOUNT = amount;
        this.CATEGORY_DESC = category_desc;
        this.ID_CATEGORY = id_category;
        this.ID_BUDGET = id_budget;
        this.BUDGETED_DATE = budgeted_date;
    }



    public String getID_EXPENSE() {
        return ID_EXPENSE;
    }

    public void setID_EXPENSE(String id_expense) {
        this.ID_EXPENSE = id_expense;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String description) {
        this.DESCRIPTION = description;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String amount) {
        this.AMOUNT = amount;
    }

    public String getID_CATEGORY() {
        return ID_CATEGORY;
    }

    public void setID_CATEGORY(String id_category) {
        this.ID_CATEGORY = id_category;
    }

    public String getID_BUDGET() {
        return ID_BUDGET;
    }

    public void setID_BUDGET(String id_budget) {
        this.ID_BUDGET = id_budget;
    }

    public String getBUDGETED_DATE() {
        return BUDGETED_DATE;
    }

    public String getCATEGORY_DESC() {
        return CATEGORY_DESC;
    }

    public void setCATEGORY_DESC(String CATEGORY_DESC) {
        this.CATEGORY_DESC = CATEGORY_DESC;
    }
}
