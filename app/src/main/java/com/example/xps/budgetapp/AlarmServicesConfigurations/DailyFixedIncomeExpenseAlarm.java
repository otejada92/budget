
package com.example.xps.budgetapp.AlarmServicesConfigurations;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Broadcast.DailyFixedIncomeExpenseReceiver;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.util.Calendar;

    /**
     * Created by XPS on 6/30/2015.
     */
    public class DailyFixedIncomeExpenseAlarm {

        private  boolean FIRST_TIME;
        private Context context;
        private ManagerDate managerDate = new ManagerDate();


        public DailyFixedIncomeExpenseAlarm(Context context,boolean first_time) {

            this.context = context;
            this.FIRST_TIME = first_time;

        }

        public void setDailyFixedIncomeExpenseServices(){

            Calendar scheduleTime = Calendar.getInstance();
            Calendar timeNow = Calendar.getInstance();
            int idAlarm = managerDate.getAlarmId();

            if (this.FIRST_TIME) {

                scheduleTime.set(Calendar.HOUR,12);
                scheduleTime.set(Calendar.HOUR_OF_DAY,0);
                scheduleTime.set(Calendar.MINUTE,0);
                scheduleTime.set(Calendar.SECOND,0);


                if (scheduleTime.before(timeNow)){

                    scheduleTime.add(Calendar.DAY_OF_MONTH, 1);
                    scheduleTime.set(Calendar.HOUR, 12);
                    scheduleTime.set(Calendar.HOUR_OF_DAY,0);
                    scheduleTime.set(Calendar.MINUTE,0);
                    scheduleTime.set(Calendar.SECOND,0);
                }

            }else{

                scheduleTime.add(Calendar.DAY_OF_MONTH, 1);

                scheduleTime.set(Calendar.HOUR_OF_DAY, 8);
                scheduleTime.set(Calendar.HOUR,8);
                scheduleTime.set(Calendar.MINUTE,0);
                scheduleTime.set(Calendar.SECOND,0);

            }

            Intent intentReciver = new Intent(this.context,DailyFixedIncomeExpenseReceiver.class);
            intentReciver.putExtra("idAlarm",String.valueOf(idAlarm));

            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.context, idAlarm, intentReciver, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmManager = (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC, scheduleTime.getTimeInMillis(), pendingIntent);
            } else {
                alarmManager.set(AlarmManager.RTC, scheduleTime.getTimeInMillis(), pendingIntent);
            }
        }

    }


