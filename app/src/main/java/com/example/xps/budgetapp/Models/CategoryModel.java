package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 10/18/2014.
 */
public class CategoryModel {

    private String ID_CATEGORY;
    private String DESCRIPTION;

    public CategoryModel(String id, String descripcion){
        super();
        this.ID_CATEGORY = id;
        this.DESCRIPTION = descripcion;
    }

    public String getID_CATEGORY(){return ID_CATEGORY;}
    public void setID_CATEGORY(String value){ ID_CATEGORY = value;}

    public String getDESCRIPTION(){return DESCRIPTION;}
    public void setDESCRIPTION(String value){ DESCRIPTION = value;}


}
