package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskExpenseProcess;
import com.example.xps.budgetapp.ClickListiners.SelectRecurrenceClick;
import com.example.xps.budgetapp.ClickListiners.ShowHelpDialog;
import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Controllers.PrincipalMenuViewController;
import com.example.xps.budgetapp.CustomAdapters.CustomAdapterSpinnerCategory;
import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by XPS on 11/15/2014.
 */
public class AddExpenseFragment extends Fragment {

    private static boolean saveState = true;
    private View ROOTVIEW;
    private EditText descriptionEditText;
    private EditText amountEditText;
    private Spinner recurrenceSpinner;
    private static Spinner categorySpinner;
    private MultipleSpinnerSelection dayHappenedSpinner;
    public ImageButton helpButton;

    public AddExpenseFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        final View rootView = inflater.inflate(R.layout.add_expense_fragment_layout,container,false);
        this.ROOTVIEW = rootView;

        InitView(rootView);
        PopulateSpinnerCategory(rootView);
        PopulateRecurrenceSpinner(rootView);

        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionEditText.length() != 0 || !descriptionEditText.getText().toString().equals("")) {
                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountEditText.length() != 0 || !amountEditText.getText().toString().equals("")) {
                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        recurrenceSpinner.setOnItemSelectedListener(new SelectRecurrenceClick(rootView));
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                saveState = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        helpButton.setOnClickListener(new ShowHelpDialog(getFragmentManager(),"dayHappenedHelp"));
        populateDaySpinner();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.expense_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();
        Toast toast;
        String messageError = "";
        saveState = true;
        String description = descriptionEditText.getText().toString().trim();
        String amount  = amountEditText.getText().toString().trim();
        HashMap<String, String> expenseInformation = new HashMap<String, String>();
        String recurrenceSelected = String.valueOf(recurrenceSpinner.getSelectedItemPosition());
        String categorySelected = String.valueOf(categorySpinner.getSelectedView().getId());
        String dayHappened = dayHappenedSpinner.getSelectedItemsAsString();

        String SaveStateStr;

        if(id_item == R.id.add_button){


            if (description.length() == 0 || description.matches("\\s+")) {

                messageError += "La descripcion del gasto no puede estar vacia.\n";
                saveState = false;
            }

            if (amount.length() == 0) {
                messageError += "El monto del gasto no puede estar vacia.\n";
                saveState = false;
            }

            if (categorySelected.equals(String.valueOf(R.id.CategoryChoise))) {
                messageError += "Seleccione una categoria.\n";
                saveState = false;
            }

            if (recurrenceSelected.equals("1")) {
                if (dayHappened.equals("")) {
                    messageError += "\n Si es un gasto fijo el 'Dia de ocurrencia' no puede estar vacio.";
                    saveState = false;
                } else {
                    saveState = true;
                }
            }

            if(saveState){

                SaveStateStr =  String.valueOf(saveState);

                expenseInformation.put("description", description);
                expenseInformation.put("amount", amount);
                expenseInformation.put("recurrence", recurrenceSelected);
                expenseInformation.put("idCategory", categorySelected);
                expenseInformation.put("dayHappened", dayHappened);
                expenseInformation.put("saveState", SaveStateStr);
                expenseInformation.put("DO", "SAVE");

                AsyncTaskExpenseProcess task = new AsyncTaskExpenseProcess(ROOTVIEW, expenseInformation,ROOTVIEW.getContext());
                task.execute();
            }else{
                toast = Toast.makeText(ROOTVIEW.getContext(),messageError.replaceAll("^\\s+", ""),Toast.LENGTH_LONG);
                toast.show();
            }

        }

        if(id_item == android.R.id.home){

            Intent homeIntent = new Intent(getActivity().getBaseContext(), PrincipalMenuViewController.class);
            startActivity(homeIntent);
        }

        return true;
    }

    public static void PopulateSpinnerCategory(final View rootview){

        ArrayList<CategoryModel> CategoriesList = CategoryDataController.GetCategories(rootview.getContext());

        if (!CategoriesList.isEmpty()) {
            CustomAdapterSpinnerCategory categoryAdapter = new CustomAdapterSpinnerCategory(rootview.getContext(), R.layout.category_spinner_layout, R.id.CategoryChoise, CategoriesList);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySpinner.setAdapter(categoryAdapter);
        }
        else{
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),R.layout.category_spinner_layout,new String[]{"No hay categoria"});
            errorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySpinner.setAdapter(errorAdapter);
        }
    }

    public void PopulateRecurrenceSpinner(View rootview){

        List<String> list = new ArrayList<String>();

        list.add("Variable");
        list.add("Fijo");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(rootview.getContext(),
                android.R.layout.simple_spinner_item,list);

        recurrenceSpinner.setAdapter(dataAdapter);
        recurrenceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                saveState = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void populateDaySpinner(){

        String[] list = getResources().getStringArray(R.array.day_of_month);
        dayHappenedSpinner.setItems(list);
    }


    public void InitView(View rootview){

        descriptionEditText = (EditText) rootview.findViewById(R.id.descripction_expense);
        amountEditText = (EditText) rootview.findViewById(R.id.amount_expense);
        categorySpinner = (Spinner) rootview.findViewById(R.id.categories_spinner);
        recurrenceSpinner = (Spinner) rootview.findViewById(R.id.RecurrenceSpinner);
        dayHappenedSpinner = (MultipleSpinnerSelection) rootview.findViewById(R.id.DayHappened);
        helpButton = (ImageButton) rootview.findViewById(R.id.help_button);

    }

}


