package com.example.xps.budgetapp.Fragments;

/**
 * Created by XPS on 12/17/2014.
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Controllers.PrincipalMenuViewController;
import com.example.xps.budgetapp.Dialogs.AddCategoryDialog;
import com.example.xps.budgetapp.CustomAdapters.CustomAdapterListViewModifyCategory;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class CategoryFragment extends Fragment {

    private static  View ROOTVIEW;
    private static FragmentManager FRAGMENTMANAGER;
    Button saveButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View rootview = inflater.inflate(R.layout.category_fragment_view,container,false);

        FRAGMENTMANAGER = getFragmentManager();
        ROOTVIEW = rootview;

        saveButton = (Button) rootview.findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCategoryDialog dialog = new AddCategoryDialog();
                dialog.show(getFragmentManager(), "Add");


            }
        });

        PopulateListViewCategory();

        return rootview;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.category_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();

        if(id_item == android.R.id.home){
            Intent intentHome = new Intent(getActivity().getBaseContext(), PrincipalMenuViewController.class);
            startActivity(intentHome);
        }

        return true;
    }

    public static void PopulateListViewCategory(){

        ListView category_listview = (ListView) ROOTVIEW.findViewById(R.id.categoriesListview);
        ArrayList<CategoryModel> list_categories = CategoryDataController.GetCategories(ROOTVIEW.getContext());
        String[] error = {"No hay categorias disponibles."};


        if (!list_categories.isEmpty()) {
            CustomAdapterListViewModifyCategory arrayAdapter =  new CustomAdapterListViewModifyCategory(ROOTVIEW.getContext(),
                    R.layout.category_listview_main_layout,
                    R.id.CategoryDescription,
                    R.id.Editar,
                    R.id.Borrar,
                    FRAGMENTMANAGER,
                    list_categories);

            category_listview.setAdapter(arrayAdapter);

        }else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(ROOTVIEW.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
            category_listview.setAdapter(errorAdapter);
        }
    }



}
