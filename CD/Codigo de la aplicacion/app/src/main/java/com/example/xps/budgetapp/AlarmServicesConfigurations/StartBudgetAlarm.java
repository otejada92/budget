package com.example.xps.budgetapp.AlarmServicesConfigurations;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Broadcast.EndBudgetReceiver;
import com.example.xps.budgetapp.Broadcast.StartBudgetReceiver;

import java.util.Calendar;

/**
 * Created by XPS on 7/19/2015.
 */
public class StartBudgetAlarm {

        private String ID_BUDGET;
        private  String[] EXE_DATE ={} ;
        private Context CONTEXT;
        private int ID_ALARM;


        public StartBudgetAlarm(Context context, String[] exe_date,String id_budget,int id_alarm){

        this.CONTEXT = context;
        this.EXE_DATE = exe_date;
        this.ID_BUDGET = id_budget;
        this.ID_ALARM =  id_alarm;
    }

    public void setStartBudgetServices(){

        Calendar scheduleTime = Calendar.getInstance();
        Calendar timeNow = Calendar.getInstance();

        scheduleTime.set(Calendar.YEAR, Integer.valueOf(this.EXE_DATE[0]));
        scheduleTime.set(Calendar.MONTH, Integer.valueOf(this.EXE_DATE[1]) - 1);
        scheduleTime.set(Calendar.DAY_OF_MONTH, Integer.valueOf(this.EXE_DATE[2]));

        scheduleTime.set(Calendar.HOUR, 12);
        scheduleTime.set(Calendar.HOUR_OF_DAY, 0);
        scheduleTime.set(Calendar.MINUTE,0);
        scheduleTime.set(Calendar.SECOND,0);

        if (scheduleTime.before(timeNow)){

            scheduleTime.add(Calendar.DAY_OF_MONTH, 1);
            scheduleTime.set(Calendar.HOUR, 12);
            scheduleTime.set(Calendar.HOUR_OF_DAY,0);
            scheduleTime.set(Calendar.MINUTE,0);
            scheduleTime.set(Calendar.SECOND,0);
        }


        Intent intentReciver = new Intent(CONTEXT,StartBudgetReceiver.class);

        intentReciver.putExtra("idAlarm",String.valueOf(this.ID_ALARM));
        intentReciver.putExtra("idBudget",this.ID_BUDGET);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(CONTEXT, this.ID_ALARM, intentReciver,PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)CONTEXT.getSystemService(Context.ALARM_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC, scheduleTime.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC, scheduleTime.getTimeInMillis(), pendingIntent);
        }

    }
}

