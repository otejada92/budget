package com.example.xps.budgetapp.TabListener;

/**
 * Created by XPS on 1/5/2015.
 */

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

import com.example.xps.budgetapp.Fragments.AddBudgetFragment;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class TabListenerBudgetView implements ActionBar.TabListener {

    private Fragment FRAGMENT;

    public TabListenerBudgetView(Fragment fragment) {

        this.FRAGMENT = fragment;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        fragmentTransaction.replace(R.id.presupuesto_layout,FRAGMENT);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        AddBudgetFragment.IncomeToSave = new ArrayList<IncomeModel>();
        AddBudgetFragment.expenseToSave = new ArrayList<ExpenseModel>();
        fragmentTransaction.remove(FRAGMENT);

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
