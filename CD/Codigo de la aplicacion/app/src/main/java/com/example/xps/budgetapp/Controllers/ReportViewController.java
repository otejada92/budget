package com.example.xps.budgetapp.Controllers;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.xps.budgetapp.Dialogs.LineChartDialog;
import com.example.xps.budgetapp.R;

public class ReportViewController extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_view_controller);

        Button consultaGeneral = (Button) findViewById(R.id.Consulta_General);
        consultaGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), GeneralBudgetReportController.class);
                startActivity(intent);
            }
        });
        Button distribucionGasto = (Button) findViewById(R.id.Distribucion_gasto);
        distribucionGasto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ExpenseDistributionSelectionViewController.class);
                startActivity(intent);
            }
        });

        Button metaPresupuesto = (Button) findViewById(R.id.GoalReport);
        metaPresupuesto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BudgetGoalReportSelectionView.class);
                startActivity(intent);
            }
        });

        final Button lineGraphic = (Button) findViewById(R.id.noName);
        lineGraphic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LineChartDialog lineChartDialog = new LineChartDialog();
                lineChartDialog.show(getFragmentManager(), "Filter");
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.consulta_view_controller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

}
