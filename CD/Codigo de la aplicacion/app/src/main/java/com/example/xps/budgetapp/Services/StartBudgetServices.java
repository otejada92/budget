package com.example.xps.budgetapp.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.example.xps.budgetapp.Broadcast.DimissDailyFixedIncomeExpenseNotification;
import com.example.xps.budgetapp.Broadcast.DimissStartBudget;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Budget_Table;

/**
 * Created by XPS on 7/19/2015.
 */
public class StartBudgetServices extends Service {

    ManagerDate managerDate = new ManagerDate();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        DataBaseController db = new DataBaseController(this);
        SQLiteDatabase tableManager = db.getWritableDatabase();
        int requestId = managerDate.getAlarmId();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        BudgetDataController dataController = new BudgetDataController();
        String idBudget = "";
        String idAlarm = "";
        try{
            idBudget = intent.getStringExtra("idBudget");
            idAlarm = intent.getStringExtra("idAlarm");
        }catch (NullPointerException ignored){}


        if(!idBudget.equals("")) {
            String description = dataController.getDescBudget(this, idBudget);

            tableManager.execSQL(" UPDATE " + Budget_Table.TABLE_NAME +
                    " SET " + Budget_Table.BUDGET_STATUS + " = 1" +
                    " WHERE " + Budget_Table.ID_BUDGET + " = ?", new String[]{idBudget});

            Intent dismissIntent = new Intent(this, DimissStartBudget.class);
            dismissIntent.putExtra("notificationId", startId);
            dismissIntent.putExtra("idAlarm", idAlarm);

            PendingIntent piDismiss = PendingIntent.getBroadcast(this, requestId, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder
                    .setSmallIcon(R.drawable.status_dialog_warning_icon)
                    .setContentTitle("Budget Helper")
                    .setContentText("Ha comenzando ciclo del presupuesto:...")
                    .setContentIntent(piDismiss)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .setBigContentTitle("Budget Helper")
                            .bigText("Ha comenzado el ciclo del presupuesto: " + description))

                    .setDefaults(Notification.DEFAULT_SOUND);

            notificationManager.notify(startId, mBuilder.build());
        }

        return START_STICKY;
    }

}
