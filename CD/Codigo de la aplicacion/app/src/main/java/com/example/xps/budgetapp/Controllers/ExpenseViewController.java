package com.example.xps.budgetapp.Controllers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.example.xps.budgetapp.Fragments.AddExpenseFragment;
import com.example.xps.budgetapp.Fragments.ModifyExpenseFragment;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.TabListener.TabListenerExpenseView;


public class ExpenseViewController extends Activity {

    ActionBar.Tab Tab1, Tab2;
    Fragment fragmentTab1 = new AddExpenseFragment();
    Fragment fragmentTab2 = new ModifyExpenseFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.expense_fragment_layout);

        ActionBar actionBar = getActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        Tab1 = actionBar.newTab().setText("Añadir Gasto");
        Tab2 = actionBar.newTab().setText("Modificar Gasto");

        Tab1.setTabListener(new TabListenerExpenseView(fragmentTab1));
        Tab2.setTabListener(new TabListenerExpenseView(fragmentTab2));

        actionBar.addTab(Tab1);
        actionBar.addTab(Tab2);

    }

}


