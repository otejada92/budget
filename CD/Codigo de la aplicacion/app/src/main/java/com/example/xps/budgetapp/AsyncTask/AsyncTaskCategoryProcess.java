package com.example.xps.budgetapp.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyCategory;
import com.example.xps.budgetapp.Fragments.CategoryFragment;
import com.example.xps.budgetapp.Processes.CategoryProcesses;

/**
 * Created by XPS on 3/15/2015.
 */
public class AsyncTaskCategoryProcess extends AsyncTask<Void,Void,String> {

    private Context CONTEXT;
    private String  CATEGORY_DESC;
    private String  ID_CATEGORY;
    private String ACTION;

    public AsyncTaskCategoryProcess(Context context, String category_desc,String action) {

        this.CONTEXT = context;
        this.CATEGORY_DESC = category_desc;
        this.ACTION = action;

    }

    public AsyncTaskCategoryProcess(Context context, String id_category,String category_desc,String action) {

        this.CONTEXT = context;
        this.ID_CATEGORY = id_category;
        this.CATEGORY_DESC = category_desc;
        this.ACTION = action;

    }

    @Override
    protected String doInBackground(Void... voids) {

        ContextStrategyCategory contextStrategy = new ContextStrategyCategory(new CategoryProcesses());

        if(this.ACTION.equals("ADD")) {
            contextStrategy.executeSaveCategory(this.CONTEXT, this.CATEGORY_DESC);
            return this.ACTION;
        }
        else {
            contextStrategy.executeModifyCategory(this.CONTEXT,this.ID_CATEGORY,this.CATEGORY_DESC);
            return this.ACTION;
        }

    }

    @Override
    protected void onPostExecute(String aAction) {
        super.onPostExecute(aAction);

        Toast toast;
        if(aAction.equals("ADD")){
            toast = Toast.makeText(this.CONTEXT,"Se ha guardado la categoria", Toast.LENGTH_LONG);
            toast.show();
        }else{
            toast = Toast.makeText(this.CONTEXT,"Se ha actualizado la categoria", Toast.LENGTH_LONG);
            toast.show();
        }


        CategoryFragment.PopulateListViewCategory();

    }

}
