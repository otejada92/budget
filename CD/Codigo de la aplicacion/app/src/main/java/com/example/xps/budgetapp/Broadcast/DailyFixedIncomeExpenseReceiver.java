package com.example.xps.budgetapp.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Services.DailyFixedIncomeExpenseService;

/**
 * Created by XPS on 6/30/2015.
 */
public class DailyFixedIncomeExpenseReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String idAlarm = intent.getStringExtra("idAlarm");

        Intent services = new Intent(context,DailyFixedIncomeExpenseService.class);
        services.putExtra("idAlarm",idAlarm);

        context.startService(services);
    }

}
