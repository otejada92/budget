package com.example.xps.budgetapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.xps.budgetapp.Models.CategoryDistributionExpenseDetail;
import com.example.xps.budgetapp.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by XPS on 7/18/2015.
 */
public class CategoryExpenseDetailDialog extends  DialogFragment{

    ArrayList<CategoryDistributionExpenseDetail> categoryDetail;
    View dialogRootView;
    TextView expenseInformation;
    TextView totalExpense;
    float totalExpenseValue;
    String idCategory;

    public CategoryExpenseDetailDialog(ArrayList<CategoryDistributionExpenseDetail> categoryDetail, String idCategory) {
        this.categoryDetail = categoryDetail;
        this.idCategory = idCategory;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        dialogRootView = getActivity().getLayoutInflater().inflate(R.layout.detail_information_layout,null);
        expenseInformation = (TextView) dialogRootView.findViewById(R.id.ViewInformation);
        totalExpense = (TextView) dialogRootView.findViewById(R.id.Total);

        String expenseDetail = "";

        for (CategoryDistributionExpenseDetail expense : categoryDetail){
            if (expense.getIdCategory().equals(this.idCategory)) {
                expenseDetail += expense.getExpenseDescription() + " - " + "$" + expense.getExpenseAmount() + "\n";
                totalExpenseValue += Float.valueOf(expense.getExpenseAmount());
            }
        }

        expenseInformation.setText(expenseDetail);
        totalExpense.setText("Total de gastos: $"+String.valueOf(totalExpenseValue));

        return new AlertDialog.Builder(getActivity())
                .setView(dialogRootView)
                .setTitle("Gastos de categoria")
                .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();

    }
}
