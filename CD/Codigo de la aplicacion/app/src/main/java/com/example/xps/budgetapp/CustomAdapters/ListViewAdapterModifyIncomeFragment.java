package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.Fragments.ModifyIncomeFragment;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.Processes.DeleteProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Tables.Income_Table;

import java.util.ArrayList;

public class ListViewAdapterModifyIncomeFragment extends ArrayAdapter<IncomeModel> {

    private Context CONTEXT;
    private final ArrayList<IncomeModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;
    public EditText descriptionEditText;
    public EditText amountEditText;
    private Spinner recurrenceSpinner;
    public RelativeLayout mainLayout;
    private MultipleSpinnerSelection dayHappennedSpinner;
    int editButtonIndex = 0;


    public ListViewAdapterModifyIncomeFragment(Context context, int resource, View rootview, ArrayList<IncomeModel> Data) {

        super(context, resource,Data);
        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;

    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder Holder;
        final Cleaner clean_helper = new Cleaner();

        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView=inflater.inflate(this.RESOURCE,parent,false);

            Holder=new ViewHolder();


            Holder.textViewDesc=(TextView)convertView.findViewById(R.id.descripcion);
            Holder.textViewAmount=(TextView)convertView.findViewById(R.id.amount);

            Holder.editButton = (ImageButton) convertView.findViewById(R.id.Editar);
            Holder.deleteButton = (ImageButton) convertView.findViewById(R.id.Borrar);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }

        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getDESCRIPTION();
        String amountString = "<b>Monto:$ </b>"+ this.DATA.get(position).getAMOUNT();

        Holder.textViewDesc.setId(Integer.parseInt(this.DATA.get(position).getID_INCOME()));

        Holder.textViewDesc.setText(Html.fromHtml(descString));

        Holder.textViewAmount.setText(Html.fromHtml(amountString));


        Holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                initViews();

                descriptionEditText.setEnabled(true);
                amountEditText.setEnabled(true);
                recurrenceSpinner.setEnabled(true);
                ModifyIncomeFragment.menuActionbar.getItem(editButtonIndex).setEnabled(true);
                dayHappennedSpinner.setEnabled(true);

                ModifyIncomeFragment.id_income = DATA.get(position).getID_INCOME();
                descriptionEditText.setText(DATA.get(position).getDESCRIPTION());
                amountEditText.setText(DATA.get(position).getAMOUNT().trim());
                recurrenceSpinner.setSelection(Integer.valueOf(DATA.get(position).getRECURRENCE()));
                String[] selections = DATA.get(position).getDAY_ISSUE().split(",");
                dayHappennedSpinner.setSelection(selections);
            }
        });

        Holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String whereClause = Income_Table.ID_INCOME + " = ?";
                final String[] whereArgs = new String[]{DATA.get(position).getID_INCOME()};

                LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);

                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                textView.setText("Estas seguro que deseas borrar este ingreso?, este sera borrado para siempre y no se podra usar para presupuestar!");

                AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
                builder
                        .setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyDeleteResource contextDelete = new ContextStrategyDeleteResource(new DeleteProcesses());
                                        contextDelete.executeStrategy(ROOTVIEW.getContext(), Income_Table.TABLE_NAME, whereClause, whereArgs);

                                        initViews();
                                        notifyDataSetChanged();
                                        clean_helper.cleanView(mainLayout);

                                        descriptionEditText.setEnabled(false);
                                        amountEditText.setEnabled(false);
                                        recurrenceSpinner.setEnabled(false);
                                        dayHappennedSpinner.setEnabled(false);
                                    }
                                }
                        )
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();
            }
        });

        return convertView;
    }

    static class ViewHolder {

        TextView textViewDesc;
        TextView textViewAmount;
        ImageButton editButton;
        ImageButton deleteButton;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        ModifyIncomeFragment.PopulateIncomeListView(ROOTVIEW);
    }

    public void initViews(){

        descriptionEditText = (EditText) ROOTVIEW.findViewById(R.id.descripction_income);
        amountEditText = (EditText) ROOTVIEW.findViewById(R.id.amount_income);
        dayHappennedSpinner = (MultipleSpinnerSelection) ROOTVIEW.findViewById(R.id.DayHappened);
        recurrenceSpinner = (Spinner) ROOTVIEW.findViewById(R.id.RecurrenceSpinner);
        mainLayout = (RelativeLayout) ROOTVIEW.findViewById(R.id.modify_income_fragment);

    }
}