package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.xps.budgetapp.CustomAdapters.CustomGoalReportSelectionBudgetAdapter;
import com.example.xps.budgetapp.Dialogs.GoalReportFilterDialog;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class BudgetGoalReportSelectionView extends Activity {

    public static ListView budgetListView;
    public View parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presupuesto_consulta_view_controller);

        budgetListView = (ListView) findViewById(R.id.presupuesto_listview);
        LayoutInflater inflater =
                (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        parentView = inflater.inflate(R.layout.activity_presupuesto_consulta_view_controller, null);

        ArrayAdapter adapter = populateBudgetListview();
        budgetListView.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.modify_budget_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.filter){
            GoalReportFilterDialog goalReportFilterDialog = new GoalReportFilterDialog(parentView);
            goalReportFilterDialog.show(getFragmentManager(), "Filter");
        }
        if(id == R.id.clear_filter){
                ArrayAdapter adapter = populateBudgetListview();
                budgetListView.setAdapter(adapter);
        }
        return super.onOptionsItemSelected(item);

    }


    public ArrayAdapter populateBudgetListview() {

        ArrayList<GenericBudgetModel> presupuestos_list = BudgetDataController.getBudgetsInformation(parentView.getContext());
        String[] error = {"No hay presupuestos."};

        if (!presupuestos_list.isEmpty()) {
            return new CustomGoalReportSelectionBudgetAdapter(
                    parentView.getContext(),
                    R.layout.budget_listview_report_layout,
                    parentView,
                    presupuestos_list);
        } else {
            return new ArrayAdapter<String>(parentView.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
        }
    }


}
