package com.example.xps.budgetapp.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;

import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;


public class ListViewAdapterIncomeBudget extends ArrayAdapter<IncomeModel>{

    private Context context;
    private ArrayList<IncomeModel> Data;
    private int RESOURCE;
    private int VIEWID;
    private View ROOTVIEW;


    @Override
    public IncomeModel getItem(int position) {
        return this.Data.get(position);
    }

    public ListViewAdapterIncomeBudget(Context context, int resource, int element,View rootview,ArrayList<IncomeModel> Data) {
        super(context,resource,element, Data);
        this.context = context;
        this.Data = Data;
        this.RESOURCE = resource;
        this.ROOTVIEW = rootview;
        this.VIEWID = element;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder Holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(this.RESOURCE, parent, false);
            Holder = new ViewHolder();
            Holder.checkBox = (CheckBox) convertView.findViewById(this.VIEWID);
            Holder.listView = (ListView) ROOTVIEW.findViewById(R.id.income_listview);
            convertView.setTag(Holder);
        }
        else{
            Holder = (ViewHolder) convertView.getTag();
        }

        Holder.checkBox.setId(Integer.parseInt(Data.get(position).getID_INCOME()));
        Holder.checkBox.setText(Data.get(position).getDESCRIPTION()+" $"+Data.get(position).getAMOUNT());
        return convertView;
    }


    public ArrayList<IncomeModel> Filter(CharSequence charSequence) {


                ArrayList<IncomeModel> list_ingreso = IncomeDataController.getIncomesInformation(context);
                ArrayList<IncomeModel> filterred_list = new ArrayList<IncomeModel>();

                if(charSequence == null || charSequence.length() == 0) {
                    return  list_ingreso;
                }
                else {
                    for (IncomeModel model_ingreso : list_ingreso) {
                        if (model_ingreso.getDESCRIPTION().contains(charSequence.toString()))
                            filterred_list.add(model_ingreso);
                    }
                }
            notifyDataSetChanged();
            return filterred_list;
            }

    static class ViewHolder{
        CheckBox checkBox;
        ListView listView;
    }


}
