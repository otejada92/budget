package com.example.xps.budgetapp.Models;

import java.util.ArrayList;

/**
 * Created by Osvaldo on 12/5/2014.
 */
public class ChartDistribucionModel {



    private String month;
    private ArrayList<DistributionCategoryTotals> categoryInformation;
    private ArrayList<CategoryDistributionExpenseDetail> expenseList;


    public ChartDistribucionModel(String month, ArrayList<DistributionCategoryTotals> categoryInformation,ArrayList<CategoryDistributionExpenseDetail> expenseList) {

        this.month = month;
        this.categoryInformation = categoryInformation;
        this.expenseList = expenseList;

    }


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public ArrayList<DistributionCategoryTotals> getCategoryInformation() {
        return categoryInformation;
    }

    public void setCategoryInformation(ArrayList<DistributionCategoryTotals> categoryInformation) {
        this.categoryInformation = categoryInformation;
    }

    public ArrayList<CategoryDistributionExpenseDetail> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(ArrayList<CategoryDistributionExpenseDetail> expenseList) {
        this.expenseList = expenseList;
    }
}
