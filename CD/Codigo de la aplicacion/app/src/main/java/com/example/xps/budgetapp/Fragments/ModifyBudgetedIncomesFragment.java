package com.example.xps.budgetapp.Fragments;

/**
 * Created by XPS on 11/29/2014.
 */

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskIncomeProcess;
import com.example.xps.budgetapp.ClickListiners.OnClickEditDate;
import com.example.xps.budgetapp.Controllers.BudgetViewController;
import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterModifyBudgetedIncome;
import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.util.ArrayList;
import java.util.HashMap;

public class ModifyBudgetedIncomesFragment extends Fragment{

    private boolean saveState = true;
    private View ROOTVIEW;
    public static String idIncome = "";
    private static ListView incomeListView;
    private EditText descriptionEditText;
    private EditText amountEditText;
    private EditText dateHappenedEditText;
    private static String ID_BUDGET;
    public static Menu menuActionbar;
    int editButtonIndex = 0;

    public ModifyBudgetedIncomesFragment(String id_budget) {

        this.ID_BUDGET = id_budget;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        final View rootView = inflater.inflate(R.layout.modify_budgeted_income_fragment_layout,container,false);
        this.ROOTVIEW = rootView;

        InitViews(rootView);



        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionEditText.length() != 0 || !descriptionEditText.getText().toString().equals("")) {

                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountEditText.length() != 0 || !amountEditText.getText().toString().equals("")) {

                    saveState = true;

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dateHappenedEditText.setOnClickListener(new OnClickEditDate(rootView, getFragmentManager()));
        dateHappenedEditText.setInputType(InputType.TYPE_NULL);
        dateHappenedEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                saveState = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        populateIncomeListView(rootView);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.modify_income_of_budget_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();
        String messageError = "Error: ";

        Toast toast;
        ManagerDate managerDate = new ManagerDate();
        String[] budget_date =  getActivity().getIntent().getStringExtra("BUDGET_DATE").split(",");

        String startDateBudgetStr = budget_date[0];
        String endDateBudgetStr = budget_date[1];
        String incomeDateHappened = dateHappenedEditText.getText().toString();

        String errorDateHappened = managerDate.ValidateDateHappened(startDateBudgetStr, endDateBudgetStr, incomeDateHappened);

        if(id_item == R.id.edit_button){

            String description = descriptionEditText.getText().toString();
            String amount = amountEditText.getText().toString();
            String saveStateStr;

            HashMap<String, String> income_information = new HashMap<String, String>();

            if(description.length() == 0 || description.matches("\\s+")){
                messageError += "La descripcion del ingresos no puede estar vacia.\n";
                saveState = false;
            }

            if (amount.length() == 0) {
                messageError += "El monto del ingresos no puede estar vacio.\n";
                saveState = false;
            }

            if (!errorDateHappened.equals("")){
                messageError += errorDateHappened+"\n";
                saveState = false;
            }

            saveStateStr = String.valueOf(saveState);

            if (saveState) {

                income_information.put("idIncome", idIncome);
                income_information.put("description", description);
                income_information.put("amount", amount);
                income_information.put("saveState", saveStateStr);
                income_information.put("DO","MODIFYOFBUDGET");
                income_information.put("dateHappened", dateHappenedEditText.getText().toString());
                income_information.put("idBudget", ID_BUDGET);

                AsyncTaskIncomeProcess task = new AsyncTaskIncomeProcess(ROOTVIEW,income_information);
                task.execute();

                descriptionEditText.setEnabled(false);
                amountEditText.setEnabled(false);
                dateHappenedEditText.setEnabled(false);
                menuActionbar.getItem(editButtonIndex).setEnabled(false);

            }else {
                toast =  Toast.makeText(getActivity(),messageError.trim(),Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(id_item == android.R.id.home){

            Intent intentHome = new Intent(ROOTVIEW.getContext(), BudgetViewController.class);
            startActivity(intentHome);
        }

        return true;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menuActionbar = menu;

        menu.getItem(editButtonIndex).setEnabled(false);
    }

    public static void populateIncomeListView(View rootview){


        ArrayList<BudgetedIncomeModel> IncomeArray = IncomeDataController.getIncomesByBudget(rootview.getContext(), ID_BUDGET);
        String[] error = {"No hay ingresos presupuestados."};

        if (!IncomeArray.isEmpty()) {
            ListViewAdapterModifyBudgetedIncome IncomeAdapter = new ListViewAdapterModifyBudgetedIncome(
                    rootview.getContext(),
                    R.layout.modify_income_listview_layout,
                    rootview,
                    IncomeArray);

            incomeListView.setAdapter(IncomeAdapter);
        }
        else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
            incomeListView.setAdapter(errorAdapter);
        }
    }

    public void InitViews(View rootview){

        incomeListView = (ListView)   rootview.findViewById(R.id.income_modify_listview);
        descriptionEditText = (EditText) rootview.findViewById(R.id.descripction_income);
        amountEditText = (EditText) rootview.findViewById(R.id.amount_income);
        dateHappenedEditText = (EditText) rootview.findViewById(R.id.DateHappened);

    }
}
