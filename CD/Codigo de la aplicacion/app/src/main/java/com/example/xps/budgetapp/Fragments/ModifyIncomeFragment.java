package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskIncomeProcess;
import com.example.xps.budgetapp.ClickListiners.SelectRecurrenceClick;
import com.example.xps.budgetapp.ClickListiners.ShowHelpDialog;
import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.Controllers.PrincipalMenuViewController;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterModifyIncomeFragment;
import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by XPS on 11/11/2014.
 */
public class ModifyIncomeFragment extends Fragment {

    private boolean saveState;
    private View ROOTVIEW;
    public static String id_income = "";
    private static  ListView incomeListView;
    private EditText descriptionEditText;
    private EditText amountEditText;
    private Spinner recurrenceSpinner;
    private MultipleSpinnerSelection dayHappenedSpinner;
    public ImageButton helpButton;
    public static Menu menuActionbar;
    int editButtonIndex = 0;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.modify_income_fragment_layout, container, false);

        this.ROOTVIEW = rootView;

        InitViews(rootView);

        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionEditText.length() != 0 || !descriptionEditText.getText().toString().equals("")) {

                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountEditText.length() != 0 || !amountEditText.getText().toString().equals("")) {

                    saveState = true;

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        recurrenceSpinner.setEnabled(false);
        recurrenceSpinner.setOnItemSelectedListener(new SelectRecurrenceClick(rootView));
        helpButton.setOnClickListener(new ShowHelpDialog(getFragmentManager(),"dayHappenedHelp"));

        populateDaySpinner();
        PopulateIncomeListView(rootView);
        PopulateRecurrenceSpinner(rootView);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.modify_income_fragment_menu,menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();

        if(id_item == R.id.edit_button){

            String description = descriptionEditText.getText().toString().trim();
            String amount = amountEditText.getText().toString().trim();
            String recurrenceSelected = String.valueOf(recurrenceSpinner.getSelectedItemPosition());
            String dayHappened = dayHappenedSpinner.getSelectedItemsAsString();
            saveState  = true;
            String SaveStateStr;
            String messageError = "";

            HashMap<String, String> incomeInformation = new HashMap<String, String>();

            Toast toast;

            if(description.length() == 0 || description.matches("\\s+")){
                messageError += "La descripcion  del ingreso no puede estar vacia.\n";
                saveState = false;
            }

            if (amount.length() == 0) {
                messageError += "El monto del ingreso no puede estar vacio.\n";
                saveState = false;
            }

            if (recurrenceSelected.equals("1")) {
                if (dayHappened.equals("")) {
                    messageError += "\n Si es un ingreso fijo Dia de ocurrencia no puede estar vacio.";
                    saveState = false;
                } else {
                    saveState = true;
                }
            }

            SaveStateStr = String.valueOf(saveState);

            if (saveState) {

                incomeInformation.put("idIncome", id_income);
                incomeInformation.put("description", description);
                incomeInformation.put("amount", amount);
                incomeInformation.put("recurrence", recurrenceSelected);
                incomeInformation.put("dayHappened", dayHappened);
                incomeInformation.put("saveState", SaveStateStr);
                incomeInformation.put("DO", "MODIFY");

                AsyncTaskIncomeProcess task = new AsyncTaskIncomeProcess(ROOTVIEW, incomeInformation);
                task.execute();

                descriptionEditText.setEnabled(false);
                amountEditText.setEnabled(false);
                recurrenceSpinner.setEnabled(false);
                dayHappenedSpinner.setEnabled(false);
                menuActionbar.getItem(editButtonIndex).setEnabled(false);

            }else {
                toast =  Toast.makeText(getActivity(), messageError.replaceAll("^\\s+", ""), Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(id_item == android.R.id.home){

            Intent intentHome = new Intent(getActivity().getBaseContext(),PrincipalMenuViewController.class);
            startActivity(intentHome);
        }

        return true;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menuActionbar = menu;

        menu.getItem(editButtonIndex).setEnabled(false);
    }

    public static void PopulateIncomeListView(View rootview){

        ArrayList<IncomeModel> income_array = IncomeDataController.getIncomesInformation(rootview.getContext());
        String[] error = {"No hay ingresos insertados."};

        if (!income_array.isEmpty()) {
            ListViewAdapterModifyIncomeFragment AdapterIncome = new ListViewAdapterModifyIncomeFragment(
                    rootview.getContext(),
                    R.layout.modify_income_listview_layout,
                    rootview,
                    income_array);

            incomeListView.setAdapter(AdapterIncome);
        }
        else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),
                                                                         android.R.layout.simple_list_item_1,
                                                                         error);
            incomeListView.setAdapter(errorAdapter);
        }

    }

    public void PopulateRecurrenceSpinner(View rootview){

        List<String> list = new ArrayList<String>();

        list.add("Variable");
        list.add("Fijo");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(rootview.getContext(),
                android.R.layout.simple_spinner_item,list);

        recurrenceSpinner.setAdapter(dataAdapter);
    }

    public void populateDaySpinner(){

        String[] list = getResources().getStringArray(R.array.day_of_month);
        dayHappenedSpinner.setItems(list);
    }

    public void InitViews(View rootview){

        incomeListView = (ListView)   rootview.findViewById(R.id.income_modify_listview);
        descriptionEditText = (EditText) rootview.findViewById(R.id.descripction_income);
        amountEditText = (EditText) rootview.findViewById(R.id.amount_income);
        recurrenceSpinner = (Spinner) rootview.findViewById(R.id.RecurrenceSpinner);
        dayHappenedSpinner = (MultipleSpinnerSelection) rootview.findViewById(R.id.DayHappened);
        helpButton  = (ImageButton) rootview.findViewById(R.id.help_button);

    }

}