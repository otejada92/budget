package com.example.xps.budgetapp.CustomView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by XPS on 3/26/2015.
 */
public class DatePickerYear extends DialogFragment implements DatePickerDialog.OnDateSetListener {


    int year;
    int month;
    int day;
    private EditText KEEPER;

    SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

    public DatePickerYear(EditText keeper){

        this.KEEPER = keeper;

    }

    public DatePickerYear(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar calendar;

        if (this.KEEPER.getText().toString().equals("")) {

            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        } else {

            String year_value = this.KEEPER.getText().toString();
            year = Integer.valueOf(year_value);

        }

        return new DatePickerDialog(getActivity(), DatePickerYear.this, year, month, day) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);

                int month = getContext().getResources().getIdentifier("android:id/month", null, null);
                int day = getContext().getResources().getIdentifier("android:id/day", null, null);

                if (month != 0) {
                    View yearPicker = findViewById(month);
                    if (yearPicker != null) {
                        yearPicker.setVisibility(View.GONE);
                    }
                }

                if (day != 0) {
                    View yearPicker = findViewById(day);
                    if (yearPicker != null) {
                        yearPicker.setVisibility(View.GONE);
                    }
                }
            }

        };
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        String[] date_value = dateFormatter.format(newDate.getTime()).split("\\-");
        this.KEEPER.setText(date_value[2]);
    }

}
