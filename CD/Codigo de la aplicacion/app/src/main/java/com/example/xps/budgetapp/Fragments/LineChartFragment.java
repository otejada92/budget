package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.xps.budgetapp.CustomAdapters.LineChartListViewAdapter;
import com.example.xps.budgetapp.Dialogs.ExpenseLineaDetailDialog;
import com.example.xps.budgetapp.Dialogs.IncomeLineaDetailDialog;

import com.example.xps.budgetapp.Models.LineExpenseModel;
import com.example.xps.budgetapp.Models.LineIncomeModel;
import com.example.xps.budgetapp.Models.TwoLineChartModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;


public class LineChartFragment extends Fragment{

    View parentRootView;
    LineChart lineChart;
    IncomeLineaDetailDialog incomeDetail;
    ExpenseLineaDetailDialog expenseDetail;
    public static ArrayList<LineIncomeModel> incomeList;
    public static ArrayList<LineExpenseModel> expenseList;
    String toShow;
    int index = 0;
    ListView listView;

    public LineChartFragment(String toshow) {this.toShow = toshow;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.toShow.equals("ingresos") || this.toShow.equals("gastos")) {
            parentRootView = inflater.inflate(R.layout.one_line_fragment_layout, container, false);
            lineChart = (LineChart) parentRootView.findViewById(R.id.linearChart);
        }
        else if(this.toShow.equals("Ingreso y Gasto")){
            parentRootView = inflater.inflate(R.layout.two_line_fragment, container, false);
            listView = (ListView) parentRootView.findViewById(R.id.linearChartContrainer);
        }

        showLineChart();

        return parentRootView;
    }

    public void showLineChart(){

        if (this.toShow.equals("ingresos")) {
            setLineChartIncome();
            lineChart.getLegend().setCustom(new int[]{Color.rgb(8, 0, 255)}, new String[]{"Ingreso"});
            setLineChartProperties(lineChart);
        }

        if (this.toShow.equals("gastos")) {
            setLineChartExpense();
            lineChart.getLegend().setCustom(new int[]{Color.rgb(250, 11, 11)}, new String[]{"Gasto"});
            setLineChartProperties(lineChart);

        }

        if (this.toShow.equals("Ingreso y Gasto")){
            setLineChartWithIncomeAndExpense();
        }

    }

    public void setLineChartIncome(){

        LineData  lineData = getLineDataIncome();
        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    public void setLineChartExpense(){

        LineData lineData = getLineDataExpense();
        lineChart.setData(lineData);
        lineChart.invalidate();

    }

    public  void setLineChartWithIncomeAndExpense(){

        ArrayList<TwoLineChartModel> listInformation = new ArrayList<TwoLineChartModel>();
        TwoLineChartModel subModelInformation = new TwoLineChartModel(incomeList,expenseList);

        listInformation.add(subModelInformation);

        LineChartListViewAdapter adapter = new LineChartListViewAdapter(
                getActivity().getBaseContext(),R.layout.listview_line_chart_layout,listInformation,getFragmentManager());

        listView.setAdapter(adapter);
    }

    public LineData getLineDataIncome(){

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();


        for (final LineIncomeModel income : incomeList){
            xVals.add(ManagerDate.ConvertToUserDateFormat(income.getBUDGETED_HAPPENED()));
            yVals.add(new Entry(Float.valueOf(income.getINCOMESUM()), index));

            lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                        incomeDetail = new IncomeLineaDetailDialog(incomeList.get(e.getXIndex()).getINCOMELIST());
                        incomeDetail.show(getFragmentManager(), "IncomeDetail");

                }

                @Override
                public void onNothingSelected() {

                }
            });
            index++;
        }

        LineDataSet lineDataSet = new LineDataSet(yVals,"");

        lineDataSet.setDrawCubic(true);
        lineDataSet.setDrawCircleHole(true);
        lineDataSet.setCubicIntensity(0.1f);
        lineDataSet.setLineWidth(5f);
        lineDataSet.setCircleSize(5f);
        lineDataSet.setFillAlpha(65);
        lineDataSet.setDrawValues(false);
        lineDataSet.setColor(Color.rgb(8, 0, 255));
        lineDataSet.setValueTextColor(Color.rgb(0, 0, 0));
        lineDataSet.setValueTextSize(5f);

        return new LineData(xVals,lineDataSet);

    }

    public LineData getLineDataExpense(){

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (LineExpenseModel expense : expenseList){
            xVals.add(ManagerDate.ConvertToUserDateFormat(expense.getBUDGETED_HAPPENED()));
            yVals.add(new Entry(Float.valueOf(expense.getEXPENSESUM()), index));
            lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    expenseDetail = new ExpenseLineaDetailDialog(expenseList.get(e.getXIndex()).getEXPENSELIST());
                    expenseDetail.show(getFragmentManager(), "ExpenseDetail");

                }

                @Override
                public void onNothingSelected() {

                }
            });

            index++;
        }

        LineDataSet lineDataSet = new LineDataSet(yVals,"");

        lineDataSet.setDrawCubic(true);
        lineDataSet.setDrawCircleHole(true);
        lineDataSet.setDrawValues(false);
        lineDataSet.setCubicIntensity(0.1f);
        lineDataSet.setLineWidth(5f);
        lineDataSet.setCircleSize(5f);
        lineDataSet.setFillAlpha(65);
        lineDataSet.setColor(Color.rgb(250, 11, 11));
        lineDataSet.setValueTextColor(Color.rgb(0,0,0));
        lineDataSet.setValueTextSize(5f);

        return new LineData(xVals,lineDataSet);

    }

    public ArrayList<String> getXvals(){

        ArrayList<String> dateList = new ArrayList<String>();

        for (LineIncomeModel income : incomeList){
            if(!dateList.contains(ManagerDate.ConvertToUserDateFormat(income.getBUDGETED_HAPPENED())))
                dateList.add(ManagerDate.ConvertToUserDateFormat(income.getBUDGETED_HAPPENED()));
        }

        for (LineExpenseModel expense : expenseList){
            if(!dateList.contains(ManagerDate.ConvertToUserDateFormat(expense.getBUDGETED_HAPPENED())))
                dateList.add(ManagerDate.ConvertToUserDateFormat(expense.getBUDGETED_HAPPENED()));
        }
        
        Collections.sort(dateList, new Comparator<String>() {
            DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            @Override
            public int compare(String date1, String date2) {
                try {
                    return dateFormat.parse(date1).compareTo(dateFormat.parse(date2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
        return dateList;
    }

    public  void setLineChartProperties(LineChart lineChart){

        lineChart.setDescription("");
        lineChart.setNoDataText("No hay data por el momento");
        lineChart.setHighlightEnabled(true);

        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);

        lineChart.setPinchZoom(true);
        lineChart.getLegend().setEnabled(false);
        lineChart.animateX(3000, Easing.EasingOption.EaseInOutQuart);

    }

}
