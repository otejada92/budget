package com.example.xps.budgetapp.Processes;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Interfaces.InsertStrategy;

/**
 * Created by XPS on 7/4/2015.
 */
public class InsertProcesses implements InsertStrategy {


    @Override
    public void InsertResource(Context context, String tableName, ContentValues resourceValues) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase tableManager = db.getWritableDatabase();

        tableManager.insert(tableName,null,resourceValues);

    }
}
