package com.example.xps.budgetapp.AsyncTask;

import android.os.AsyncTask;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyIncome;
import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.Fragments.AddIncomeToBudgetFragment;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Fragments.ModifyBudgetedIncomesFragment;
import com.example.xps.budgetapp.Fragments.ModifyIncomeFragment;
import com.example.xps.budgetapp.Processes.IncomeProcesses;

import java.util.HashMap;


public class AsyncTaskIncomeProcess extends AsyncTask<Void, Void, Boolean> {

    private View ROOTVIEW;
    private HashMap<String, String> INCOME_INFORMATION;

    public AsyncTaskIncomeProcess(View rootview, HashMap<String, String> income_information) {

        this.ROOTVIEW = rootview;
        this.INCOME_INFORMATION = income_information;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        ContextStrategyIncome contextStrategyIncome = new ContextStrategyIncome(new IncomeProcesses());

        if (this.INCOME_INFORMATION.get("saveState").equals("true")) {
              if(this.INCOME_INFORMATION.get("DO").equals("SAVE")) {
                  contextStrategyIncome.executeStrategyAdd(this.ROOTVIEW.getContext(),
                          INCOME_INFORMATION.get("description"),
                          INCOME_INFORMATION.get("amount"),
                          INCOME_INFORMATION.get("recurrence"),
                          INCOME_INFORMATION.get("dayHappened"));
                  return true;
              }

            if(this.INCOME_INFORMATION.get("DO").equals("MODIFY")) {
                contextStrategyIncome.executeStrategyModify(this.ROOTVIEW.getContext(),
                        INCOME_INFORMATION.get("idIncome"),
                        INCOME_INFORMATION.get("description"),
                        INCOME_INFORMATION.get("amount"),
                        INCOME_INFORMATION.get("recurrence"),
                        INCOME_INFORMATION.get("dayHappened"));
                return true;
            }

            if(this.INCOME_INFORMATION.get("DO").equals("ADDTOBUDGET")) {
                IncomeDataController.AddIncomeToBudget(this.ROOTVIEW.getContext(),
                                                       INCOME_INFORMATION.get("description"),
                                                       INCOME_INFORMATION.get("amount"),
                                                       INCOME_INFORMATION.get("idBudget"),
                                                       INCOME_INFORMATION.get("dateHappened"));

                ContextStrategyIncome context = new ContextStrategyIncome(new IncomeProcesses());
                String[] date = INCOME_INFORMATION.get("dateHappened").split("-");
                String day = date[1];
                context.executeStrategyAdd(this.ROOTVIEW.getContext(),INCOME_INFORMATION.get("description"),INCOME_INFORMATION.get("amount"),"1",day);
                return true;
            }

            if(this.INCOME_INFORMATION.get("DO").equals("MODIFYOFBUDGET")) {
                IncomeDataController.modifyBudgetedIncome(
                        this.ROOTVIEW,
                        INCOME_INFORMATION.get("idIncome"),
                        INCOME_INFORMATION.get("description"),
                        INCOME_INFORMATION.get("amount"),
                        INCOME_INFORMATION.get("idBudget"),
                        INCOME_INFORMATION.get("dateHappened"));
                return true;
            }
        }

        return false;

    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        Toast toast;
        Cleaner clean_helper = new Cleaner();
        if (aBoolean) {

            if (this.INCOME_INFORMATION.get("DO").equals("SAVE")) {
                clean_helper.cleanView((RelativeLayout) this.ROOTVIEW);
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Se ha guardado su ingreso.", Toast.LENGTH_LONG);
                toast.show();
            }

            if (this.INCOME_INFORMATION.get("DO").equals("MODIFY")) {
                clean_helper.cleanView((RelativeLayout) this.ROOTVIEW);
                ModifyIncomeFragment.PopulateIncomeListView(this.ROOTVIEW);
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Se ha actualizado su ingreso.", Toast.LENGTH_LONG);
                toast.show();
            }
            if (this.INCOME_INFORMATION.get("DO").equals("ADDTOBUDGET")){
                clean_helper.cleanView((RelativeLayout) this.ROOTVIEW);
                AddIncomeToBudgetFragment.PopulateIncomeListView((RelativeLayout)this.ROOTVIEW);
                toast = Toast.makeText(this.ROOTVIEW.getContext(), "Su ingreso ha sido presupuestado.", Toast.LENGTH_SHORT);
                toast.show();
            }
            if (this.INCOME_INFORMATION.get("DO").equals("MODIFYOFBUDGET")){
                clean_helper.cleanView((RelativeLayout) ROOTVIEW);
                ModifyBudgetedIncomesFragment.populateIncomeListView(ROOTVIEW);
                toast = Toast.makeText(ROOTVIEW.getContext(), "Se ha actualizado su ingreso.", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }


}