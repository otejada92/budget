package com.example.xps.budgetapp.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Services.EndBudgetServices;
import com.example.xps.budgetapp.Services.StartBudgetServices;

/**
 * Created by XPS on 7/19/2015.
 */
public class StartBudgetReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent service = new Intent(context,StartBudgetServices.class);

        service.putExtra("idAlarm",intent.getStringExtra("idAlarm"));
        service.putExtra("idBudget",intent.getStringExtra("idBudget"));

        context.startService(service);
    }

}
