package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyInsertResource;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.Processes.InsertProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 7/4/2015.
 */
public class ListViewAdapterAddExpenseToBudgetFragment extends  ArrayAdapter<ExpenseModel>{


    private Context CONTEXT;
    private final ArrayList<ExpenseModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;
    ManagerDate managerDate = new ManagerDate();
    private String ID_BUDGET;
    private GenericBudgetModel budgetInformation;

    public ListViewAdapterAddExpenseToBudgetFragment(Context context, int resource, View rootview, ArrayList<ExpenseModel> Data, String id_budget) {
        super(context, resource,Data);
        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;
        this.ID_BUDGET = id_budget;
        getBudgetInformation(rootview,id_budget);


    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder Holder;

        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView=inflater.inflate(this.RESOURCE,parent,false);

            Holder=new ViewHolder();

            Holder.textViewDesc=(TextView)convertView.findViewById(R.id.descripcion);
            Holder.textViewAmount=(TextView)convertView.findViewById(R.id.amount);
            Holder.addButton =(ImageButton) convertView.findViewById(R.id.anadir);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }

        Holder.textViewDesc.setId(Integer.parseInt(this.DATA.get(position).getID_EXPENSE()));
        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getDESCRIPTION();
        String amountString = "<b>Monto:$ </b>"+ this.DATA.get(position).getAMOUNT();

        Holder.textViewDesc.setText(Html.fromHtml(descString));

        Holder.textViewAmount.setText(Html.fromHtml(amountString));


        Holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);
                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                final String tableName = Expense_Budget_Table.TABLE_NAME;
                final ContentValues expenseValues = new ContentValues();

                textView.setText("Estas seguro que deseas agregar este gasto al presupuestar?");

                String categoryDesc = CategoryDataController.getCategoriaDescByID(CONTEXT, DATA.get(position).getID_CATEGORY());

                expenseValues.put(Expense_Budget_Table.DESCRIPTION, DATA.get(position).getDESCRIPTION());
                expenseValues.put(Expense_Budget_Table.AMOUNT, DATA.get(position).getAMOUNT());
                expenseValues.put(Expense_Budget_Table.ID_CATEGORY, DATA.get(position).getID_CATEGORY());
                expenseValues.put(Expense_Budget_Table.CATEGORY_DESC,categoryDesc);
                expenseValues.put(Expense_Budget_Table.DATE_BDGETED, ManagerDate.ConvertToSqlLiteDateFormat(budgetInformation.getSTART_DATE()));
                expenseValues.put(Expense_Budget_Table.ID_BUDGET, ID_BUDGET);

                AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
                builder
                        .setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyInsertResource contextStrategyInsertResource = new ContextStrategyInsertResource(new InsertProcesses());
                                        contextStrategyInsertResource.executeStategy(CONTEXT, tableName, expenseValues);
                                    }
                                }
                        )
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();
            }
        });

        return convertView;
    }

    static class ViewHolder {

        TextView textViewDesc;
        TextView textViewAmount;
        ImageButton addButton;

    }

    public void getBudgetInformation(View rootView,String id_budget){

        budgetInformation = BudgetDataController.getBudgetByID(rootView.getContext(), id_budget);

    }
}
