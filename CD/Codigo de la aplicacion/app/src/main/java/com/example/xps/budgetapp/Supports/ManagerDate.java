package com.example.xps.budgetapp.Supports;


import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TreeMap;


/**
 * Created by XPS on 10/28/2014.
 */
public class ManagerDate  {

    private HashMap<String, String> Months = new HashMap<String, String>();
    private TreeMap<String, String> RangeMonthNormal = new TreeMap<String, String>();
    private TreeMap<String, String> RangeMonthBi = new TreeMap<String, String>();

    private static  SimpleDateFormat SqlLiteFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
    private static SimpleDateFormat UserFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
    private SimpleDateFormat SqlLiteFormatWithHour = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSSSS");

    private Context CONTEXT;

    public ManagerDate(Context context) {
        this.CONTEXT = context;
    }

    public ManagerDate() {
    }

    public void Set_Hmap_MonthStringNumber(){

        Months.put("Enero", "01");
        Months.put("Febrero", "02");
        Months.put("Marzo", "03");
        Months.put("Abril", "04");
        Months.put("Mayo", "05");
        Months.put("Junio", "06");
        Months.put("Julio", "07");
        Months.put("Agosto", "08");
        Months.put("Septiembre", "09");
        Months.put("Octubre", "10");
        Months.put("Noviembre", "11");
        Months.put("Diciembre", "12");
    }

    public void Set_Hmap_MonthNumberString(){

        Months.put("01", "Enero");
        Months.put("02", "Febrero");
        Months.put("03", "Marzo");
        Months.put("04", "Abril");
        Months.put("05", "Mayo");
        Months.put("06", "Junio");
        Months.put("07", "Julio");
        Months.put("08", "Agosto");
        Months.put("09", "Septiembre");
        Months.put("10", "Octubre");
        Months.put("11", "Noviembre");
        Months.put("12", "Diciembre");
    }

    public TreeMap<String,String> getHmapMonthRangeNormal(){

        RangeMonthNormal.put("01", "/01/01,/01/31");
        RangeMonthNormal.put("02", "/02/01,/02/28");
        RangeMonthNormal.put("03", "/03/01,/03/31");
        RangeMonthNormal.put("04", "/04/01,/04/30");
        RangeMonthNormal.put("05", "/05/01,/05/31");
        RangeMonthNormal.put("06", "/06/01,/06/30");
        RangeMonthNormal.put("07", "/07/01,/07/31");
        RangeMonthNormal.put("08", "/08/01,/08/31");
        RangeMonthNormal.put("09", "/09/01,/09/30");
        RangeMonthNormal.put("10", "/10/01,/10/31");
        RangeMonthNormal.put("11", "/11/01,/11/30");
        RangeMonthNormal.put("12", "/12/01,/12/31");

        return RangeMonthNormal;
    }

    public TreeMap<String,String> getHmapMonthRangeBi(){

        RangeMonthBi.put("01","/01/01,/01/31");
        RangeMonthBi.put("02","/02/01,/02/29");
        RangeMonthBi.put("03","/03/01,/03/31");
        RangeMonthBi.put("04", "/04/01,/04/30");
        RangeMonthBi.put("05", "/05/01,/05/31");
        RangeMonthBi.put("06", "/06/01,/06/30");
        RangeMonthBi.put("07", "/07/01,/07/31");
        RangeMonthBi.put("08", "/08/01,/08/31");
        RangeMonthBi.put("09", "/09/01,/09/30");
        RangeMonthBi.put("10", "/10/01,/10/31");
        RangeMonthBi.put("11", "/11/01,/11/30");
        RangeMonthBi.put("12", "/12/01,/12/31");

        return RangeMonthBi;
    }

    public static String ConvertToSqlLiteDateFormat(String StrDate){

        SimpleDateFormat fromUser = new SimpleDateFormat("MM-dd-yyyy");
        SimpleDateFormat SqlLitFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            StrDate = SqlLitFormat.format(fromUser.parse(StrDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return StrDate;
    }

    public static String ConvertToUserDateFormat(String StrDate){

        SimpleDateFormat SqlLitFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            StrDate = UserFormat.format(SqlLitFormat.parse(StrDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return StrDate;
    }

    public   String getCurrentDateSqlLiteFormat(){
        return SqlLiteFormat.format(Calendar.getInstance().getTime());
    }
    
    public String GetCurrentDateUserFormat(){
        return UserFormat.format(Calendar.getInstance().getTime());
    }

    public String getCurrentDateWithTime(){
        return SqlLiteFormatWithHour.format(Calendar.getInstance().getTime());
    }

    public String GetMonthNumber(String month){

        Set_Hmap_MonthStringNumber();

        return Months.get(month);
    }

    public String getMonthName(String numberMonth){

        Set_Hmap_MonthNumberString();

        return Months.get(numberMonth);
    }

    public String ValidateDateHappened(String start_dateBudgetStr, String end_dateBudgetStr, String TransactionDateHappened){

        String ErrorToReturn = "";
        String Case_Error1 = "";
        String Case_Error2 = "";

        TransactionDateHappened = ManagerDate.ConvertToSqlLiteDateFormat(TransactionDateHappened);
        start_dateBudgetStr = ManagerDate.ConvertToSqlLiteDateFormat(start_dateBudgetStr);
        end_dateBudgetStr = ManagerDate.ConvertToSqlLiteDateFormat(end_dateBudgetStr);

        Date transaction_dateHappened = new Date();
        Date start_dateBudget = new Date();
        Date end_dateBudget = new Date();

        try {

            transaction_dateHappened = SqlLiteFormat.parse(TransactionDateHappened);
            start_dateBudget = SqlLiteFormat.parse(start_dateBudgetStr);
            end_dateBudget = SqlLiteFormat.parse(end_dateBudgetStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (transaction_dateHappened.before(start_dateBudget)) {
            Case_Error1 = " La fecha ocurrido no puede ser menor que la fecha de inicio del presupuesto.";
            ErrorToReturn += Case_Error1+"\n";

        }

        if (transaction_dateHappened.after(end_dateBudget)) {
            Case_Error2 = " La fecha ocurrido no puede ser mayor que la fecha de finalizacion del presupuesto.";
            ErrorToReturn += Case_Error2+"\n";
        }
        return ErrorToReturn;
    }

    public String parseCalendarToStringSqlFormat(Calendar dateCalendar){
        return SqlLiteFormat.format(dateCalendar.getTime());
    }

    public Boolean areDateEquals(String firstDate,String secondDate) throws ParseException {
        return SqlLiteFormat.parse(firstDate).equals(SqlLiteFormat.parse(secondDate));
    }

    public int getAlarmId(){

        return (int) System.currentTimeMillis();
    }

    public Boolean isTheStartServiceNeed(String startDate) throws ParseException {

        ManagerDate managerDate = new ManagerDate();

        return SqlLiteFormat.parse(startDate).after(SqlLiteFormat.parse(managerDate.getCurrentDateSqlLiteFormat()));
    }
}

