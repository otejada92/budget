package com.example.xps.budgetapp.Controllers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

import com.example.xps.budgetapp.Fragments.AddExpenseToBudgetFragment;
import com.example.xps.budgetapp.Fragments.AddIncomeToBudgetFragment;
import com.example.xps.budgetapp.Fragments.ModifyBudgetedExpensesFragment;
import com.example.xps.budgetapp.Fragments.ModifyBudgetedIncomesFragment;
import com.example.xps.budgetapp.Fragments.ModifyMainBudgetInformationFragment;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.TabListener.TabListinerModifyBudgetView;

public class ModifyBudgetViewController extends Activity {

    ActionBar.Tab Tab1,Tab2, Tab3,Tab4,Tab5;
    Fragment fragmentTab1,fragmentTab2,fragmentTab3,fragmentTab4,fragmentTab5;
    String ID_BUDGET ="";
    String BUDGET_DATE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_modify_budget_view_controller);

        ID_BUDGET = getIntent().getStringExtra("ID_BUDGET");
        BUDGET_DATE =  getIntent().getStringExtra("BUDGET_DATE");
        ActionBar actionBar = getActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        Tab1 = actionBar.newTab().setText("Informacion Principal");
        Tab2 = actionBar.newTab().setText("Añadir Ingreso");
        Tab3 = actionBar.newTab().setText("Modificar Ingresos");
        Tab4 = actionBar.newTab().setText("Añadir Gastos");
        Tab5 = actionBar.newTab().setText("Modificar Gastos");

        fragmentTab1 = new ModifyMainBudgetInformationFragment(ID_BUDGET);
        fragmentTab2 = new AddIncomeToBudgetFragment(ID_BUDGET);
        fragmentTab3 = new ModifyBudgetedIncomesFragment(ID_BUDGET);
        fragmentTab4 = new AddExpenseToBudgetFragment(ID_BUDGET);
        fragmentTab5 = new ModifyBudgetedExpensesFragment(ID_BUDGET);

        Tab1.setTabListener(new TabListinerModifyBudgetView(fragmentTab1));
        Tab2.setTabListener(new TabListinerModifyBudgetView(fragmentTab2));
        Tab3.setTabListener(new TabListinerModifyBudgetView(fragmentTab3));
        Tab4.setTabListener(new TabListinerModifyBudgetView(fragmentTab4));
        Tab5.setTabListener(new TabListinerModifyBudgetView(fragmentTab5));

        actionBar.addTab(Tab1);
        actionBar.addTab(Tab2);
        actionBar.addTab(Tab3);
        actionBar.addTab(Tab4);
        actionBar.addTab(Tab5);
    }

}
