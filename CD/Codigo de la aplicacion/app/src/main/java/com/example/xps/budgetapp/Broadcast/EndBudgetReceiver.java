package com.example.xps.budgetapp.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Services.EndBudgetServices;

/**
 * Created by XPS on 12/4/2014.
 */
public class EndBudgetReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent services = new Intent(context,EndBudgetServices.class);

        services.putExtra("idAlarm",intent.getStringExtra("idAlarm"));
        services.putExtra("idBudget",intent.getStringExtra("idBudget"));


        context.startService(services);
    }
}
