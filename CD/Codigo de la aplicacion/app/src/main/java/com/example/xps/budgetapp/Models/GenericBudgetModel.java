package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 3/31/2015.
 */
public class GenericBudgetModel {


    private String ID_BUDGET;
    private String BUDGET_DESCRIPTION;
    private String BUDGET_GOAL;
    private String START_DATE;
    private String FINISH_DATE;
    private String BUDGET_STATUS;
    private String SINCRONIZADO;
    private  String ID_START_PROCESS;
    private  String ID_END_PROCESS;

    public GenericBudgetModel(String id_budget,
                              String budget_description,
                              String budget_goal,
                              String start_date,
                              String finish_date,
                              String budget_status,
                              String sincronizado,
                              String id_start_process,
                              String id_end_process) {

        this.ID_BUDGET = id_budget;
        this.BUDGET_DESCRIPTION = budget_description;
        this.BUDGET_GOAL = budget_goal;
        this.START_DATE = start_date;
        this.FINISH_DATE = finish_date;
        this.SINCRONIZADO = sincronizado;
        this.BUDGET_STATUS = budget_status;
        this.ID_START_PROCESS = id_start_process;
        this.ID_END_PROCESS = id_end_process;


    }

    public String getSTART_DATE() {
        return START_DATE;
    }

    public void setSTART_DATE(String START_DATE) {
        this.START_DATE = START_DATE;
    }

    public String getFINISH_DATE() {
        return FINISH_DATE;
    }

    public void setFINISH_DATE(String FINISH_DATE) {
        this.FINISH_DATE = FINISH_DATE;
    }

    public String getID_BUDGET() {
        return ID_BUDGET;
    }

    public void setID_BUDGET(String ID_BUDGET) {
        this.ID_BUDGET = ID_BUDGET;
    }

    public String getBUDGET_DESCRIPTION() {
        return BUDGET_DESCRIPTION;
    }

    public void setBUDGET_DESCRIPTION(String BUDGET_DESCRIPTION) {
        this.BUDGET_DESCRIPTION = BUDGET_DESCRIPTION;
    }

    public String getBUDGET_GOAL() {
        return BUDGET_GOAL;
    }

    public void setBUDGET_GOAL(String BUDGET_GOAL) {
        this.BUDGET_GOAL = BUDGET_GOAL;
    }

    public String getID_END_PROCESS() {
        return ID_END_PROCESS;
    }

    public void setID_END_PROCESS(String ID_END_PROCESS) {
        this.ID_END_PROCESS = ID_END_PROCESS;
    }

    public String getSINCRONIZADO() {
        return SINCRONIZADO;
    }

    public void setSINCRONIZADO(String SINCRONIZADO) {
        this.SINCRONIZADO = SINCRONIZADO;
    }

    public String getBUDGET_STATUS() {
        return BUDGET_STATUS;
    }

    public void setBUDGET_STATUS(String BUDGET_STATUS) {
        this.BUDGET_STATUS = BUDGET_STATUS;
    }

    public String getID_START_PROCESS() {
        return ID_START_PROCESS;
    }

    public void setID_START_PROCESS(String ID_START_PROCESS) {
        this.ID_START_PROCESS = ID_START_PROCESS;
    }
}
