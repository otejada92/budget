package com.example.xps.budgetapp.Interfaces;

import android.content.Context;

/**
 * Created by XPS on 2/13/2015.
 */
public interface ExpenseStrategy {


    void Save(Context context, String Description, String Amount, String Recurrence, String id_category, String DateHappened);

    void Modify(Context context, String Id_expense, String Descripction, String Amount, String Recurrence, String id_category, String DateHappened);
}
