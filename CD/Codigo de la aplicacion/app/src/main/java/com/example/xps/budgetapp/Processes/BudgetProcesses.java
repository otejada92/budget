package com.example.xps.budgetapp.Processes;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.example.xps.budgetapp.AlarmServicesConfigurations.EndBudgetAlarm;
import com.example.xps.budgetapp.AlarmServicesConfigurations.StartBudgetAlarm;
import com.example.xps.budgetapp.AsyncTask.AsyncTaskBudgetProcess;
import com.example.xps.budgetapp.Broadcast.EndBudgetReceiver;
import com.example.xps.budgetapp.Broadcast.StartBudgetReceiver;
import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Supports.IdFactory;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Interfaces.BudgetStrategy;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Tables.Budget_Table;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by XPS on 2/25/2015.
 */
public class BudgetProcesses implements BudgetStrategy {

    public EditText StartDateEditText;
    public EditText EndDateEditText;
    public View ROOTVIEW;
    public EditText GoalEditText;
    public EditText DescriptionBudgetText;
    public Switch PredeterminateSwitch;
    public ManagerDate managerDate = new ManagerDate();
    public Calendar calendar = Calendar.getInstance();
    public SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);


    public BudgetProcesses(View rootview) {

        this.ROOTVIEW = rootview;
    }

    public BudgetProcesses() {
    }

    @Override
    public void SaveBudget(View rootView, String idBudget, String budgetDescription, String goalBudget, String startDate,String endDate,String budgetState,String idStartBudgetProcess,String idEndtBudgetProcess,String sincronizado) throws ParseException {

        DataBaseController db = new DataBaseController(rootView.getContext());
        SQLiteDatabase budget_table = db.getWritableDatabase();
        ContentValues budget_values = new ContentValues();

        budget_values.put(Budget_Table.ID_BUDGET, idBudget);
        budget_values.put(Budget_Table.BUDGET_DESCRIPTION, budgetDescription);
        budget_values.put(Budget_Table.BUDGET_GOAL,goalBudget);
        budget_values.put(Budget_Table.INIT_DATE, startDate);
        budget_values.put(Budget_Table.END_DATE, endDate);
        budget_values.put(Budget_Table.BUDGET_STATUS, budgetState);
        budget_values.put(Budget_Table.SINCRONIZADO,sincronizado);

        if (managerDate.isTheStartServiceNeed(startDate)) {
            budget_values.put(Budget_Table.ID_ALARM_START_PROCESS,idStartBudgetProcess);
            StartBudgetAlarm startBudgetAlarm = new StartBudgetAlarm(rootView.getContext(), startDate.split("/"), idBudget, Integer.valueOf(idStartBudgetProcess));
            startBudgetAlarm.setStartBudgetServices();
        }else{
            budget_values.put(Budget_Table.ID_ALARM_START_PROCESS,"0");
        }

        budget_values.put(Budget_Table.ID_ALARM_END_PROCESS, idEndtBudgetProcess);

        budget_table.insert(Budget_Table.TABLE_NAME, null, budget_values);

        if(setEndBudgetAlarm(endDate)) {
            EndBudgetAlarm endBudgetAlarm = new EndBudgetAlarm(rootView.getContext(), endDate.split("/"), idBudget, Integer.valueOf(idEndtBudgetProcess));
            endBudgetAlarm.setEndBudgetServices();
        }

        db.close();

    }

    @Override
    public Boolean ModifyMainBudgetInformation(Activity activity, HashMap<String, String> budgetInformation) {

        DataBaseController db = new DataBaseController(activity.getBaseContext());
        SQLiteDatabase table_manager = db.getWritableDatabase();
        String whereClause = Income_Budget_Table.DATE_INSERTED+" NOT BETWEEN "+" ? AND ?";

        if(budgetInformation.get("EndDateChanged").equals("false") && budgetInformation.get("StartDateChanged").equals("false")) {
            table_manager.execSQL("UPDATE " + Budget_Table.TABLE_NAME +
                    " SET " + Budget_Table.BUDGET_DESCRIPTION + "=?," +
                    Budget_Table.BUDGET_GOAL + "=?," +
                    Budget_Table.INIT_DATE + "=?," +
                    Budget_Table.END_DATE + "=?," +
                    Budget_Table.SINCRONIZADO + "=?" +
                    " WHERE " + Budget_Table.ID_BUDGET + "=?"
                    , new String[]{budgetInformation.get("Description"), budgetInformation.get("Goal"),
                    budgetInformation.get("StartDate"), budgetInformation.get("EndDate"),
                    budgetInformation.get("Sincronizar"), budgetInformation.get("ID_BUDGET")});
            return true;
        }

        if(budgetInformation.get("EndDateChanged").equals("true") || budgetInformation.get("StartDateChanged").equals("true")) {

            AlarmManager alarmManager = (AlarmManager) activity.getBaseContext().getSystemService(Context.ALARM_SERVICE);
            ManagerDate managerDate = new ManagerDate();

            int newIdStartAlarmBudget = managerDate.getAlarmId();
            int oldIdStartAlarmBudget = Integer.parseInt(budgetInformation.get("IdStartBudgetAlarm"));

            int newIdEndAlarmBudget = managerDate.getAlarmId();
            int oldIdEndAlarmBudget = Integer.parseInt(budgetInformation.get("IdEndBudgetAlarm"));

            String budgetStatus = getBudgetStatus(budgetInformation.get("StartDate"));

            table_manager.execSQL("UPDATE " + Budget_Table.TABLE_NAME +
                    " SET " + Budget_Table.BUDGET_DESCRIPTION + "= ? ," +
                    Budget_Table.BUDGET_GOAL + "=?," +
                    Budget_Table.INIT_DATE + "=?," +
                    Budget_Table.END_DATE + "=?," +
                    Budget_Table.SINCRONIZADO + "=?," +
                    Budget_Table.BUDGET_STATUS + "=?," +
                    Budget_Table.ID_ALARM_START_PROCESS + "=?," +
                    Budget_Table.ID_ALARM_END_PROCESS + "=?" +
                    " WHERE " + Budget_Table.ID_BUDGET + "=?"
                    , new String[]{budgetInformation.get("Description"),
                    budgetInformation.get("Goal"),
                    budgetInformation.get("StartDate"),
                    budgetInformation.get("EndDate"),
                    budgetInformation.get("Sincronizar"),
                    budgetStatus,
                    String.valueOf(newIdStartAlarmBudget),
                    String.valueOf(newIdEndAlarmBudget),
                    budgetInformation.get("ID_BUDGET")});
            ContextStrategyDeleteResource deleteResource = new ContextStrategyDeleteResource(new DeleteProcesses());

            deleteResource.executeStrategy(activity.getBaseContext(),Income_Budget_Table.TABLE_NAME,whereClause,new String[]{budgetInformation.get("StartDate"), budgetInformation.get("EndDate")});
            deleteResource.executeStrategy(activity.getBaseContext(),Expense_Budget_Table.TABLE_NAME,whereClause,new String[]{budgetInformation.get("StartDate"), budgetInformation.get("EndDate")});

            if (budgetInformation.get("StartDateChanged").equals("true")) {

                if (setStartBudgetAlarm(budgetInformation.get("StartDate"))) {

                    StartBudgetAlarm startudgetAlarm = new StartBudgetAlarm(activity.getBaseContext(),
                            budgetInformation.get("StartDate").split("/"),
                            budgetInformation.get("ID_BUDGET"),
                            newIdEndAlarmBudget);
                    startudgetAlarm.setStartBudgetServices();
                }else{
                    Intent intentReciver = new Intent(activity.getBaseContext(), StartBudgetReceiver.class);
                    PendingIntent pendingIntentEndServices = PendingIntent.getBroadcast(activity.getBaseContext(),
                            oldIdStartAlarmBudget, intentReciver, PendingIntent.FLAG_NO_CREATE);

                    alarmManager.cancel(pendingIntentEndServices);
                }
            }

            if (budgetInformation.get("EndDateChanged").equals("true")) {

                if(setEndBudgetAlarm(budgetInformation.get("EndDate"))) {
                    EndBudgetAlarm endBudgetAlarm = new EndBudgetAlarm(activity.getBaseContext(),
                            budgetInformation.get("EndDate").split("/"),
                            budgetInformation.get("ID_BUDGET"),
                            newIdEndAlarmBudget);
                    endBudgetAlarm.setEndBudgetServices();
                }else{
                    Intent intentReciver = new Intent(activity.getBaseContext(), EndBudgetReceiver.class);
                    PendingIntent pendingIntentEndServices = PendingIntent.getBroadcast(activity.getBaseContext(),
                            oldIdEndAlarmBudget, intentReciver, PendingIntent.FLAG_NO_CREATE);

                    alarmManager.cancel(pendingIntentEndServices);

                }

            }

            return true;
        }

        return false;
    }

    @Override
    public void SaveBudgetedIncomes(View rootView, String idBudget, ArrayList<IncomeModel> budgetedIncomes,String dateBudgeted) throws ParseException {


        DataBaseController db = new DataBaseController(rootView.getContext());
        SQLiteDatabase table_manager = db.getWritableDatabase();
        ContentValues contentIncomeValues = new ContentValues();

        for (IncomeModel incomeModel : budgetedIncomes) {

            contentIncomeValues.put(Income_Budget_Table.ID_BUDGET, idBudget);
            contentIncomeValues.put(Income_Budget_Table.DESCRIPTION, incomeModel.getDESCRIPTION().trim());
            contentIncomeValues.put(Income_Budget_Table.AMOUNT, incomeModel.getAMOUNT().trim());
            contentIncomeValues.put(Income_Budget_Table.DATE_INSERTED, dateBudgeted);

            table_manager.insert(Income_Budget_Table.TABLE_NAME, null, contentIncomeValues);
        }

        db.close();
    }

    @Override
    public void SaveBudgetedExpense(View rootView, String idBudget,ArrayList<ExpenseModel> budgtedExpenses,String dateBudgeted) throws ParseException {

        ContentValues contentExpensesValues = new ContentValues();
        DataBaseController db = new DataBaseController(rootView.getContext());
        SQLiteDatabase table_manager = db.getWritableDatabase();
        ManagerDate time = new ManagerDate();



        for (ExpenseModel expenseModel : budgtedExpenses){

            contentExpensesValues.put(Expense_Budget_Table.ID_BUDGET, idBudget);
            contentExpensesValues.put(Expense_Budget_Table.DESCRIPTION, expenseModel.getDESCRIPTION());
            contentExpensesValues.put(Expense_Budget_Table.AMOUNT, expenseModel.getAMOUNT());
            contentExpensesValues.put(Expense_Budget_Table.ID_CATEGORY, expenseModel.getID_CATEGORY());
            contentExpensesValues.put(Expense_Budget_Table.DATE_BDGETED,dateBudgeted);

            table_manager.insert(Expense_Budget_Table.TABLE_NAME,null,contentExpensesValues);
        }

        db.close();

    }

    public String getBudgetStatus(String startDateBudgetStr){

        String statusBudget = "";

        calendar.set(Calendar.DAY_OF_MONTH,calendar.getMinimum(Calendar.DAY_OF_MONTH));
        Date minDateOfMonthBadFormat = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date maxDateOfMonthBadFormat = calendar.getTime();

        String minDateRightFormat = simpleDateFormat.format(minDateOfMonthBadFormat);
        String maxDateRightFormat = simpleDateFormat.format(maxDateOfMonthBadFormat);

        try {

            Date startBudgetDate = simpleDateFormat.parse(startDateBudgetStr);
            Date minDateOfMonth =  simpleDateFormat.parse(minDateRightFormat);
            Date maxDateOfMonth = simpleDateFormat.parse(maxDateRightFormat);

            if((startBudgetDate.after(minDateOfMonth) || startBudgetDate.equals(minDateOfMonth)) && (startBudgetDate.before(maxDateOfMonth) ||startBudgetDate.equals(maxDateOfMonth)))
                statusBudget = "1";

            if(startBudgetDate.after(maxDateOfMonth))
                statusBudget = "2";

            if (startBudgetDate.before(minDateOfMonth))
                statusBudget = "0";

        }catch (ParseException e){
            e.printStackTrace();
        }
        return  statusBudget;
    }

    public Boolean setEndBudgetAlarm(String endDateBudgetStr){

        String currentDayStr = managerDate.getCurrentDateSqlLiteFormat();
        Boolean result = false;
        try {

            Date endBudgetDate = simpleDateFormat.parse(endDateBudgetStr);
            Date currentDay = simpleDateFormat.parse(currentDayStr);

            result = endBudgetDate.after(currentDay) || endBudgetDate.equals(currentDay);

        }catch (ParseException e){
            e.printStackTrace();
        }
        return result;
    }

    public Boolean setStartBudgetAlarm(String startDateBudgetStr){

        String currentDayStr = managerDate.getCurrentDateSqlLiteFormat();
        Boolean result = false;
        try {

            Date startBudgetDate = simpleDateFormat.parse(startDateBudgetStr);
            Date currentDay = simpleDateFormat.parse(currentDayStr);

            result = startBudgetDate.equals(currentDay);

        }catch (ParseException e){
            e.printStackTrace();
        }

        return result;
    }

    public void InitBudgetViews(View rootview){

        DescriptionBudgetText = (EditText) rootview.findViewById(R.id.DescriptionBudget);
        GoalEditText = (EditText) rootview.findViewById(R.id.BudgetGoal);
        StartDateEditText = (EditText) rootview.findViewById(R.id.Fecha_inicio);
        EndDateEditText = (EditText) rootview.findViewById(R.id.Fecha_fin);
        PredeterminateSwitch = (Switch) rootview.findViewById(R.id.PredeterminadoSwitch);

    }

    public void ExecuteSaveBudget(ArrayList<IncomeModel> BudgetedIncome, ArrayList<ExpenseModel> BudgetedExpense){

        String idBudget;
        String descBudget;
        String goalBudget;
        String startDate;
        String endDate;
        String switchValue;
        IdFactory idFactory = new  IdFactory(this.ROOTVIEW.getContext(),Budget_Table.TABLE_NAME);
        InitBudgetViews(this.ROOTVIEW);

        idBudget = idFactory.GetUUID();
        descBudget = DescriptionBudgetText.getText().toString().trim();
        goalBudget =  GoalEditText.getText().toString().trim();
        startDate = ManagerDate.ConvertToSqlLiteDateFormat(StartDateEditText.getText().toString());
        endDate = ManagerDate.ConvertToSqlLiteDateFormat(EndDateEditText.getText().toString());
        switchValue = String.valueOf(PredeterminateSwitch.isChecked());
        String budgetStatus = getBudgetStatus(startDate);
        long idStartBudgetAlarm = managerDate.getAlarmId();
        long idEndBudgetAlarm = managerDate.getAlarmId();

        HashMap<String, String> budget_information = new HashMap<String, String>();

        budget_information.put("idBudget",idBudget);
        budget_information.put("description",descBudget);
        budget_information.put("goalBudget",goalBudget);
        budget_information.put("startDate", startDate);
        budget_information.put("endDate", endDate);
        budget_information.put("budgetState", budgetStatus);
        budget_information.put("sincronizado",switchValue);
        budget_information.put("idStartBudgetAlarm", String.valueOf(idStartBudgetAlarm));
        budget_information.put("idEndBudgetAlarm", String.valueOf(idEndBudgetAlarm));

        AsyncTaskBudgetProcess task = new AsyncTaskBudgetProcess(this.ROOTVIEW, budget_information,BudgetedIncome,BudgetedExpense);
        task.execute();

    }

}
