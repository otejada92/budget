package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 10/17/2014.
 */
public class IncomeModel {

    private String ID_INCOME;
    private String DESCRIPTION;
    private String AMOUNT;
    private String RECURRENCE;
    private String DAY_ISSUE;

    public IncomeModel(String id_income, String description, String amount,String recurrence,String date_issue){
        super();
        this.ID_INCOME = id_income;
        this.DESCRIPTION = description;
        this.AMOUNT = amount;
        this.RECURRENCE = recurrence;
        this.DAY_ISSUE = date_issue;
    }

    public void setID_INCOME(String value){
        ID_INCOME = value;}
    public String getID_INCOME(){return ID_INCOME;}

    public void setDESCRIPTION(String value){ DESCRIPTION = value;}
    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setAMOUNT(String value){ AMOUNT = value;}
    public String getAMOUNT() {
        return AMOUNT;
    }


    public String getRECURRENCE() {
        return RECURRENCE;
    }

    public void setRECURRENCE(String value) {
        this.RECURRENCE = value;
    }

    public String getDAY_ISSUE() {
        return DAY_ISSUE;
    }

    public void setDAY_ISSUE(String DAY_ISSUE) {
        this.DAY_ISSUE = DAY_ISSUE;
    }
}
