package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskExpenseProcess;
import com.example.xps.budgetapp.ClickListiners.OnClickEditDate;
import com.example.xps.budgetapp.Controllers.BudgetViewController;
import com.example.xps.budgetapp.Controllers.CategoryDataController;
import com.example.xps.budgetapp.Controllers.ExpenseDataController;
import com.example.xps.budgetapp.CustomAdapters.CustomAdapterSpinnerCategory;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterAddExpenseToBudgetFragment;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.util.ArrayList;
import java.util.HashMap;

public class AddExpenseToBudgetFragment extends Fragment{

    private View ROOTVIEW;
    public static boolean saveState = true;
    private EditText descriptionEditText;
    private EditText amountEditText;
    private static Spinner categorySpinner;
    private EditText dateHappenedEditText;
    private static ListView expenseListView;
    private static String ID_BUDGET;
    ManagerDate managerDate = new ManagerDate();

    public AddExpenseToBudgetFragment(String id_presupuesto) {

        this.ID_BUDGET = id_presupuesto;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        String current_date = managerDate.GetCurrentDateUserFormat();
        final View rootView = inflater.inflate(R.layout.add_expense_to_budget_layout,container,false);

        this.ROOTVIEW = rootView;

        InitView(rootView);

        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionEditText.length() != 0 || !descriptionEditText.getText().toString().equals("")) {

                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountEditText.length() != 0 || !amountEditText.getText().toString().equals("")) {

                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dateHappenedEditText.setInputType(InputType.TYPE_NULL);
        dateHappenedEditText.setOnClickListener(new OnClickEditDate(rootView, getFragmentManager()));
        dateHappenedEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                saveState = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dateHappenedEditText.setText(current_date);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                saveState = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        PopulateExpenseListView(rootView);
        PopulateSpinnerCategory(rootView);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.add_expense_budget_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();
        String errorMessage = "";
        String errorDateHappened = "";

        Toast toast;
        ManagerDate managerDate = new ManagerDate();
        String[] budget_date =  getActivity().getIntent().getStringExtra("BUDGET_DATE").split(",");

        String start_dateBudgetStr = budget_date[0];
        String end_dateBudgetStr = budget_date[1];
        String IncomeDateHappened = dateHappenedEditText.getText().toString();
        String CategorySelectedId = String.valueOf(categorySpinner.getSelectedView().getId());
        String CategoryDesc = "";

        if(!CategorySelectedId.equals(String.valueOf(R.id.CategoryChoise))) {
            CategoryModel model = (CategoryModel) categorySpinner.getSelectedItem();
            CategoryDesc = model.getDESCRIPTION();
        }

        errorDateHappened = managerDate.ValidateDateHappened(start_dateBudgetStr, end_dateBudgetStr, IncomeDateHappened);

        if(id_item == R.id.add_button){

            String Description = descriptionEditText.getText().toString().trim();
            String Amount = amountEditText.getText().toString().trim();
            String SaveStateStr = "";

            if(Description.length() == 0 || Description.matches("\\s+") || Description == null){
                errorMessage += "La descripcion del gasto no puede estar vacia.\n";
                saveState = false;
            }

            if (Amount.length() == 0 || Amount == null) {
                errorMessage += "El monto del gasto no puede estar vacio.\n";
                saveState = false;

            }

            if (CategorySelectedId.equals(String.valueOf(R.id.CategoryChoise))) {
                errorMessage += "Seleccione  una categoria.\n";
                saveState = false;
            }

            if (!errorDateHappened.equals("")){
                errorMessage += "\n " + errorDateHappened;
                saveState = false;
            }

            SaveStateStr =  String.valueOf(saveState);

            if(saveState) {

                HashMap<String, String> expenseInformation = new HashMap<String, String>();

                expenseInformation.put("description", Description);
                expenseInformation.put("amount", Amount);
                expenseInformation.put("recurrence", "0");
                expenseInformation.put("idCategory", CategorySelectedId);
                expenseInformation.put("categoryDesc", CategoryDesc);
                expenseInformation.put("saveState", SaveStateStr);
                expenseInformation.put("DO", "ADDTOBUDGET");
                expenseInformation.put("idBudget", this.ID_BUDGET);
                expenseInformation.put("dateHappened", dateHappenedEditText.getText().toString());


                AsyncTaskExpenseProcess task = new AsyncTaskExpenseProcess(ROOTVIEW, expenseInformation,ROOTVIEW.getContext());
                task.execute();
            }
            else{
                toast =  Toast.makeText(getActivity(),errorMessage.trim(),Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(id_item == android.R.id.home){

            Intent intenHome = new Intent(ROOTVIEW.getContext(), BudgetViewController.class);
            startActivity(intenHome);
        }
        return true;
    }

    public static void PopulateSpinnerCategory(final View rootview){

        ArrayList<CategoryModel> CategoriesList = CategoryDataController.GetCategories(rootview.getContext());

        if (!CategoriesList.isEmpty()) {
            CustomAdapterSpinnerCategory categoryAdapter = new CustomAdapterSpinnerCategory(rootview.getContext(), R.layout.category_spinner_layout, R.id.CategoryChoise, CategoriesList);
            categorySpinner.setAdapter(categoryAdapter);
        }
        else{
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),R.layout.category_spinner_layout,new String[]{"No hay categoria"});
            categorySpinner.setAdapter(errorAdapter);
        }

    }

    public static void PopulateExpenseListView(View rootview){

        ArrayList<ExpenseModel> expenseList = ExpenseDataController.getExpenseInformation(rootview.getContext());
        String[] error = {"No hay gasto insertados."};

        if (!expenseList.isEmpty()) {
            ListViewAdapterAddExpenseToBudgetFragment AdapterExpense = new ListViewAdapterAddExpenseToBudgetFragment(
                    rootview.getContext(),
                    R.layout.add_income_listview_layout,
                    rootview,
                    expenseList,
                    ID_BUDGET);
            expenseListView.setAdapter(AdapterExpense);
        }
        else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
            expenseListView.setAdapter(errorAdapter);
        }
    }


    public void InitView(View rootview){

        descriptionEditText = (EditText) rootview.findViewById(R.id.descripction_expense);
        amountEditText = (EditText) rootview.findViewById(R.id.amount_expense);
        categorySpinner = (Spinner) rootview.findViewById(R.id.categories_spinner);
        dateHappenedEditText = (EditText) rootview.findViewById(R.id.DateHappened);
        expenseListView = (ListView) rootview.findViewById(R.id.expense_add_listview);

    }
}
