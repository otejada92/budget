package com.example.xps.budgetapp.CustomAdapters;

/**
 * Created by XPS on 5/6/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.xps.budgetapp.Models.CategoryModel;

import java.util.ArrayList;

public class CustomAdapterSpinnerCategory extends ArrayAdapter<CategoryModel> {


    private  ArrayList<CategoryModel> DATA;
    private  Context CONTEXT;
    private  int RESOURCE;
    private int ELEMENTID;


    public CustomAdapterSpinnerCategory(Context context, int resource, int elementid, ArrayList<CategoryModel> data) {
        super(context,resource,data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = data;
        this.ELEMENTID = elementid;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        super.getView(position, convertView, parent);

        ViewHolder Holder;

        if(convertView == null){
            LayoutInflater inflater=(LayoutInflater)CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView=inflater.inflate(this.RESOURCE,parent,false);
            Holder=new ViewHolder();
            Holder.textView= (TextView) convertView.findViewById(this.ELEMENTID);
            convertView.setTag(Holder);
        }else{
            Holder=(ViewHolder)convertView.getTag();
        }

        Holder.textView.setId(Integer.parseInt(DATA.get(position).getID_CATEGORY()));
        Holder.textView.setText(DATA.get(position).getDESCRIPTION());

        return convertView;
    }

    @Override
    public CategoryModel getItem(int position) {
        return DATA.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return DATA.size();
    }


    @Override
    public long getItemId(int position) {
        return Long.parseLong(DATA.get(position).getID_CATEGORY());
    }

    static class ViewHolder{

        TextView textView;
    }
}
