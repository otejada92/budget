package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.xps.budgetapp.AlarmServicesConfigurations.DailyFixedIncomeExpenseAlarm;
import com.example.xps.budgetapp.AlarmServicesConfigurations.MonthBudgetCheckerAlarm;
import com.example.xps.budgetapp.R;

public class PrincipalMenuViewController extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal_menu_layout);

        ImageView boton_ingreso = (ImageView) findViewById(R.id.boton_ingreso);
        ImageView boton_gasto = (ImageView) findViewById(R.id.boton_gasto);
        ImageView boton_presupuesto = (ImageView) findViewById(R.id.boton_presupuesto);
        ImageView boton_consulta = (ImageView) findViewById(R.id.boton_consultas);
        ImageView boton_categoria = (ImageView) findViewById(R.id.boton_categoria);


        boton_ingreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(getApplicationContext(),IncomeViewController.class);
                startActivity(intent);
            }
        });

        boton_gasto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent =  new Intent(getApplicationContext(),ExpenseViewController.class);
                startActivity(intent);
            }
        });


        boton_presupuesto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(),BudgetViewController.class);
                startActivity(intent);

               }
        });

        boton_consulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),ReportViewController.class);
                startActivity(intent);
            }
        });

        boton_categoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),CategoryViewController.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }


}
