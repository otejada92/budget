package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskIncomeProcess;
import com.example.xps.budgetapp.ClickListiners.SelectRecurrenceClick;
import com.example.xps.budgetapp.ClickListiners.ShowHelpDialog;
import com.example.xps.budgetapp.Controllers.PrincipalMenuViewController;
import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AddIncomeFragment extends Fragment {

    private boolean saveState;
    private EditText descriptionEditText;
    private EditText amountEditText;
    private Spinner recurrenceSpinner;
    private MultipleSpinnerSelection dayHappenedSpinner;
    private ImageButton helpButton;
    private View ROOTVIEW;


    public AddIncomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        final View rootView = inflater.inflate(R.layout.add_income_fragment_layout, container, false);

        this.ROOTVIEW = rootView;

        InitViews(rootView);
        populateRecurrenceSpinner(rootView);
        populateDaySpinner();

        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionEditText.length() != 0 || !descriptionEditText.getText().toString().equals("")) {
                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountEditText.length() != 0 || !amountEditText.getText().toString().equals("")) {
                    saveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        recurrenceSpinner.setOnItemSelectedListener(new SelectRecurrenceClick(rootView));

        helpButton.setOnClickListener(new ShowHelpDialog(getFragmentManager(), "dayHappenedHelp"));

        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.income_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();

        String description = descriptionEditText.getText().toString().trim();
        String amount = amountEditText.getText().toString().trim();
        String recurrenceSelected = String.valueOf(recurrenceSpinner.getSelectedItemPosition());
        String dayHappened = dayHappenedSpinner.getSelectedItemsAsString();
        saveState = true;
        String saveStateStr;
        String messageError = "";

        Toast toast;

        HashMap<String, String> incomeInformation = new HashMap<String, String>();

        if (id_item == R.id.add_button){

            if (description.length() == 0 || description.matches("\\s+")) {

                messageError += "La descripcion del ingreso no puede estar vacia.";
                saveState = false;
            }

            if (amount.length() == 0) {
                messageError += "\n El monto del ingreso no puede estar vacio.";
                saveState = false;
            }

            if (recurrenceSelected.equals("1")) {
                if (dayHappened.equals("")) {
                    messageError += "\n Si es un ingreso fijo el 'Dia de ocurrencia' no puede estar vacio.";
                    saveState = false;
                } else {
                    saveState = true;
                }
            }

            saveStateStr = String.valueOf(saveState);

            if(saveState){
                incomeInformation.put("description", description);
                incomeInformation.put("amount", amount);
                incomeInformation.put("recurrence", recurrenceSelected);
                incomeInformation.put("dayHappened", dayHappened);
                incomeInformation.put("saveState", saveStateStr);
                incomeInformation.put("DO", "SAVE");

                AsyncTaskIncomeProcess task = new AsyncTaskIncomeProcess(ROOTVIEW, incomeInformation);
                task.execute();
            }else{
                toast =  Toast.makeText(getActivity(), messageError.replaceAll("^\\s+", ""), Toast.LENGTH_LONG);
                toast.show();
            }
        }

        if(id_item  == android.R.id.home){

            Intent homeIntent = new Intent(getActivity().getBaseContext(), PrincipalMenuViewController.class);
            startActivity(homeIntent);
        }

        return true;
    }

    public void populateRecurrenceSpinner(View rootview){

        List<String> list = new ArrayList<String>();

        list.add("Variable");
        list.add("Fijo");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(rootview.getContext(),
                android.R.layout.simple_spinner_item,list);


        recurrenceSpinner.setAdapter(dataAdapter);

    }

    public void populateDaySpinner(){

        String[] list = getResources().getStringArray(R.array.day_of_month);
        dayHappenedSpinner.setItems(list);
    }

    public void InitViews(View rootview) {

        descriptionEditText = (EditText) rootview.findViewById(R.id.descripction_income);
        amountEditText = (EditText) rootview.findViewById(R.id.amount_income);
        recurrenceSpinner = (Spinner) rootview.findViewById(R.id.RecurrenceSpinner);
        dayHappenedSpinner = (MultipleSpinnerSelection) rootview.findViewById(R.id.DayHappened);
        helpButton = (ImageButton) rootview.findViewById(R.id.help_button);

    }



}





