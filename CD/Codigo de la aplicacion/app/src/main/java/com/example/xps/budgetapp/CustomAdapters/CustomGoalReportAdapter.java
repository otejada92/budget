package com.example.xps.budgetapp.CustomAdapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.xps.budgetapp.Models.GoalBudgetReportModel;
import com.example.xps.budgetapp.R;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XPS on 5/12/2015.
 */
public class CustomGoalReportAdapter extends ArrayAdapter<GoalBudgetReportModel> {

    private Context CONTEXT;
    private int RESOURCE;
    private int LINECHART;
    private int TABLELAYOUT;
    private int TEXTVIEW;
    float Totalincome = (float) 0.0;
    float Totalexpense = (float) 0.0;
    private ArrayList<GoalBudgetReportModel> DATA;


    public CustomGoalReportAdapter(Context context, int resource, int  linechart,int tablelayout,int textview,ArrayList<GoalBudgetReportModel> data) {
        super(context,resource,textview,data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.LINECHART = linechart;
        this.TABLELAYOUT = tablelayout;
        this.TEXTVIEW = textview;
        this.DATA = data;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        super.getView(position, convertView, parent);

        ViewHolder Holder;
        if(convertView == null){

            LayoutInflater inflater = (LayoutInflater) CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(this.RESOURCE,parent,false);
            Holder = new ViewHolder();

            Holder.barChart = (HorizontalBarChart) convertView.findViewById(this.LINECHART);
            Holder.tableLayout = (TableLayout) convertView.findViewById(this.TABLELAYOUT);
            Holder.textView = (TextView) convertView.findViewById(this.TEXTVIEW);
            convertView.setTag(Holder);
        }else{

            Holder = (ViewHolder) convertView.getTag();
        }


       PopulateBarChartView(Holder.barChart, Holder.textView, DATA.get(position));

       if(DATA.size()-1 == getPosition(DATA.get(position))) {
           Holder.tableLayout.setVisibility(View.VISIBLE);
           GetTotals(DATA);
           PopulateTableResultView(Holder.tableLayout, Totalincome, Totalexpense,DATA.get(position).getGOALBUDGET());
       }else{
           Holder.tableLayout.setVisibility(View.GONE);
       }
        return convertView;
    }

    @Override
    public int getPosition(GoalBudgetReportModel item) {
        return DATA.indexOf(item);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void PopulateBarChartView(HorizontalBarChart barChart, TextView textMonth, GoalBudgetReportModel monthInformation){


        BarDataSet barValues;
        Resources resources = CONTEXT.getResources();

        ArrayList<String> element_names = new ArrayList<String>();
        ArrayList<BarEntry> element_values = new ArrayList<BarEntry>();

        int index = 0;

        List<Integer> colorsArrays = new ArrayList<Integer>();

        textMonth.setText("Mes de "+monthInformation.getMONTH().toLowerCase());


        if(monthInformation.getTOTALINCOME() != null) {
            element_names.add("Ingresos");
            element_values.add(new BarEntry(Float.valueOf(monthInformation.getTOTALINCOME()), 0));
            colorsArrays.add(resources.getColor(R.color.blue_1));
        }

        if(monthInformation.getTOTALEXPENSE() != null) {
            element_names.add("Gastos");
            if(element_values.isEmpty()) {
                element_values.add(new BarEntry(Float.valueOf(monthInformation.getTOTALEXPENSE()), 0));
                colorsArrays.add(resources.getColor(R.color.red_1));
            }else{
                element_values.add(new BarEntry(Float.valueOf(monthInformation.getTOTALEXPENSE()), 1));
                colorsArrays.add(resources.getColor(R.color.red_1));
            }
        }


        if(monthInformation.getGOALBUDGET_PROCESS() > 0){

            if(monthInformation.getTOTALINCOME() != null) {
                index+= 1;
            }

            if(monthInformation.getTOTALEXPENSE() != null) {
                index+=1;
            }

            element_names.add("Ahorro");
            element_values.add(new BarEntry(monthInformation.getGOALBUDGET_PROCESS(), index));
            colorsArrays.add(resources.getColor(R.color.green_2));
        }


        barValues = new BarDataSet(element_values,"");

        barValues.setColors(colorsArrays);
        barValues.setValueTextSize(8f);

        barChart.getLegend().setCustom(colorsArrays,element_names);
        barChart.setDescription("");


        barChart.setData(new BarData(element_names, barValues));
        barChart.getXAxis().setEnabled(false);

        barChart.animateXY(3000, 3000);

        YAxis leftAxis = barChart.getAxisLeft();

        LimitLine limitLine = new LimitLine(Float.valueOf(monthInformation.getGOALBUDGET()));
        limitLine.setLabel("Limite de la Meta");
        limitLine.setLineColor(Color.BLACK);
        limitLine.setTextColor(Color.BLACK);
        limitLine.setLabelPosition(LimitLine.LimitLabelPosition.POS_LEFT);
        limitLine.setLineWidth(4f);
        limitLine.setTextSize(12f);

        leftAxis.addLimitLine(limitLine);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void PopulateTableResultView(TableLayout tableLayout,float totalIncome,float totalExpense,String goalAmount){

        TableRow incomeRow = new TableRow(CONTEXT);
        TableRow expenseRow = new TableRow(CONTEXT);
        TableRow savedamountRow = new TableRow(CONTEXT);

        SetProperty(incomeRow);
        SetProperty(expenseRow);
        SetProperty(savedamountRow);

        TextView incomeTotalLabel = new TextView(CONTEXT);
        TextView incomeTotalValue = new TextView(CONTEXT);

        TextView expenseTotalLabel = new TextView(CONTEXT);
        TextView expenseTotalValue = new TextView(CONTEXT);

        TextView budgetGoalLabel = new TextView(CONTEXT);
        TextView budgetGoalValue = new TextView(CONTEXT);

        TextView diffTotalLabel = new TextView(CONTEXT);
        TextView diffTotalValue = new TextView(CONTEXT);


        incomeTotalLabel.setText("Total de Ingresos");
        incomeTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        incomeTotalLabel.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        incomeTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        incomeTotalLabel.setGravity(Gravity.CENTER);


        incomeTotalValue.setText(String.valueOf(totalIncome));
        incomeTotalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        incomeTotalValue.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        incomeTotalValue.setGravity(Gravity.CENTER);

        expenseTotalLabel.setText("Total de Gastos");
        expenseTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        expenseTotalLabel.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        expenseTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        expenseTotalLabel.setGravity(Gravity.CENTER);

        expenseTotalValue.setText(String.valueOf(totalExpense));
        expenseTotalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        expenseTotalValue.setGravity(Gravity.CENTER);
        expenseTotalValue.setTextColor(CONTEXT.getResources().getColor(R.color.black));


        budgetGoalLabel.setText("Meta propuesta");
        budgetGoalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        budgetGoalLabel.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        budgetGoalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
        budgetGoalLabel.setGravity(Gravity.CENTER);

        budgetGoalValue.setText(goalAmount);
        budgetGoalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        budgetGoalValue.setGravity(Gravity.CENTER);
        budgetGoalValue.setTextColor(CONTEXT.getResources().getColor(R.color.black));


        float saved_amount = totalIncome - totalExpense;
        float goal_budget = Float.valueOf(goalAmount);

        if(saved_amount > goal_budget || saved_amount > 0) {

            diffTotalLabel.setText("Monto ahorrado");
            diffTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
            diffTotalLabel.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            diffTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
            diffTotalLabel.setGravity(Gravity.CENTER);

            diffTotalValue.setText(String.valueOf(saved_amount));
            diffTotalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            diffTotalValue.setTextColor(CONTEXT.getResources().getColor(R.color.green_2));
            diffTotalValue.setGravity(Gravity.CENTER);

        }else{

            diffTotalLabel.setText("Deuda");
            diffTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
            diffTotalLabel.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            diffTotalLabel.setTextColor(CONTEXT.getResources().getColor(R.color.black));
            diffTotalLabel.setGravity(Gravity.CENTER);


            diffTotalValue.setText(String.valueOf(saved_amount));
            diffTotalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            diffTotalValue.setGravity(Gravity.CENTER);
            diffTotalValue.setTextColor(CONTEXT.getResources().getColor(R.color.red_1));
        }

        incomeRow.addView(incomeTotalLabel);
        incomeRow.addView(incomeTotalValue);

        expenseRow.addView(expenseTotalLabel);
        expenseRow.addView(expenseTotalValue);

        savedamountRow.addView(diffTotalLabel);
        savedamountRow.addView(diffTotalValue);

        tableLayout.addView(incomeRow);
        tableLayout.addView(expenseRow);
        tableLayout.addView(savedamountRow);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void SetProperty(TableRow tableRow){

        int sdk = android.os.Build.VERSION.SDK_INT;

        tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            tableRow.setBackgroundDrawable(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
        } else {
            tableRow.setBackground(CONTEXT.getResources().getDrawable(R.drawable.table_cell_style));
        }

    }

    public  void GetTotals(ArrayList<GoalBudgetReportModel> budgetInformation){

        for (GoalBudgetReportModel information: budgetInformation){

            if(information.getTOTALINCOME()!=null)
                Totalincome += Float.valueOf(information.getTOTALINCOME());

            if(information.getTOTALEXPENSE() !=null)
                Totalexpense += Float.valueOf(information.getTOTALEXPENSE());
        }
    }

    class  ViewHolder{

        HorizontalBarChart barChart;
        TableLayout tableLayout;
        TextView textView;
    }
}
