package com.example.xps.budgetapp.ClickListiners;

import android.app.FragmentManager;
import android.view.View;
import android.widget.EditText;

import com.example.xps.budgetapp.CustomView.DatePicker;

/**
 * Created by XPS on 6/29/2015.
 */
public class OnClickEditDate implements View.OnClickListener {

    public View rootView;
    public FragmentManager fragmentManager;


    public OnClickEditDate(View rootview,FragmentManager fragmentmanager) {
        this.rootView = rootview;
        this.fragmentManager = fragmentmanager;
    }

    @Override
    public void onClick(View view) {
        DatePicker datePicker = new DatePicker(rootView,(EditText)view);

        datePicker.show(this.fragmentManager,String.valueOf(view.getId()));

    }
}
