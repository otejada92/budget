package com.example.xps.budgetapp.CustomAdapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.xps.budgetapp.Dialogs.ExpenseLineaDetailDialog;
import com.example.xps.budgetapp.Dialogs.IncomeLineaDetailDialog;
import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.Models.LineExpenseModel;
import com.example.xps.budgetapp.Models.LineIncomeModel;
import com.example.xps.budgetapp.Models.TwoLineChartModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Highlight;

import java.util.ArrayList;

/**
 * Created by XPS on 7/19/2015.
 */
public class LineChartListViewAdapter extends ArrayAdapter<TwoLineChartModel> {

    private Context context;
    private int resource;
    private ArrayList<TwoLineChartModel> lineInformation;
    IncomeLineaDetailDialog incomeDetail;
    ExpenseLineaDetailDialog expenseDetail;
    FragmentManager fragmentManager;


    public LineChartListViewAdapter(Context context, int resource,ArrayList<TwoLineChartModel> arrayList, FragmentManager fragmentManager) {
        super(context, resource, arrayList);

        this.context = context;
        this.resource = resource;
        this.lineInformation = arrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){

        LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView =  inflater.inflate(this.resource,parent,false);


        viewHolder = new ViewHolder();

        viewHolder.incomeChart = (LineChart) convertView.findViewById(R.id.incomeLineChart);
        viewHolder.expenseChart = (LineChart) convertView.findViewById(R.id.expenseLineChart);
        viewHolder.relativeLayout = (RelativeLayout) convertView.findViewById(R.id.chartContainer);

        convertView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(this.lineInformation.get(position).getExpenseList().isEmpty()){

            viewHolder.relativeLayout.removeView(viewHolder.expenseChart);
            LineData incomeData = getLineDataIncome(this.lineInformation.get(position).getIncomeList(),viewHolder.incomeChart);

            viewHolder.incomeChart.setData(incomeData);
            setLineChartProperties(viewHolder.incomeChart);
            viewHolder.incomeChart.getLegend().setCustom(new int[]{Color.rgb(8, 0, 255)}, new String[]{"Ingreso"});
            viewHolder.incomeChart.invalidate();
         }

        else if(this.lineInformation.get(position).getIncomeList().isEmpty()){

            viewHolder.relativeLayout.removeView(viewHolder.incomeChart);
            LineData expenseData = getLineDataExpense(this.lineInformation.get(position).getExpenseList(),viewHolder.expenseChart);
            viewHolder.expenseChart.setData(expenseData);
            setLineChartProperties(viewHolder.expenseChart);
            viewHolder.expenseChart.getLegend().setCustom(new int[]{Color.rgb(250, 11, 11)}, new String[]{"Gasto"});
            viewHolder.expenseChart.invalidate();
         }

        else if(!this.lineInformation.get(position).getIncomeList().isEmpty() && !this.lineInformation.get(position).getExpenseList().isEmpty()){

            LineData incomeData = getLineDataIncome(this.lineInformation.get(position).getIncomeList(),viewHolder.incomeChart);
            viewHolder.incomeChart.setData(incomeData);
            viewHolder.incomeChart.getLegend().setCustom(new int[]{Color.rgb(8, 0, 255)}, new String[]{"Ingreso"});
            setLineChartProperties(viewHolder.incomeChart);

            LineData expenseData = getLineDataExpense(this.lineInformation.get(position).getExpenseList(),viewHolder.expenseChart);
            viewHolder.expenseChart.setData(expenseData);
            setLineChartProperties(viewHolder.expenseChart);
            viewHolder.expenseChart.getLegend().setCustom(new int[]{Color.rgb(250, 11, 11)}, new String[]{"Gasto"});
            viewHolder.incomeChart.invalidate();

            viewHolder.expenseChart.invalidate();
         }

        return  convertView;
    }

    public LineData getLineDataIncome(final ArrayList<LineIncomeModel> incomeList,LineChart lineChart){

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        int index = 0;

        for (LineIncomeModel income : incomeList){
            xVals.add(ManagerDate.ConvertToUserDateFormat(income.getBUDGETED_HAPPENED()));
            yVals.add(new Entry(Float.valueOf(income.getINCOMESUM()), index));
            lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    incomeDetail = new IncomeLineaDetailDialog(incomeList.get(e.getXIndex()).getINCOMELIST());
                    incomeDetail.show(fragmentManager, "IncomeDetail");

                }

                @Override
                public void onNothingSelected() {

                }
            });

            index++;
        }

        LineDataSet lineDataSet = new LineDataSet(yVals,"");

        lineDataSet.setDrawCubic(true);
        lineDataSet.setDrawCircleHole(true);
        lineDataSet.setDrawValues(false);
        lineDataSet.setCubicIntensity(0.1f);
        lineDataSet.setLineWidth(5f);
        lineDataSet.setCircleSize(5f);
        lineDataSet.setFillAlpha(65);
        lineDataSet.setColor(Color.rgb(8, 0, 255));
        lineDataSet.setValueTextColor(Color.rgb(0,0,0));
        lineDataSet.setValueTextSize(10f);

        return new LineData(xVals,lineDataSet);

    }

    public LineData getLineDataExpense(final ArrayList<LineExpenseModel> expenseList, LineChart lineChart){



        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        int index = 0;

        for (LineExpenseModel expense : expenseList){
            xVals.add(ManagerDate.ConvertToUserDateFormat(expense.getBUDGETED_HAPPENED()));
            yVals.add(new Entry(Float.valueOf(expense.getEXPENSESUM()), index));
            lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    expenseDetail = new ExpenseLineaDetailDialog(expenseList.get(e.getXIndex()).getEXPENSELIST());
                    expenseDetail.show(fragmentManager, "ExpenseDetail");
                }

                @Override
                public void onNothingSelected() {

                }
            });
            index++;
        }

        LineDataSet lineDataSet = new LineDataSet(yVals,"");

        lineDataSet.setDrawCubic(true);
        lineDataSet.setDrawCircleHole(true);
        lineDataSet.setDrawValues(false);
        lineDataSet.setCubicIntensity(0.1f);
        lineDataSet.setLineWidth(5f);
        lineDataSet.setCircleSize(5f);
        lineDataSet.setFillAlpha(65);
        lineDataSet.setColor(Color.rgb(250, 11, 11));
        lineDataSet.setValueTextColor(Color.rgb(0,0,0));
        lineDataSet.setValueTextSize(10f);

        return new LineData(xVals,lineDataSet);

    }

    public  void setLineChartProperties(LineChart lineChart){

        lineChart.setDescription("");
        lineChart.setNoDataText("No hay data por el momento");
        lineChart.setHighlightEnabled(true);

        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);

        lineChart.setPinchZoom(true);
        lineChart.animateX(3000, Easing.EasingOption.EaseInOutQuart);

    }

        public class ViewHolder {

            LineChart incomeChart;
            LineChart expenseChart;
            RelativeLayout relativeLayout;
        }

    }

