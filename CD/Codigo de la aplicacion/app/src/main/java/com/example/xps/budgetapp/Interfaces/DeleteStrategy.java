package com.example.xps.budgetapp.Interfaces;

import android.content.Context;

/**
 * Created by XPS on 6/24/2015.
 */
public interface DeleteStrategy {


    void DeleteResource(Context context,String tableName,String whereClause,String[] whereArgs);

}
