package com.example.xps.budgetapp.CustomAdapters;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.xps.budgetapp.Controllers.BudgetGoalReportView;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Fragments.ModifyBudgetFragment;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

/**
 * Created by XPS on 5/14/2015.
 */
public class CustomGoalReportSelectionBudgetAdapter extends ArrayAdapter<GenericBudgetModel> {


    private Context CONTEXT;
    private ArrayList<GenericBudgetModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;

    public CustomGoalReportSelectionBudgetAdapter(Context context, int resource, View rootview, ArrayList<GenericBudgetModel> Data) {
        super(context, resource, Data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder Holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(this.RESOURCE,parent,false);
            Holder = new ViewHolder();

            Holder.descView =(TextView)convertView.findViewById(R.id.descripcion_budget);
            Holder.goalView =(TextView)convertView.findViewById(R.id.goal_budget);
            Holder.durationView =(TextView)convertView.findViewById(R.id.range_budget);
            Holder.estadoView = (TextView) convertView.findViewById(R.id.state_budget);
            Holder.consultaButton =(ImageButton) convertView.findViewById(R.id.query_button);

            convertView.setTag(Holder);
        }
        else{
            Holder=(ViewHolder)convertView.getTag();
        }
        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getBUDGET_DESCRIPTION();
        String amountString = "<b>Meta:$ </b>"+ this.DATA.get(position).getBUDGET_GOAL();


        String duration = "<b>Duracion: </b>"+ ManagerDate.ConvertToUserDateFormat(this.DATA.get(position).getSTART_DATE())+"" +
                " - "+ManagerDate.ConvertToUserDateFormat(this.DATA.get(position).getFINISH_DATE());

        String estado = "<b>Estado: </b>"+ getBudgetStatus(this.DATA.get(position).getBUDGET_STATUS());


        Holder.descView.setText(Html.fromHtml(descString));
        Holder.goalView.setText(Html.fromHtml(amountString));
        Holder.durationView.setText(Html.fromHtml(duration));
        Holder.estadoView.setText(Html.fromHtml(estado));

        Holder.consultaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ROOTVIEW.getContext(), BudgetGoalReportView.class);
                intent.putExtra("ID_BUDGET", DATA.get(position).getID_BUDGET());
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        ModifyBudgetFragment.PopulateBudgetListview(ROOTVIEW);
    }

    public String getBudgetStatus(String status){

        if (status.equals("0"))
            return "Finalizado";

        if (status.equals("1"))
            return "En Proceso";

        if (status.equals("2"))
            return "Programado";

        return  "Nunca pasara";
    }


    static class ViewHolder {

        TextView descView;
        TextView goalView;
        TextView durationView;
        TextView estadoView;
        ImageButton consultaButton;

    }

}
