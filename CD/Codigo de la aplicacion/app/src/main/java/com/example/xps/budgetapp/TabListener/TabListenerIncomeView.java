package com.example.xps.budgetapp.TabListener;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

import com.example.xps.budgetapp.R;

/**
 * Created by XPS on 1/5/2015.
 */
public class TabListenerIncomeView implements ActionBar.TabListener {

    private Fragment FRAGMENT;

    public TabListenerIncomeView(Fragment fragment) {
        this.FRAGMENT = fragment;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        fragmentTransaction.replace(R.id.ingreso_layout, FRAGMENT);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.remove(FRAGMENT);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
