package com.example.xps.budgetapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.xps.budgetapp.ClickListiners.OnClickEditDate;
import com.example.xps.budgetapp.Controllers.LineChartController;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osvaldo on 7/9/2015.
 */
public class LineChartDialog extends DialogFragment {

    public Spinner whatToRetrieve;
    public EditText fromDate;
    public EditText toDate;
    public Intent intent;
    public View dialogRootView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        dialogRootView = inflater.inflate(R.layout.line_filter_dialog, null);

        whatToRetrieve = (Spinner) dialogRootView.findViewById(R.id.noName);
        PopulateRecurrenceSpinner(dialogRootView);

        fromDate = (EditText) dialogRootView.findViewById(R.id.FromDate);
        fromDate.setOnClickListener(new OnClickEditDate(dialogRootView,getFragmentManager()));

        toDate = (EditText) dialogRootView.findViewById(R.id.ToDate);
        toDate.setOnClickListener(new OnClickEditDate(dialogRootView,getFragmentManager()));

        return new AlertDialog.Builder(getActivity())
                .setView(dialogRootView)
                .setTitle("Filtros")
                .setPositiveButton("Filtrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String fromDateStr = fromDate.getText().toString();
                        String toDateStr = toDate.getText().toString();

                        if (whatToRetrieve.getSelectedItem().toString().equals("Ingresos")) {

                            intent = new Intent(getActivity(), LineChartController.class);
                            intent.putExtra("fromDate",fromDateStr);
                            intent.putExtra("toDate",toDateStr);
                            intent.putExtra("Show","ingresos");

                            startActivity(intent);
                        }

                        if (whatToRetrieve.getSelectedItem().toString().equals("Gastos")) {

                            intent = new Intent(getActivity(), LineChartController.class);
                            intent.putExtra("fromDate",fromDateStr);
                            intent.putExtra("toDate",toDateStr);
                            intent.putExtra("Show","gastos");

                            startActivity(intent);

                        }

                        if (whatToRetrieve.getSelectedItem().toString().equals("Ingreso y Gasto")) {

                            intent = new Intent(getActivity(), LineChartController.class);
                            intent.putExtra("fromDate",fromDateStr);
                            intent.putExtra("toDate",toDateStr);
                            intent.putExtra("Show","Ingreso y Gasto");

                            startActivity(intent);

                        }


                    }
                })
                .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

    public void PopulateRecurrenceSpinner(View dialogRootView){

        List<String> list = new ArrayList<String>();

        list.add("Ingresos");
        list.add("Gastos");
        list.add("Ingreso y Gasto");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(dialogRootView.getContext(),
                android.R.layout.simple_spinner_item,list);

        whatToRetrieve.setAdapter(dataAdapter);
    }
}
