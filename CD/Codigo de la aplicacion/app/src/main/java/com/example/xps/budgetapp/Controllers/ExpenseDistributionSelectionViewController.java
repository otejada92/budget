package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.xps.budgetapp.CustomAdapters.CustomAdapterListviewExpenseDistribution;
import com.example.xps.budgetapp.Dialogs.DistributionReportFilterDialog;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class ExpenseDistributionSelectionViewController extends Activity{

    public static ListView budgetListView;
    private View parentRootView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presupuesto_consulta_view_controller);

        budgetListView = (ListView) findViewById(R.id.presupuesto_listview);
        LayoutInflater inflater =
                (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        parentRootView = inflater.inflate(R.layout.activity_presupuesto_consulta_view_controller,null);

        ArrayAdapter adapter = PopulateBudgetListview(parentRootView);
        budgetListView.setAdapter(adapter);

    }

    public ArrayAdapter PopulateBudgetListview(View rootview) {

        ArrayList<GenericBudgetModel> presupuestos_list = BudgetDataController.getBudgetsInformation(rootview.getContext());
        String[] error = {"No hay presupuestos."};

        if (!presupuestos_list.isEmpty()) {
            return new CustomAdapterListviewExpenseDistribution(
                    rootview.getContext(), R.layout.budget_listview_report_layout,
                    rootview,
                    presupuestos_list);
        }
            return new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.general_report_menu_option, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.filter) {
            DistributionReportFilterDialog dialogBudgetFilter = new DistributionReportFilterDialog(parentRootView);
            dialogBudgetFilter.show(getFragmentManager(), "Filter");
        }
        if(id == R.id.clear_filter) {
            ArrayAdapter adapter = PopulateBudgetListview(parentRootView);
            budgetListView.setAdapter(adapter);
        }

        return super.onOptionsItemSelected(item);

    }

}
