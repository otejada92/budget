package com.example.xps.budgetapp.Fragments;

/**
 * Created by XPS on 11/26/2014.
 */

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterModifyBudget;
import com.example.xps.budgetapp.Dialogs.BudgetFilterDialog;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;


public class ModifyBudgetFragment extends Fragment {


    public ModifyBudgetFragment(){}

    private static ListView budgetListView;
    private View parentRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        parentRootView= inflater.inflate(R.layout.modify_budget_fragment_layout,container,false);

        InitViews(parentRootView);
        PopulateBudgetListview(parentRootView);

        return parentRootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.modify_budget_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int itemId = item.getItemId();

            if(itemId == R.id.filter) {
                BudgetFilterDialog budgetFilterDialog = new BudgetFilterDialog(parentRootView);
                budgetFilterDialog.show(getFragmentManager(), "Filter");
            }

            if ( itemId == R.id.clear_filter) {
                PopulateBudgetListview(parentRootView);
            }
        return super.onOptionsItemSelected(item);

    }

    public static void PopulateBudgetListview(View rootview){

        ArrayList<GenericBudgetModel> Budget_list = BudgetDataController.getBudgetsInformation(rootview.getContext());
        String[] error = {"No hay presupuestos generados."};

        if (!Budget_list.isEmpty()) {

            ListViewAdapterModifyBudget presupuestoAdapter = new ListViewAdapterModifyBudget(
                    rootview.getContext(), R.layout.modify_budget_listview_layout,
                    rootview,
                    Budget_list);
            budgetListView.setAdapter(presupuestoAdapter);
        }
        else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
            budgetListView.setAdapter(errorAdapter);
        }
    }

    public void InitViews(View rootview){
        budgetListView = (ListView) rootview.findViewById(R.id.presupuesto_listview);
    }
}
