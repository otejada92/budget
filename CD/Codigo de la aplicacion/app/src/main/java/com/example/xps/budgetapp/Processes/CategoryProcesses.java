package com.example.xps.budgetapp.Processes;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Interfaces.CategoryStrategy;
import com.example.xps.budgetapp.Tables.Category_Table;

/**
 * Created by XPS on 3/15/2015.
 */
public class CategoryProcesses implements CategoryStrategy {


    @Override
    public void Save(Context context, String Description) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ContentValues Category_Information = new ContentValues();

        Category_Information.put(Category_Table.DESCRIPTION,Description);
        table_manager.insert(Category_Table.TABLE_NAME, null, Category_Information);

        db.close();
    }

    @Override
    public void Modify(Context context, String id_category,String Description) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();


        ContentValues data = new ContentValues();
        data.put(Category_Table.DESCRIPTION, Description);

        String wherecl = Category_Table.ID_CATEGORY + " = ?";
        String[] wherearg = {id_category};

        table_manager.update(Category_Table.TABLE_NAME, data, wherecl, wherearg);

    }

}
