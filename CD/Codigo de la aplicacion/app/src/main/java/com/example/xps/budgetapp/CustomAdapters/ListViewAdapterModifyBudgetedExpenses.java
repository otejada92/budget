package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.Fragments.ModifyBudgetedExpensesFragment;
import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.Processes.DeleteProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 11/29/2014.
 */
public class ListViewAdapterModifyBudgetedExpenses extends ArrayAdapter<BudgetedExpenseModel> {

    private Context CONTEXT;
    private ArrayList<BudgetedExpenseModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;
    public EditText Description_EditText;
    public EditText Amount_EditText;
    public Spinner Category_Spinner;
    private EditText DateHappened_EditText;
    int editButtonIndex = 0;


    public ListViewAdapterModifyBudgetedExpenses(Context context, int resource, View rootview, ArrayList<BudgetedExpenseModel> Data) {
        super(context, resource,Data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.DATA = Data;
        this.ROOTVIEW = rootview;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder Holder;
        final Cleaner clean_helper = new Cleaner();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(this.RESOURCE, parent, false);
            Holder = new ViewHolder();


            Holder.textViewDesc = (TextView) convertView.findViewById(R.id.descripcion);
            Holder.textViewAmount = (TextView) convertView.findViewById(R.id.amount);
            Holder.update_button = (ImageButton) convertView.findViewById(R.id.Editar);
            Holder.delete_button = (ImageButton) convertView.findViewById(R.id.Borrar);

            convertView.setTag(Holder);

        } else {
            Holder = (ViewHolder) convertView.getTag();
        }

        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getDESCRIPTION();
        String amountString = "<b>Monto:$ </b>"+ this.DATA.get(position).getAMOUNT();

        Holder.textViewDesc.setId(Integer.parseInt(this.DATA.get(position).getID_EXPENSE()));

        Holder.textViewDesc.setText(Html.fromHtml(descString));

        Holder.textViewAmount.setText(Html.fromHtml(amountString));

        Holder.update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                initViews();

                ModifyBudgetedExpensesFragment.id_expense = DATA.get(position).getID_EXPENSE();
                Description_EditText.setEnabled(true);
                Amount_EditText.setEnabled(true);
                DateHappened_EditText.setEnabled(true);
                Category_Spinner.setEnabled(true);
                ModifyBudgetedExpensesFragment.menuActionbar.getItem(editButtonIndex).setEnabled(true);

                Description_EditText.setText(DATA.get(position).getDESCRIPTION());
                Amount_EditText.setText(DATA.get(position).getAMOUNT());
                DateHappened_EditText.setText(ManagerDate.ConvertToUserDateFormat(DATA.get(position).getBUDGETED_DATE()));

                for (int index = 0; index <= Category_Spinner.getAdapter().getCount(); index++) {
                    try{
                        CategoryModel category = (CategoryModel) Category_Spinner.getItemAtPosition(index);
                        if (category.getID_CATEGORY().equals(DATA.get(position).getID_CATEGORY())) {
                            Category_Spinner.setSelection(index);
                            break;
                        }
                    }catch (IndexOutOfBoundsException e){
                        break;
                    }
                }
            }
        });

        Holder.delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final String whereClause = Expense_Budget_Table.ID_EXPENSE + " = ?" + " AND " + Expense_Budget_Table.ID_BUDGET + " = ? ";
                final String[] whereArgs = new String[] { DATA.get(position).getID_EXPENSE(),DATA.get(position).getID_BUDGET() };

                LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);

                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                textView.setText("Estas seguro que deseas borrar este gasto presupuestado?, este sera borrado y desvinculado para siempre del presupuesto correspondiente!");

                AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
                builder.setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyDeleteResource contextDelete = new ContextStrategyDeleteResource(new DeleteProcesses());
                                        contextDelete.executeStrategy(rootView.getContext(),Expense_Budget_Table.TABLE_NAME,whereClause,whereArgs);

                                        initViews();
                                        clean_helper.cleanView((RelativeLayout) ROOTVIEW);

                                        notifyDataSetChanged();

                                        Description_EditText.setEnabled(false);
                                        Amount_EditText.setEnabled(false);
                                        DateHappened_EditText.setEnabled(false);
                                        Category_Spinner.setEnabled(false);


                                    }
                                }
                        )
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();

            }
        });
        return convertView;
    }

    static class ViewHolder {

        TextView textViewDesc;
        TextView textViewAmount;
        ImageButton update_button;
        ImageButton delete_button;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        ModifyBudgetedExpensesFragment.PopulateExpensesListView(ROOTVIEW);
    }

    public void initViews() {


        Description_EditText = (EditText) ROOTVIEW.findViewById(R.id.descripction_expense);
        Amount_EditText = (EditText) ROOTVIEW.findViewById(R.id.amount_expense);
        DateHappened_EditText =(EditText) ROOTVIEW.findViewById(R.id.DateHappened);
        Category_Spinner = (Spinner) ROOTVIEW.findViewById(R.id.categories_spinner);

    }


}