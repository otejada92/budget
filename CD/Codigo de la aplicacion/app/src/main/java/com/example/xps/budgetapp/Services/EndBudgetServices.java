package com.example.xps.budgetapp.Services;

/**
 * Created by XPS on 12/4/2014.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.example.xps.budgetapp.Broadcast.DimissEndBudget;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Budget_Table;

public class EndBudgetServices extends Service {

    ManagerDate managerDate = new ManagerDate();
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        DataBaseController db = new DataBaseController(this);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        int requestId = managerDate.getAlarmId();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        String idBudget = "";
        String idAlarm = "";

        BudgetDataController budgetDataController = new BudgetDataController();

        try {
            idBudget = intent.getStringExtra("idBudget");
            idAlarm = intent.getStringExtra("idAlarm");
        }catch(NullPointerException ignored){

        }

        if(!idBudget.equals("")) {

            String description = budgetDataController.getDescBudget(this, idBudget);
            table_manager.execSQL(" UPDATE " + Budget_Table.TABLE_NAME +
                    " SET " + Budget_Table.BUDGET_STATUS + " = 0" +
                    " WHERE " + Budget_Table.ID_BUDGET + " = ?", new String[]{idBudget});

            Intent dismissIntent = new Intent(this, DimissEndBudget.class);
            dismissIntent.putExtra("notificationId", startId);
            dismissIntent.putExtra("idAlarm",idAlarm);

            PendingIntent piDismiss = PendingIntent.getBroadcast(this, requestId, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder
                    .setContentIntent(piDismiss)
                    .setSmallIcon(R.drawable.status_dialog_warning_icon)
                    .setContentTitle("Budget Helper")
                    .setContentText("Se ha  finalizado el presupuesto: " + description);
            mBuilder.setDefaults(Notification.DEFAULT_SOUND);
            notificationManager.notify(startId, mBuilder.build());
        }

        return START_STICKY;
    }


}
