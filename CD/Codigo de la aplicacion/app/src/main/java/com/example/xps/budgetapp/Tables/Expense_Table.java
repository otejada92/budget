package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;

/**
 * Created by XPS on 10/5/2014.
 */
public class Expense_Table implements BaseColumns{

    public static final String TABLE_NAME = "GASTOS_TABLE";
    public static final String ID_EXPENSE = "id_gasto";
    public static final String DESCRIPCTION = "descripcion";
    public static final String AMOUNT = "monto";
    public static final String ID_CATEGORY = "id_categoria";
    public static final String RECURRENCE = "recurrencia";
    public static final String DAY_ISSUE = "dias_ocurrencia";
}
