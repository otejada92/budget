package com.example.xps.budgetapp.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

/**
 * Created by XPS on 8/12/2015.
 */
public class ExpenseLineaDetailDialog extends DialogFragment {

    ArrayList<BudgetedExpenseModel> expenseDetail;
    View dialogRootView;
    TextView expenseInformation;
    TextView totalExpense;
    float totalExpenseValue = (float) 0.0;

    public ExpenseLineaDetailDialog(ArrayList<BudgetedExpenseModel> expenseDetail) {
        this.expenseDetail = expenseDetail;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        dialogRootView = getActivity().getLayoutInflater().inflate(R.layout.detail_information_layout, null);
        expenseInformation = (TextView) dialogRootView.findViewById(R.id.ViewInformation);
        totalExpense = (TextView) dialogRootView.findViewById(R.id.Total);


        String expenseDetail = "";

        for (BudgetedExpenseModel expense : this.expenseDetail) {
            expenseDetail += expense.getDESCRIPTION() + " - " + "$" + expense.getAMOUNT() + "\n";
            totalExpenseValue +=Integer.valueOf(expense.getAMOUNT());
        }

        expenseInformation.setText(expenseDetail);
        totalExpense.setText("Total de gastos: $" + String.valueOf(totalExpenseValue));

        return new AlertDialog.Builder(getActivity())
                .setView(dialogRootView)
                .setTitle("Detalle de gastos")
                .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();

    }
}