package com.example.xps.budgetapp.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.xps.budgetapp.Services.CreateBudgetByMonthService;

/**
 * Created by XPS on 7/8/2015.
 */
public class CreateBudgetMonthReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent service = new Intent(context, CreateBudgetByMonthService.class);
        context.startService(service);
    }
}
