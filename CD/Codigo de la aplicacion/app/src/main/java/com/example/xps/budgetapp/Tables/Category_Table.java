package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;

/**
 * Created by XPS on 10/12/2014.
 */
public class Category_Table implements BaseColumns {

    public static final String TABLE_NAME = "CATEGORIAS_TABLE";
    public static final String ID_CATEGORY = "id_categoria";
    public static final String DESCRIPTION = "descripcion";
}
