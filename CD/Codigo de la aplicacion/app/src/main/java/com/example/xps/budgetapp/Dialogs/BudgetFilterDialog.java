package com.example.xps.budgetapp.Dialogs;

/**
 * Created by XPS on 7/5/2015.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;

import com.example.xps.budgetapp.ClickListiners.OnClickEditDate;
import com.example.xps.budgetapp.Controllers.BudgetDataController;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterModifyBudget;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.util.ArrayList;

public class BudgetFilterDialog extends DialogFragment {

    EditText  fromDate;
    EditText  toDate;
    RadioGroup statusBudget;
    View parentRootView;
    String[] error = {"No se encontro ningun presupuesto."};


    public BudgetFilterDialog(View listviewrootview) {
        this.parentRootView = listviewrootview;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View childRootView = inflater.inflate(R.layout.dialog_budget_filter, null);


        fromDate = (EditText) childRootView.findViewById(R.id.FromDate);
        toDate = (EditText) childRootView.findViewById(R.id.ToDate);
        statusBudget = (RadioGroup) childRootView.findViewById(R.id.StatusBudget);

        fromDate.setOnClickListener(new OnClickEditDate(childRootView,getFragmentManager()));
        toDate.setOnClickListener(new OnClickEditDate(childRootView,getFragmentManager()));

        return new AlertDialog.Builder(getActivity())
                .setTitle("Buscar los presupuestos?")
                .setView(childRootView)
                .setPositiveButton("Filtrar",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {

                        String fromDateParam = ManagerDate.ConvertToSqlLiteDateFormat(fromDate.getText().toString());
                        String toDateParam = ManagerDate.ConvertToSqlLiteDateFormat(toDate.getText().toString());
                        int statusBudgetParam = statusBudget.getCheckedRadioButtonId();

                        ArrayList<GenericBudgetModel> budgetList = BudgetDataController.getFilteredBudgetInformations(getActivity(),
                                                                                                                      fromDateParam,
                                                                                                                      toDateParam,
                                                                                                                      statusBudgetParam);

                        ListView budgetListView = (ListView) parentRootView.findViewById(R.id.presupuesto_listview);

                        if (!budgetList.isEmpty()) {

                            ListViewAdapterModifyBudget budgetAdapter = new ListViewAdapterModifyBudget(
                                    parentRootView.getContext(), R.layout.modify_budget_listview_layout,
                                    parentRootView,
                                    budgetList);
                            budgetListView.setAdapter(budgetAdapter);
                        }
                        else {
                            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(parentRootView.getContext(),
                                    android.R.layout.simple_list_item_1,
                                    error);
                            budgetListView.setAdapter(errorAdapter);
                        }
                    }
                })
                .setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

}
