package com.example.xps.budgetapp.TabListener;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

import com.example.xps.budgetapp.R;

/**
 * Created by Osvaldo on 3/20/2015.
 */
public class TabListinerModifyBudgetView implements ActionBar.TabListener {

    private Fragment FRAGMENT;

    public TabListinerModifyBudgetView(Fragment fragment) {
        this.FRAGMENT = fragment;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        fragmentTransaction.add(R.id.modify_budget_view,FRAGMENT);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.remove(FRAGMENT);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
