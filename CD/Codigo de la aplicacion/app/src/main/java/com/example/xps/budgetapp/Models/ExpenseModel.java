package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 11/15/2014.
 */
public class ExpenseModel
{

        private String ID_EXPENSE;
        private String DESCRIPTION;
        private String AMOUNT;
        private String ID_CATEGORY;
        private String RECURRENCE;
        private String DAY_ISSUE;

        public ExpenseModel(String id_expense, String description, String amount,String recurrence,String id_category,String date_issue){

            super();
            this.ID_EXPENSE = id_expense;
            this.DESCRIPTION = description;
            this.AMOUNT = amount;
            this.RECURRENCE = recurrence;
            this.ID_CATEGORY =  id_category;
            this.DAY_ISSUE = date_issue;

        }

        public void setID_EXPENSE(String value){
            ID_EXPENSE = value;}
        public String getID_EXPENSE(){return ID_EXPENSE;}

        public void setDESCRIPTION(String value){ DESCRIPTION = value;}
        public String getDESCRIPTION() {
            return DESCRIPTION;
        }

        public void setAMOUNT(String value){ AMOUNT = value;}
        public String getAMOUNT() {
            return AMOUNT;
        }

        public void setID_CATEGORY(String value){ ID_CATEGORY = value;}
        public String getID_CATEGORY() {
            return ID_CATEGORY;
        }

    public String getRECURRENCE() {
        return RECURRENCE;
    }

    public void setRECURRENCE(String value) {
        this.RECURRENCE = value;
    }

    public String getDAY_ISSUE() {
        return DAY_ISSUE;
    }

    public void setDAY_ISSUE(String DAY_ISSUE) {
        this.DAY_ISSUE = DAY_ISSUE;
    }
}


