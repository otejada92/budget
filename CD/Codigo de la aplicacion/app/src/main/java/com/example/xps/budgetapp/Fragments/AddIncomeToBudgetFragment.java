package com.example.xps.budgetapp.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.xps.budgetapp.AsyncTask.AsyncTaskIncomeProcess;
import com.example.xps.budgetapp.ClickListiners.OnClickEditDate;
import com.example.xps.budgetapp.Controllers.BudgetViewController;
import com.example.xps.budgetapp.Controllers.IncomeDataController;
import com.example.xps.budgetapp.CustomAdapters.ListViewAdapterAddIncomeToBudgetFragment;
import com.example.xps.budgetapp.Models.IncomeModel;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by XPS on 11/30/2014.
 */
public class AddIncomeToBudgetFragment extends Fragment{

    private boolean SaveState = true;
    private View ROOTVIEW;
    private EditText descriptionEditText;
    private EditText amountEditText;
    private EditText dateHappenedEditText;
    private static ListView incomeListView;
    private static String ID_BUDGET;
    public ManagerDate managerDate = new ManagerDate();

    public AddIncomeToBudgetFragment(String id_budget){

        this.ID_BUDGET = id_budget;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        final View rootView = inflater.inflate(R.layout.add_income_to_budget_layout,container,false);
        this.ROOTVIEW = rootView;


        String current_date = managerDate.GetCurrentDateUserFormat();

        InitViews(rootView);

        descriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (descriptionEditText.length() != 0 || !descriptionEditText.getText().toString().equals("")) {
                    SaveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (amountEditText.length() != 0 || !amountEditText.getText().toString().equals("")) {
                    SaveState = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dateHappenedEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                SaveState = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dateHappenedEditText.setInputType(InputType.TYPE_NULL);
        dateHappenedEditText.setOnClickListener(new OnClickEditDate(rootView, getFragmentManager()));
        dateHappenedEditText.setText(current_date);

        PopulateIncomeListView(rootView);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_income_budget_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id_item = item.getItemId();
        String messageError = "";

        Toast toast;
        ManagerDate managerDate = new ManagerDate();
        String[] budgetDate =  getActivity().getIntent().getStringExtra("BUDGET_DATE").split(",");

        String startDateBudgetStr = budgetDate[0];
        String endDateBudgetStr = budgetDate[1];
        String incomeDateHappened = dateHappenedEditText.getText().toString();
        String errorDateHappened = managerDate.ValidateDateHappened(startDateBudgetStr, endDateBudgetStr, incomeDateHappened);

        if (id_item == R.id.add_button){

            if(descriptionEditText.getText().length() == 0 || descriptionEditText.getText().toString().matches("\\s+")){
                messageError += "La descripcion del ingreso no puede estar vacia.\n";
                SaveState = false;
            }

            if (amountEditText.getText().length() == 0) {
                messageError += "El monto del ingreso no puede estar vacio.\n";
                SaveState = false;

            }

            if (!errorDateHappened.equals("")){
                messageError += errorDateHappened+"\n ";
                SaveState = false;
            }


            if(SaveState){
                HashMap<String, String> incomeInformation = new HashMap<String, String>();

                incomeInformation.put("description", descriptionEditText.getText().toString().trim());
                incomeInformation.put("amount", amountEditText.getText().toString().trim());
                incomeInformation.put("recurrence", "0");
                incomeInformation.put("dateHappened", dateHappenedEditText.getText().toString());
                incomeInformation.put("saveState", String.valueOf(SaveState));
                incomeInformation.put("DO", "ADDTOBUDGET");
                incomeInformation.put("idBudget", this.ID_BUDGET);

                AsyncTaskIncomeProcess task = new AsyncTaskIncomeProcess(ROOTVIEW,incomeInformation);
                task.execute();
            }else{
                toast =  Toast.makeText(getActivity(),messageError.trim(),Toast.LENGTH_LONG);
                toast.show();
            }

        }

        if (id_item == android.R.id.home){

            Intent intentHome = new Intent(getActivity().getBaseContext(), BudgetViewController.class);
            startActivity(intentHome);
        }

        return true;
    }

    public static void PopulateIncomeListView(View rootview){

        ArrayList<IncomeModel> income_array = IncomeDataController.getIncomesInformation(rootview.getContext());
        String[] error = {"No hay ingresos insertados."};

        if (!income_array.isEmpty()) {
            ListViewAdapterAddIncomeToBudgetFragment AdapterIncome = new ListViewAdapterAddIncomeToBudgetFragment(
                    rootview.getContext(),
                    R.layout.add_income_listview_layout,
                    rootview,
                    income_array,
                    ID_BUDGET);

            incomeListView.setAdapter(AdapterIncome);
        }
        else {
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(rootview.getContext(),
                    android.R.layout.simple_list_item_1,
                    error);
            incomeListView.setAdapter(errorAdapter);
        }

    }


    public void InitViews(View rootview) {

        descriptionEditText = (EditText) rootview.findViewById(R.id.descripction_income);
        amountEditText = (EditText) rootview.findViewById(R.id.amount_income);
        dateHappenedEditText = (EditText) rootview.findViewById(R.id.DateHappened);
        incomeListView = (ListView)   rootview.findViewById(R.id.income_add_listview);

    }
}
