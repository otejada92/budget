package com.example.xps.budgetapp.Models;

import java.util.ArrayList;

/**
 * Created by XPS on 8/1/2015.
 */
public class LineIncomeModel {


    private String INCOMESUM;
    private String BUDGETED_HAPPENED;
    private ArrayList<BudgetedIncomeModel> INCOMELIST = new ArrayList<BudgetedIncomeModel>();

    public LineIncomeModel(String incomeSum, String dateHappened, ArrayList<BudgetedIncomeModel> incomeList) {
        super();

        this.INCOMESUM = incomeSum;
        this.BUDGETED_HAPPENED = dateHappened;
        this.INCOMELIST = incomeList;
    }

    public String getINCOMESUM() {
        return INCOMESUM;
    }

    public void setINCOMESUM(String INCOMESUM) {
        this.INCOMESUM = INCOMESUM;
    }

    public String getBUDGETED_HAPPENED() {
        return BUDGETED_HAPPENED;
    }

    public void setBUDGETED_HAPPENED(String BUDGETED_HAPPENED) {
        this.BUDGETED_HAPPENED = BUDGETED_HAPPENED;
    }

    public ArrayList<BudgetedIncomeModel> getINCOMELIST() {
        return INCOMELIST;
    }

    public void setINCOMELIST(ArrayList<BudgetedIncomeModel> INCOMELIST) {
        this.INCOMELIST = INCOMELIST;
    }
}
