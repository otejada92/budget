package com.example.xps.budgetapp.Processes;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Interfaces.IncomeStrategy;
import com.example.xps.budgetapp.Tables.Income_Table;

/**
 * Created by XPS on 2/12/2015.
 */
public class IncomeProcesses implements IncomeStrategy {


    @Override
    public void Save(Context context, String Description, String Amount, String Recurrence, String DayHappened) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ContentValues Income_Content_Value;

        Income_Content_Value = setIncomeContentValue(Description,Amount,Recurrence, DayHappened);

        table_manager.insert(Income_Table.TABLE_NAME, null, Income_Content_Value);

        db.close();

    }

    @Override
    public void Modify(Context context, String id_income, String Description, String Amount, String Recurrence,String DayHappened) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ContentValues Income_Content_Value;

        String where = Income_Table.ID_INCOME + " = ? ";
        String[] wherearg = {id_income.trim()};

        Income_Content_Value = setIncomeContentValue(Description,Amount,Recurrence, DayHappened);

        if (Income_Content_Value.size() != 0) {
            table_manager.update(Income_Table.TABLE_NAME,
                    Income_Content_Value,
                    where,
                    wherearg);
        }
    }

    public ContentValues setIncomeContentValue(String Description, String Amount, String Recurrence,String DateHappened){

        ContentValues incomeContentValue = new ContentValues();

        incomeContentValue.put(Income_Table.DESCRIPCTION,Description);
        incomeContentValue.put(Income_Table.AMOUNT,Amount);
        incomeContentValue.put(Income_Table.RECURRENCE,Recurrence);
        incomeContentValue.put(Income_Table.DAY_ISSUE,DateHappened);

        return incomeContentValue;
    }
}
