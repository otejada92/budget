package com.example.xps.budgetapp.Controllers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.xps.budgetapp.Tables.Budget_Table;
import com.example.xps.budgetapp.Tables.Category_Table;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;
import com.example.xps.budgetapp.Tables.Expense_Table;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;
import com.example.xps.budgetapp.Tables.Income_Table;
import com.example.xps.budgetapp.Tables.Queries;

public class DataBaseController extends SQLiteOpenHelper{

    public static final String DB_NAME = "PRESUPUESTO_DB";
    public static final int DATABASE_VERSION = 1;

    public DataBaseController(Context context){
        super(context,DB_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(Queries.INCOME_QUERY);
        sqLiteDatabase.execSQL(Queries.EXPENSE_QUERY);
        sqLiteDatabase.execSQL(Queries.CATEGORY_QUERY);
        sqLiteDatabase.execSQL(Queries.BUDGET_QUERY);
        sqLiteDatabase.execSQL(Queries.BUDGETED_INCOMES_QUERY);
        sqLiteDatabase.execSQL(Queries.BUDGTED_EXPENSES_QUERY);
    }


    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Income_Table.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Expense_Table.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Category_Table.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Budget_Table.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Income_Budget_Table.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Expense_Budget_Table.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

}
