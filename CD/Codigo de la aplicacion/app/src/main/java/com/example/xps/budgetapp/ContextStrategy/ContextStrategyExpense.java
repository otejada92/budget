package com.example.xps.budgetapp.ContextStrategy;

import android.content.Context;

import com.example.xps.budgetapp.Interfaces.ExpenseStrategy;

/**
 * Created by XPS on 2/13/2015.
 */
public class ContextStrategyExpense {

    private ExpenseStrategy STRATEGY;

    public ContextStrategyExpense(ExpenseStrategy strategy) {

        this.STRATEGY = strategy;
    }

    public void executeprocessAdd(Context context, String Description, String Amount, String Recurrence, String Id_category,String DateHappened){
        this.STRATEGY.Save(context,Description,Amount,Recurrence,Id_category,DateHappened);
    }
    public void executeprocessModify(Context context, String Id_expense, String Description, String Amount, String Recurrence, String Id_category,String DateHappened){
        this.STRATEGY.Modify(context,Id_expense,Description,Amount,Recurrence,Id_category,DateHappened);
    }

}
