package com.example.xps.budgetapp.Interfaces;

import android.content.ContentValues;
import android.content.Context;

/**
 * Created by XPS on 7/4/2015.
 */
public interface InsertStrategy {

    void InsertResource(Context context,String tableName,ContentValues resourceValues);

}
