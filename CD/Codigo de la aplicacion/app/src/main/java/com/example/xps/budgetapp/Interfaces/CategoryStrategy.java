package com.example.xps.budgetapp.Interfaces;

import android.content.Context;

/**
 * Created by XPS on 3/15/2015.
 */
public interface CategoryStrategy {

    void Save(Context context, String Description);
    void Modify(Context context, String id_category, String Description);

}
