package com.example.xps.budgetapp.CustomAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.xps.budgetapp.ContextStrategy.ContextStrategyDeleteResource;
import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.Supports.Cleaner;
import com.example.xps.budgetapp.Fragments.ModifyExpenseFragment;
import com.example.xps.budgetapp.Models.CategoryModel;
import com.example.xps.budgetapp.Models.ExpenseModel;
import com.example.xps.budgetapp.Processes.DeleteProcesses;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Tables.Expense_Table;

import java.util.ArrayList;

/**
 * Created by XPS on 11/15/2014.
 */
public class ListViewAdapterModifyExpenseFragment extends ArrayAdapter<ExpenseModel> {

    private Context CONTEXT;
    private ArrayList<ExpenseModel> DATA;
    private int RESOURCE;
    private View ROOTVIEW;

    public EditText descriptionEditText;
    public EditText amountEditText;
    public Spinner categorySpinner;
    public Spinner recurrenceSpinner;
    public MultipleSpinnerSelection dayHappenedSpinner;
    int editButtonIndex = 0;


    public ListViewAdapterModifyExpenseFragment(Context context, int resource, View rootview, ArrayList<ExpenseModel> Data) {
        super(context, resource, Data);

        this.CONTEXT = context;
        this.RESOURCE = resource;
        this.ROOTVIEW = rootview;
        this.DATA = Data;
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder Holder;
        final Cleaner cleanHelper = new Cleaner();


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) CONTEXT
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(this.RESOURCE, parent, false);
            Holder = new ViewHolder();

            Holder.textViewDesc = (TextView) convertView.findViewById(R.id.descripcion);
            Holder.textViewAmount = (TextView) convertView.findViewById(R.id.amount);
            Holder.update_button = (ImageButton) convertView.findViewById(R.id.Editar);
            Holder.delete_button = (ImageButton) convertView.findViewById(R.id.Borrar);

            convertView.setTag(Holder);
        } else {
            Holder = (ViewHolder) convertView.getTag();
        }

        String descString = "<b>Descripcion:</b>"+ this.DATA.get(position).getDESCRIPTION();
        String amountString = "<b> Monto:$ </b>"+ this.DATA.get(position).getAMOUNT();

        Holder.textViewDesc.setId(Integer.parseInt(this.DATA.get(position).getID_EXPENSE()));

        Holder.textViewDesc.setText(Html.fromHtml(descString));

        Holder.textViewAmount.setText(Html.fromHtml(amountString));


        Holder.update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                initViews();

                descriptionEditText.setEnabled(true);
                amountEditText.setEnabled(true);
                recurrenceSpinner.setEnabled(true);
                categorySpinner.setEnabled(true);
                dayHappenedSpinner.setEnabled(true);
                ModifyExpenseFragment.menuActionbar.getItem(editButtonIndex).setEnabled(true);


                ModifyExpenseFragment.idExpense = DATA.get(position).getID_EXPENSE();


                descriptionEditText.setText(DATA.get(position).getDESCRIPTION());
                amountEditText.setText(DATA.get(position).getAMOUNT());
                recurrenceSpinner.setSelection(Integer.valueOf(DATA.get(position).getRECURRENCE()));
                String[] selected = DATA.get(position).getDAY_ISSUE().split(",");
                dayHappenedSpinner.setSelection(selected);

                if (DATA.get(position).getID_CATEGORY() != null) {
                    for (int index = 0; index <= categorySpinner.getAdapter().getCount(); index++) {
                        CategoryModel category = (CategoryModel) categorySpinner.getItemAtPosition(index);
                        if (category.getID_CATEGORY().equals(DATA.get(position).getID_CATEGORY())) {
                            categorySpinner.setSelection(index);
                            break;
                        }
                    }
                }
            }
        });

        Holder.delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String whereClause = Expense_Table.ID_EXPENSE + " = ?";
                final String[] whereArgs = new String[] { DATA.get(position).getID_EXPENSE() };

                LayoutInflater inflater = (LayoutInflater) ROOTVIEW.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rootView = inflater.inflate(R.layout.general_dialog_layout, null);

                TextView textView = (TextView) rootView.findViewById(R.id.Advice);
                textView.setText("Estas seguro que deseas borrar este gasto?, este sera borrado para siempre y no se podra usar para presupuestar!");

                AlertDialog.Builder builder = new AlertDialog.Builder(ROOTVIEW.getContext());
                builder
                        .setTitle("Aviso")
                        .setView(rootView)
                        .setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        ContextStrategyDeleteResource contextDelete = new ContextStrategyDeleteResource(new DeleteProcesses());
                                        contextDelete.executeStrategy(rootView.getContext(), Expense_Table.TABLE_NAME, whereClause, whereArgs);

                                        notifyDataSetChanged();

                                        initViews();

                                        descriptionEditText.setEnabled(false);
                                        recurrenceSpinner.setEnabled(false);
                                        amountEditText.setEnabled(false);
                                        categorySpinner.setEnabled(false);
                                        dayHappenedSpinner.setEnabled(false);
                                        cleanHelper.cleanView((RelativeLayout) ROOTVIEW);

                                    }
                                }
                        )
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }
                        ).show();
            }
        });



        return convertView;
    }


    static class ViewHolder {
        TextView textViewDesc;
        TextView textViewAmount;
        ImageButton update_button;
        ImageButton delete_button;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        ModifyExpenseFragment.populateExpenseListView(ROOTVIEW);
    }

    public void initViews() {

        descriptionEditText = (EditText) ROOTVIEW.findViewById(R.id.descripction_expense);
        amountEditText = (EditText) ROOTVIEW.findViewById(R.id.amount_expense);
        categorySpinner = (Spinner) ROOTVIEW.findViewById(R.id.categories_spinner);
        recurrenceSpinner = (Spinner) ROOTVIEW.findViewById(R.id.RecurrenceSpinner);
        dayHappenedSpinner = (MultipleSpinnerSelection) ROOTVIEW.findViewById(R.id.DayHappened);

    }

}