package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;


public class Income_Table implements  BaseColumns{

    public static final String TABLE_NAME = "INGRESOS_TABLE";
    public static final String ID_INCOME = "id_ingreso";
    public static final String DESCRIPCTION = "descripcion";
    public static final String AMOUNT = "monto";
    public static final String RECURRENCE = "recurrencia";
    public static final String DAY_ISSUE = "dias_ocurrencia";

}
