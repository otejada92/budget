package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.xps.budgetapp.CustomAdapters.CustomGeneralBudgetInformationReportAdapter;
import com.example.xps.budgetapp.Models.GeneralBudgerReportModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class GeneralBudgetInformationController extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.general_budget_information_report_layout);
        ListView listViewContainer = (ListView) findViewById(R.id.ListViewContainer);
        String id_budget = getIntent().getStringExtra("ID_BUDGET");

        BudgetDataController dataController = new BudgetDataController();

        ArrayList<GeneralBudgerReportModel> budgetInformation = dataController.getGeneralBudgetReportInformation(this, id_budget);
        if (!budgetInformation.isEmpty()) {
            CustomGeneralBudgetInformationReportAdapter budgetInformationAdapter = new CustomGeneralBudgetInformationReportAdapter(
                    this,
                    R.layout.general_budget_report_view,
                    R.id.PeriodoContainer,
                    R.id.TextResult,
                    R.id.ingreso_table,
                    R.id.gasto_table,
                    R.id.result_table,
                    budgetInformation);
            listViewContainer.setAdapter(budgetInformationAdapter);
        }else {
            String [] messageError = {"No hay data por el momento"};
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,messageError);
            listViewContainer.setAdapter(errorAdapter);
        }


    }


}
