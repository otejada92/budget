package com.example.xps.budgetapp.Supports;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Tables.Budget_Table;

import java.util.UUID;

public class IdFactory {

    private Context CONTEXT;
    private String TABLE;

    public IdFactory(Context context,String table) {

        this.CONTEXT = context;
        this.TABLE = table;
    }

    public  String GetUUID(){

       return GetUnExistID();
   }

   public String GetUnExistID(){

       DataBaseController db = new DataBaseController(CONTEXT);
       SQLiteDatabase table_manager = db.getWritableDatabase();
       String id = UUID.randomUUID().toString();

       Cursor budget_cursor = table_manager.rawQuery("SELECT * FROM " + this.TABLE + " WHERE "+ Budget_Table.ID_BUDGET+ " = ?" , new String[]{id});

       if(budget_cursor.moveToFirst())
           GetUnExistID();

       return id;

   }

}
