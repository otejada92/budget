package com.example.xps.budgetapp.ClickListiners;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.xps.budgetapp.CustomView.MultipleSpinnerSelection;
import com.example.xps.budgetapp.R;

/**
 * Created by XPS on 6/29/2015.
 */
public class SelectRecurrenceClick implements AdapterView.OnItemSelectedListener {

    public View rootView;

    public SelectRecurrenceClick(View rootView) {

        this.rootView = rootView;
    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

        TextView textView = (TextView) rootView.findViewById(R.id.textView4);
        MultipleSpinnerSelection spinnerSelection = (MultipleSpinnerSelection) rootView.findViewById(R.id.DayHappened);
        ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.help_button);

        int variable_value = 0;

        if (position == variable_value) {
            textView.setVisibility(View.INVISIBLE);
            spinnerSelection.setVisibility(View.INVISIBLE);
            imageButton.setVisibility(View.INVISIBLE);
        }else{
            textView.setVisibility(View.VISIBLE);
            spinnerSelection.setVisibility(View.VISIBLE);
            imageButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
