package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 3/10/2015.
 */
public class MonthlyBudgetModel {

    private String START_DATE;
    private String END_DATE;


    public MonthlyBudgetModel(String start_date,String end_date) {

        this.START_DATE = start_date;
        this.END_DATE = end_date;

    }

    public String getSTART_DATE() {
        return START_DATE;
    }

    public void setSTART_DATE(String START_DATE) {
        this.START_DATE = START_DATE;
    }

    public String getEND_DATE() {
        return END_DATE;
    }

    public void setEND_DATE(String END_DATE) {
        this.END_DATE = END_DATE;
    }


}

