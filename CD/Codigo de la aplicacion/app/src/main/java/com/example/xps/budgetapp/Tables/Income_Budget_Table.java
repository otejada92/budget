package com.example.xps.budgetapp.Tables;

import android.provider.BaseColumns;

/**
 * Created by XPS on 10/25/2014.
 */
public class Income_Budget_Table implements BaseColumns {

    public static final String TABLE_NAME = "INGRESO_PRESUPUESTADO";
    public static final String ID_INCOME = "id_ingreso_presupuestado";
    public static final String ID_BUDGET = "id_presupuesto";
    public static final String DESCRIPTION = "descripcion";
    public static final String AMOUNT = "monto";
    public static final String DATE_INSERTED = "fecha_presupuestado";

}
