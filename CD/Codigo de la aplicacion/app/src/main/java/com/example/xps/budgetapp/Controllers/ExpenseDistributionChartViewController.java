package com.example.xps.budgetapp.Controllers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.xps.budgetapp.CustomAdapters.CustomExpenseDistributionAdapter;
import com.example.xps.budgetapp.Models.ChartDistribucionModel;
import com.example.xps.budgetapp.R;

import java.util.ArrayList;

public class ExpenseDistributionChartViewController extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.expense_distribution_chart_view);

        String idBudget = getIntent().getStringExtra("ID_BUDGET");
        ListView listViewContainer = (ListView) findViewById(R.id.ListViewContainer);


        ArrayList<ChartDistribucionModel> chartInformation = BudgetDataController.getDistributionBudgetInformation(this, idBudget);
        if (!chartInformation.isEmpty()) {
            CustomExpenseDistributionAdapter distributionAdapter = new CustomExpenseDistributionAdapter(
                    getBaseContext(),
                    R.layout.expense_distribution_listview_layout,
                    R.id.chart1,
                    R.id.Month,
                    chartInformation,
                    getFragmentManager());
            listViewContainer.setAdapter(distributionAdapter);
        }else {

            String [] messageError = {"No hay data por el momento"};
            ArrayAdapter<String> errorAdapter = new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,messageError);
            listViewContainer.setAdapter(errorAdapter);
        }

    }

}