package com.example.xps.budgetapp.Models;

/**
 * Created by XPS on 7/18/2015.
 */
public class CategoryDistributionExpenseDetail {

    private String expenseDescription;
    private String expenseAmount;
    private String idCategory;

    public CategoryDistributionExpenseDetail(String expenseDescription, String expenseAmount,String idCategory) {

        this.expenseDescription = expenseDescription;
        this.expenseAmount = expenseAmount;
        this.idCategory= idCategory;
    }

    public String getExpenseDescription() {
        return expenseDescription;
    }

    public void setExpenseDescription(String expenseDescription) {
        this.expenseDescription = expenseDescription;
    }

    public String getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }
}
