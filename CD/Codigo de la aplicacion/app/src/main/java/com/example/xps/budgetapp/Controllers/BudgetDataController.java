package com.example.xps.budgetapp.Controllers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Models.BudgetedExpenseModel;
import com.example.xps.budgetapp.Models.BudgetedIncomeModel;
import com.example.xps.budgetapp.Models.CategoryDistributionExpenseDetail;
import com.example.xps.budgetapp.Models.ChartDistribucionModel;
import com.example.xps.budgetapp.Models.GeneralBudgerReportModel;
import com.example.xps.budgetapp.Models.GenericBudgetModel;
import com.example.xps.budgetapp.Models.GoalBudgetReportModel;
import com.example.xps.budgetapp.Models.DistributionCategoryTotals;
import com.example.xps.budgetapp.R;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Budget_Table;
import com.example.xps.budgetapp.Tables.Expense_Budget_Table;
import com.example.xps.budgetapp.Tables.Income_Budget_Table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TreeMap;

/**
 * Created by Osvaldo on 11/6/2014.
 */
public class BudgetDataController {

    private static SimpleDateFormat SqlLiteFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);

    public static ArrayList<GenericBudgetModel> getBudgetsInformation(Context context){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase manager_tables = db.getWritableDatabase();
        ArrayList<GenericBudgetModel> presupuesto_list = new ArrayList<GenericBudgetModel>();

        Cursor budget_c = manager_tables.rawQuery("SELECT " +
                Budget_Table.ID_BUDGET + "," +
                Budget_Table.BUDGET_DESCRIPTION + "," +
                Budget_Table.BUDGET_GOAL + "," +
                Budget_Table.INIT_DATE + "," +
                Budget_Table.END_DATE + "," +
                Budget_Table.BUDGET_STATUS + "," +
                Budget_Table.SINCRONIZADO + "," +
                Budget_Table.ID_ALARM_START_PROCESS + "," +
                Budget_Table.ID_ALARM_END_PROCESS +
                " FROM " +
                Budget_Table.TABLE_NAME +
                " WHERE " + Budget_Table.BUDGET_STATUS + " = 1" +
                " OR " + Budget_Table.BUDGET_STATUS + " = 2"
                , null);
        if(budget_c.moveToFirst()){
            do {
                presupuesto_list.add(new GenericBudgetModel
                        (budget_c.getString(0),
                        budget_c.getString(1),
                        budget_c.getString(2),
                        budget_c.getString(3),
                        budget_c.getString(4),
                        budget_c.getString(5),
                        budget_c.getString(6),
                        budget_c.getString(7),
                        budget_c.getString(8)));
            }while (budget_c.moveToNext());
        }
        budget_c.close();
        return presupuesto_list;
    }

    public static ArrayList<GenericBudgetModel> getFilteredBudgetInformations(Context context,String fromDate,String toDate,int budgetStatus){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase manager_tables = db.getWritableDatabase();
        ArrayList<GenericBudgetModel> budgetList = new ArrayList<GenericBudgetModel>();
        Cursor budget_c;

        String budgetStatusId = getBudgetStatus(budgetStatus);

        if (!budgetStatusId.equals("Todos")) {
             budget_c = manager_tables.rawQuery("SELECT " +
                    Budget_Table.ID_BUDGET + "," +
                    Budget_Table.BUDGET_DESCRIPTION + "," +
                    Budget_Table.BUDGET_GOAL + "," +
                    Budget_Table.INIT_DATE + "," +
                    Budget_Table.END_DATE + "," +
                    Budget_Table.BUDGET_STATUS + "," +
                    Budget_Table.SINCRONIZADO + "," +
                    Budget_Table.ID_ALARM_START_PROCESS + "," +
                    Budget_Table.ID_ALARM_END_PROCESS +
                    " FROM " +
                    Budget_Table.TABLE_NAME +
                    " WHERE " + Budget_Table.INIT_DATE +
                    " BETWEEN " + "'" + fromDate + "'" + " AND " + "'" + toDate + "'" +
                    " AND " + Budget_Table.BUDGET_STATUS + " = ?" +
                    " ORDER BY " + Budget_Table.INIT_DATE + " ASC ", new String[]{budgetStatusId});
        }
        else {
            budget_c = manager_tables.rawQuery("SELECT " +
                    Budget_Table.ID_BUDGET + "," +
                    Budget_Table.BUDGET_DESCRIPTION + "," +
                    Budget_Table.BUDGET_GOAL + "," +
                    Budget_Table.INIT_DATE + "," +
                    Budget_Table.END_DATE + "," +
                    Budget_Table.BUDGET_STATUS + "," +
                    Budget_Table.SINCRONIZADO + "," +
                    Budget_Table.ID_ALARM_START_PROCESS + "," +
                    Budget_Table.ID_ALARM_END_PROCESS +
                    " FROM " +
                    Budget_Table.TABLE_NAME +
                    " WHERE " + Budget_Table.INIT_DATE +
                    " BETWEEN " + "'" + fromDate + "'" + " AND " + "'" + toDate + "'" +
                    " ORDER BY " + Budget_Table.INIT_DATE + " ASC ",null);
        }

        if(budget_c.moveToFirst()){
            do {
                budgetList.add(new GenericBudgetModel(
                        budget_c.getString(0),
                        budget_c.getString(1),
                        budget_c.getString(2),
                        budget_c.getString(3),
                        budget_c.getString(4),
                        budget_c.getString(5),
                        budget_c.getString(6),
                        budget_c.getString(7),
                        budget_c.getString(8)));
            }while (budget_c.moveToNext());
        }
        budget_c.close();

        return budgetList;
    }

    private static String getBudgetStatus(int budgetStatus) {

        if (R.id.Todos == budgetStatus)
            return "Todos";

        if (R.id.en_progreso == budgetStatus)
            return "1";

        if (R.id.programado == budgetStatus)
            return "2";

        if (R.id.finalizado == budgetStatus)
            return "0";

        return "Todos";
    }

    public static ArrayList<GenericBudgetModel> getFixedBudgetsInformation(Context context){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase manager_tables = db.getWritableDatabase();
        ArrayList<GenericBudgetModel> presupuesto_list = new ArrayList<GenericBudgetModel>();

        Cursor budget_c = manager_tables.rawQuery("SELECT "+
                Budget_Table.ID_BUDGET +","+
                Budget_Table.BUDGET_DESCRIPTION+","+
                Budget_Table.BUDGET_GOAL+","+
                Budget_Table.INIT_DATE +","+
                Budget_Table.END_DATE +","+
                Budget_Table.BUDGET_STATUS+","+
                Budget_Table.SINCRONIZADO +","+
                Budget_Table.ID_ALARM_START_PROCESS+","+
                Budget_Table.ID_ALARM_END_PROCESS +
                " FROM " +
                Budget_Table.TABLE_NAME +
                " WHERE " + Budget_Table.BUDGET_STATUS + " = 1" +
                " AND " + Budget_Table.SINCRONIZADO + " LIKE '%true%'"
                , null);
        if(budget_c.moveToFirst()){
            do {
                presupuesto_list.add(new GenericBudgetModel(
                        budget_c.getString(0),
                        budget_c.getString(1),
                        budget_c.getString(2),
                        budget_c.getString(3),
                        budget_c.getString(4),
                        budget_c.getString(5),
                        budget_c.getString(6),
                        budget_c.getString(7),
                        budget_c.getString(8)));
            }while (budget_c.moveToNext());
        }
        budget_c.close();
        return presupuesto_list;
    }

    public static  boolean verifyBudgetOnDateRange(Context context, String fromDate, String toDate){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase manager_tables = db.getWritableDatabase();

        Cursor budget_c = manager_tables.rawQuery("SELECT "+
                Budget_Table.ID_BUDGET +","+
                Budget_Table.BUDGET_DESCRIPTION+","+
                Budget_Table.BUDGET_GOAL+","+
                Budget_Table.INIT_DATE +","+
                Budget_Table.END_DATE +","+
                Budget_Table.ID_ALARM_END_PROCESS +","+
                Budget_Table.SINCRONIZADO +","+
                Budget_Table.BUDGET_STATUS+
                " FROM " +Budget_Table.TABLE_NAME +
                " WHERE "+Budget_Table.INIT_DATE+
                " BETWEEN " + "'" + fromDate + "'" + " AND " + "'" + toDate + "'", null);

        if(budget_c.moveToFirst()){
            return true;
        }

        budget_c.close();
        return  false;
    }

    public static GenericBudgetModel getBudgetByID(Context context, String id_budget){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase manager_tables = db.getWritableDatabase();
        GenericBudgetModel genericBudgetModel = null;

        Cursor budget_c = manager_tables.rawQuery("SELECT "+
                Budget_Table.ID_BUDGET +","+
                Budget_Table.BUDGET_DESCRIPTION+","+
                Budget_Table.BUDGET_GOAL+","+
                Budget_Table.INIT_DATE +","+
                Budget_Table.END_DATE +","+
                Budget_Table.BUDGET_STATUS+","+
                Budget_Table.SINCRONIZADO +","+
                Budget_Table.ID_ALARM_START_PROCESS+","+
                Budget_Table.ID_ALARM_END_PROCESS +
                " FROM " +
                Budget_Table.TABLE_NAME +
                " WHERE " + Budget_Table.ID_BUDGET + " = ?"
                , new String[]{id_budget});
        if(budget_c.moveToFirst()){
            do {
                genericBudgetModel = new GenericBudgetModel(budget_c.getString(0),
                        budget_c.getString(1),
                        budget_c.getString(2),
                        ManagerDate.ConvertToUserDateFormat(budget_c.getString(3)),
                        ManagerDate.ConvertToUserDateFormat(budget_c.getString(4)),
                        budget_c.getString(5),
                        budget_c.getString(6),
                        budget_c.getString(7),
                        budget_c.getString(8));
            }while (budget_c.moveToNext());
        }
        budget_c.close();
        return genericBudgetModel;
    }

    public String getDescBudget(Context context, String id_budget){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase presupuesto_table = db.getWritableDatabase();
        String budget_description = "";

        Cursor presupuesto_c = presupuesto_table.rawQuery("SELECT "+
                Budget_Table.BUDGET_DESCRIPTION +
                " FROM " + Budget_Table.TABLE_NAME+
                " WHERE "+ Budget_Table.ID_BUDGET +" = ?",new String[]{id_budget});

        if (presupuesto_c.moveToFirst()) {
            do {

                budget_description = presupuesto_c.getString(0);

            } while(presupuesto_c.moveToNext());

        }
        presupuesto_c.close();
        db.close();
        return budget_description;
    }

    public static int getTotalExpense(ArrayList<BudgetedExpenseModel> ExpenseList) {

        int TotalIncome = 0;

        for (BudgetedExpenseModel expenseModel : ExpenseList){

            TotalIncome = Integer.parseInt(expenseModel.getAMOUNT()) + TotalIncome;
        }
        return TotalIncome;


    }

    public static int getTotalIncome(ArrayList<BudgetedIncomeModel> IncomeList){

        int TotalIncome = 0;

        for (BudgetedIncomeModel incomeModel : IncomeList){

            TotalIncome = Integer.parseInt(incomeModel.getAMOUNT()) + TotalIncome;
        }
        return TotalIncome;
    }

    public ArrayList<GeneralBudgerReportModel> getGeneralBudgetReportInformation(Context context, String id_budget){

        ArrayList<GeneralBudgerReportModel> budgetInformation = new ArrayList<GeneralBudgerReportModel>();

        IncomeDataController incomeDataController = new IncomeDataController();
        ExpenseDataController  expenseDataController =  new ExpenseDataController();

        ManagerDate managerDate = new ManagerDate();

        Calendar calendar = Calendar.getInstance();
        int Year = calendar.get(Calendar.YEAR);

        TreeMap<String, String> MonthsRange = null;

        if (calendar.getActualMaximum(Calendar.DAY_OF_YEAR) == 365) {
            MonthsRange = managerDate.getHmapMonthRangeNormal();
        }else{
            MonthsRange =managerDate.getHmapMonthRangeBi();
        }

        for(String key: MonthsRange.keySet()) {

            String[] monthDates = MonthsRange.get(key).split(",");
           String startDateMonth = String.valueOf(Year)+monthDates[0];
           String endDateMonth = String.valueOf(Year)+monthDates[1];

         ArrayList<BudgetedIncomeModel> incomeList = incomeDataController.getIncomesForGeneralBudgetReport(context, id_budget, startDateMonth, endDateMonth);
          ArrayList<BudgetedExpenseModel> expenseList = expenseDataController.getExpenseForGeneralBudgetReport(context, id_budget, startDateMonth, endDateMonth);

            if(!incomeList.isEmpty() || !expenseList.isEmpty()) {
               budgetInformation.add(new GeneralBudgerReportModel(managerDate.getMonthName(key),incomeList,expenseList));
           }
        }
        return budgetInformation;
    }

    public static ArrayList<ChartDistribucionModel> getDistributionBudgetInformation(Context context, String id_budget){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        ManagerDate managerDate = new ManagerDate();

        ArrayList<ChartDistribucionModel> chartData = new ArrayList<ChartDistribucionModel>();

        Calendar calendar = Calendar.getInstance();
        int Year = calendar.get(Calendar.YEAR);

        TreeMap<String, String> MonthsRange = null;

        if (calendar.getActualMaximum(Calendar.DAY_OF_YEAR) == 365) {
            MonthsRange = managerDate.getHmapMonthRangeNormal();
        }else{
            MonthsRange =managerDate.getHmapMonthRangeBi();
        }

        for(String key: MonthsRange.keySet()) {

            ArrayList<DistributionCategoryTotals> expenseTotals = new ArrayList<DistributionCategoryTotals>();
            ArrayList<CategoryDistributionExpenseDetail> expenseDetails = new ArrayList<CategoryDistributionExpenseDetail>();
            String[] monthDates = MonthsRange.get(key).split(",");
            String startDateMonth = String.valueOf(Year) + monthDates[0];
            String endDateMonth = String.valueOf(Year) + monthDates[1];

            Cursor category_c = table_manager.rawQuery(
                    "SELECT " + Expense_Budget_Table.CATEGORY_DESC + "," +
                            "SUM(" + Expense_Budget_Table.AMOUNT + ")," +
                            Expense_Budget_Table.DATE_BDGETED + "," +
                            Expense_Budget_Table.ID_CATEGORY +
                            " FROM " + Expense_Budget_Table.TABLE_NAME +
                            " WHERE " + Expense_Budget_Table.ID_BUDGET + " = ?" +
                            " AND " + Expense_Budget_Table.DATE_BDGETED +
                            " BETWEEN " + "'" + startDateMonth + "'" + " AND " + "'" + endDateMonth + "'" +
                            " GROUP BY " + Expense_Budget_Table.ID_CATEGORY, new String[]{id_budget});

            if(category_c.moveToFirst()){
                do {

                    expenseTotals.add(new DistributionCategoryTotals(category_c.getString(0), category_c.getString(1), category_c.getString(3)));

                    Cursor expense_c = table_manager.rawQuery(
                            " SELECT "+Expense_Budget_Table.DESCRIPTION+","+
                                    Expense_Budget_Table.AMOUNT+","+
                                    Expense_Budget_Table.ID_CATEGORY+
                                    " FROM " + Expense_Budget_Table.TABLE_NAME +
                                    " WHERE " + Expense_Budget_Table.ID_BUDGET + " = ?"+
                                    " AND "+ Expense_Budget_Table.ID_CATEGORY+" = ?",new String[]{id_budget,category_c.getString(3)});

                    if (expense_c.moveToFirst()) {
                        do {
                            expenseDetails.add(new CategoryDistributionExpenseDetail(expense_c.getString(0), expense_c.getString(1), expense_c.getString(2)));
                        } while (expense_c.moveToNext());
                    expense_c.close();
                    }

                    if(category_c.isLast()) {
                        chartData.add(new ChartDistribucionModel(managerDate.getMonthName(key),expenseTotals,expenseDetails));
                        break;
                    }
                }while (category_c.moveToNext());
            }
            category_c.close();
        }

        db.close();

        return chartData;

    }

    public static ArrayList<GoalBudgetReportModel> getGoalBudgetInformation(Context context, String id_budget){

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();
        ManagerDate managerDate = new ManagerDate();

        GenericBudgetModel BudgetInformation = getBudgetByID(context, id_budget);
        ArrayList<GoalBudgetReportModel> chartData = new ArrayList<GoalBudgetReportModel>();

        Calendar calendar = Calendar.getInstance();
        int Year = calendar.get(Calendar.YEAR);

        TreeMap<String, String> MonthsRange = null;

        if (calendar.getActualMaximum(Calendar.DAY_OF_YEAR) == 365) {
            MonthsRange = managerDate.getHmapMonthRangeNormal();
        }else{
            MonthsRange =managerDate.getHmapMonthRangeBi();
        }

        for(String key: MonthsRange.keySet()) {

            int ahorro = 0;
            GoalBudgetReportModel goalBudgetInformation = new GoalBudgetReportModel();

            String[] monthDates = MonthsRange.get(key).split(",");
            String startDateMonth = String.valueOf(Year) + monthDates[0];
            String endDateMonth = String.valueOf(Year) + monthDates[1];

            Cursor income_c = table_manager.rawQuery(
                    "SELECT " + "SUM(" + Income_Budget_Table.AMOUNT + ")," +
                            Income_Budget_Table.TABLE_NAME + "." + Income_Budget_Table.DATE_INSERTED +
                            " FROM " + Income_Budget_Table.TABLE_NAME +
                            " WHERE "+ Income_Budget_Table.ID_BUDGET + " = ?" +
                            " AND " + Income_Budget_Table.DATE_INSERTED +
                            " BETWEEN " + "'" + startDateMonth + "'" + " AND " + "'" + endDateMonth + "'", new String[]{id_budget});

            Cursor expense_c = table_manager.rawQuery(
                    "SELECT " + "SUM(" + Expense_Budget_Table.AMOUNT + ")," +
                             Expense_Budget_Table.DATE_BDGETED +
                            " FROM " + Expense_Budget_Table.TABLE_NAME +
                            " WHERE " + Expense_Budget_Table.ID_BUDGET + " = ?" +
                            " AND " + Expense_Budget_Table.DATE_BDGETED +
                            " BETWEEN " + "'" + startDateMonth + "'" + " AND " + "'" + endDateMonth + "'", new String[]{id_budget});

            if (income_c.moveToFirst()){
                do {
                    goalBudgetInformation.setTOTALINCOME(income_c.getString(0));
                }while (income_c.moveToNext());
            }

            if (expense_c.moveToFirst()){
                do {
                    goalBudgetInformation.setTOTALEXPENSE(expense_c.getString(0));
                }while (expense_c.moveToNext());
            }

            if(goalBudgetInformation.getTOTALINCOME() != null || goalBudgetInformation.getTOTALEXPENSE() != null) {

                goalBudgetInformation.setMONTH(managerDate.getMonthName(key));
                ahorro += calculateGoalResult(goalBudgetInformation.getTOTALINCOME(), goalBudgetInformation.getTOTALEXPENSE());
                goalBudgetInformation.setGOALBUDGET_PROCESS(ahorro);
                goalBudgetInformation.setGOALBUDGET(BudgetInformation.getBUDGET_GOAL());

                chartData.add(goalBudgetInformation);
            }

        }

        db.close();


        return chartData;

    }

    public static float calculateGoalResult(String incomeTotal, String expenseTotal){

        float result = (float) 0;

        if(incomeTotal != null && expenseTotal !=null) {
            result = Float.valueOf(incomeTotal) - Float.valueOf(expenseTotal);
            return result;
        }

        if(incomeTotal != null ) {
            result += Float.valueOf(incomeTotal);
            return result;
        }

        if(expenseTotal != null ) {
            result -= Float.valueOf(expenseTotal);
            return result;
        }
        return result;
    }

    public String vatidateBudgetInformation(String descBudget, String goalBudget, String startDateBudgetStr, String endDateBudgetStr) {

        String errorToReturn = "";

        String caseError1 = "";
        String caseError2 = "";
        String caseError3 = "";
        String caseError4 = "";
        String caseError5 = "";
        String caseError6 = "";

        startDateBudgetStr = ManagerDate.ConvertToSqlLiteDateFormat(startDateBudgetStr);
        endDateBudgetStr = ManagerDate.ConvertToSqlLiteDateFormat(endDateBudgetStr);

        Date startDateBudget = new Date();
        Date endDateBudget = new Date();

        try {
            startDateBudget = SqlLiteFormat.parse(startDateBudgetStr);
            endDateBudget = SqlLiteFormat.parse(endDateBudgetStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (descBudget.equals("")) {
            caseError1 = " La descripcion no puede estar vacia.";
            errorToReturn += caseError1+"\n";
        }

        if (goalBudget.equals("")) {
            caseError2 = " La meta del presupuesto no puede estar vacia.";
            errorToReturn += caseError2+"\n";
        }else if(Float.valueOf(goalBudget) <= 0){
            caseError2 = " La meta del presupuesto debe ser mayor que 0";
            errorToReturn += caseError2+"\n";
        }

        if (startDateBudget.after(endDateBudget)) {
            caseError3 = " La fecha inicio no puede ser mayor que la Fecha Fin.";
            errorToReturn += caseError3+"\n";
        }

        if (endDateBudget.before(startDateBudget)) {
            caseError4 = " La fecha fin no puede ser menor que la fecha inicio.";
            errorToReturn += caseError4+"\n";
        }

        if(startDateBudgetStr.equals("")){
            caseError5 = " La fecha inicio no puede estar vacio.";
            errorToReturn += caseError5+"\n";
        }

        if(endDateBudgetStr.equals("")){
            caseError6 = " La fecha fin no puede estar vacio.";
            errorToReturn += caseError6+"\n";
        }
        return errorToReturn.trim();
    }


}
