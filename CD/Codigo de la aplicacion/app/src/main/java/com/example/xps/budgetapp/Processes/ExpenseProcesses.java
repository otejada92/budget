package com.example.xps.budgetapp.Processes;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.xps.budgetapp.Controllers.DataBaseController;
import com.example.xps.budgetapp.Interfaces.ExpenseStrategy;
import com.example.xps.budgetapp.Tables.Expense_Table;

/**
 * Created by XPS on 2/13/2015.
 */
public class ExpenseProcesses implements ExpenseStrategy {

    @Override
    public void Save(Context context, String Description, String Amount, String Recurrence, String Id_category, String DateHappened) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ContentValues Expense_Content_Value;

        Expense_Content_Value = setExpenseContentValue(Description, Amount, Recurrence, Id_category,DateHappened);

        table_manager.insert(Expense_Table.TABLE_NAME, null, Expense_Content_Value);
        db.close();
    }

    @Override
    public void Modify(Context context, String id_expense, String description, String amount, String recurrence, String id_category, String DateHappened) {

        DataBaseController db = new DataBaseController(context);
        SQLiteDatabase table_manager = db.getWritableDatabase();

        ContentValues Expense_Content_Value = setExpenseContentValue(description, amount, recurrence, id_category,DateHappened);
        String where = Expense_Table.ID_EXPENSE + " = ? ";
        String[] wherearg = {id_expense};

        if (Expense_Content_Value.size() != 0) {
            table_manager.update(Expense_Table.TABLE_NAME,
                    Expense_Content_Value,
                    where,
                    wherearg);
        }

    }

    public ContentValues setExpenseContentValue(String description, String amount, String recurrence, String id_category,String DateHappened){


        ContentValues expenseContentValue = new ContentValues();

        expenseContentValue.put(Expense_Table.DESCRIPCTION,description);
        expenseContentValue.put(Expense_Table.AMOUNT,amount);
        expenseContentValue.put(Expense_Table.RECURRENCE,recurrence);
        expenseContentValue.put(Expense_Table.ID_CATEGORY,id_category);
        expenseContentValue.put(Expense_Table.DAY_ISSUE,DateHappened);

        return expenseContentValue;
    }

}
