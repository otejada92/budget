package com.example.xps.budgetapp.Services;

/**
 * Created by XPS on 7/8/2015.
 */

import android.app.NotificationManager;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;

import com.example.xps.budgetapp.AlarmServicesConfigurations.EndBudgetAlarm;
import com.example.xps.budgetapp.AlarmServicesConfigurations.MonthBudgetCheckerAlarm;
import com.example.xps.budgetapp.AlarmServicesConfigurations.StartBudgetAlarm;
import com.example.xps.budgetapp.Controllers.DataBaseController;

import com.example.xps.budgetapp.Supports.IdFactory;
import com.example.xps.budgetapp.Supports.ManagerDate;
import com.example.xps.budgetapp.Tables.Budget_Table;

import java.text.ParseException;
import java.util.Calendar;

public class CreateBudgetByMonthService extends Service {


    ManagerDate managerDate = new ManagerDate();
    MonthBudgetCheckerAlarm monthBudgetCheckerAlarm;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent,flags, startId);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        DataBaseController db = new DataBaseController(getApplication().getBaseContext());
        SQLiteDatabase tableManager = db.getWritableDatabase();
        ContentValues budgetValue = new ContentValues();

        Calendar calendarInstance = Calendar.getInstance();

        String startDateStr = "";
        String endDateStr = "";

        if (intent != null) {

            startDateStr = intent.getStringExtra("START_DATE");
            endDateStr = intent.getStringExtra("END_DATE");

            int currentDay = calendarInstance.get(Calendar.DAY_OF_MONTH);
            int maxDay = calendarInstance.getActualMaximum(Calendar.DAY_OF_MONTH);

            IdFactory idFactory = new IdFactory(getBaseContext(), Budget_Table.TABLE_NAME);
            String idBudget = idFactory.GetUUID();

            int idStartProcess = managerDate.getAlarmId();
            int idEndProcess = managerDate.getAlarmId();

            budgetValue.put(Budget_Table.ID_BUDGET, idBudget);
            budgetValue.put(Budget_Table.BUDGET_DESCRIPTION, "Sin descripcion");
            budgetValue.put(Budget_Table.BUDGET_GOAL, "0");
            budgetValue.put(Budget_Table.BUDGET_STATUS, "1");
            budgetValue.put(Budget_Table.INIT_DATE, startDateStr);
            budgetValue.put(Budget_Table.END_DATE, endDateStr);
            budgetValue.put(Budget_Table.SINCRONIZADO, "true");

            try {
                if (managerDate.isTheStartServiceNeed(startDateStr)) {
                    budgetValue.put(Budget_Table.ID_ALARM_START_PROCESS, idStartProcess);
                    StartBudgetAlarm startBudgetAlarm = new StartBudgetAlarm(getApplication().getBaseContext(), startDateStr.split("/"), idBudget, idStartProcess);
                    startBudgetAlarm.setStartBudgetServices();
                } else {
                    budgetValue.put(Budget_Table.ID_ALARM_START_PROCESS, "0");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            EndBudgetAlarm endBudgetAlarm = new EndBudgetAlarm(getApplication().getBaseContext(), endDateStr.split("/"), idBudget, idEndProcess);
            endBudgetAlarm.setEndBudgetServices();

            budgetValue.put(Budget_Table.ID_ALARM_END_PROCESS, idEndProcess);
            tableManager.insert(Budget_Table.TABLE_NAME, null, budgetValue);

            monthBudgetCheckerAlarm = new MonthBudgetCheckerAlarm(getBaseContext(), true, maxDay - currentDay);
            monthBudgetCheckerAlarm.setMonthBudgetChecker();

        }
        notificationManager.cancel(startId);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
